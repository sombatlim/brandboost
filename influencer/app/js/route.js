﻿
app.config(function($routeProvider, $locationProvider, $httpProvider) {
  	
	$locationProvider.html5Mode(true).hashPrefix('!');
	
	
	$routeProvider.when('/', {
	    templateUrl: '/app/view/home.html',
	    controller: HomeController,
	    reloadOnSearch: false
	});		

	//// TEST by sombat
	
	$routeProvider.when('/inbox', {
	    templateUrl: '/app/view/messages.html',
	    controller: MessagesController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/outbox', {
	    templateUrl: '/app/view/messages.html',
	    controller: MessagesController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/requested-campaigns', {
	    templateUrl: '/app/view/requested-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/posted-campaigns', {
	    templateUrl: '/app/view/posted-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/revenue-earned', {
	    templateUrl: '/app/view/revenue-earned.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/advertiser-requests', {
	    templateUrl: '/app/view/advertiser-requests.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/interests', {
	    templateUrl: '/app/view/signup/interests.html',
	    controller: InterestsController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/profile', {
	    templateUrl: '/app/view/signup/profile.html',
	    controller: ProfileController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/login', {
	    templateUrl: '/app/view/signup/login.html',
	    controller: LoginController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/signup', {
	    templateUrl: '/app/view/signup/signup.html',
	    controller: SignupController,
	    reloadOnSearch: false
	});	
	
	
	$routeProvider.when('/instagram/:success', {
		templateUrl: '/app/view/instagram.html',
		controller: InstagramController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/logout', {
		templateUrl: '/app/view/logout.html',
		controller: LogoutController,
	    reloadOnSearch: false
	});	
	
	
	/// Authorise the user.
	
	$routeProvider.otherwise('/signup');
	$routeProvider.otherwise('/signup/:success')
	
     var interceptor = ['$location', '$q', function ($location, $q) {

         function success(response) {
             return response;
         }

         function error(response) {

             if (response.status === 401) {
                 $location.path('/login');
                 return $q.reject(response);
             }
             else {
                 return $q.reject(response);
             }
         }

         return function (promise) {
             return promise.then(success, error);
         };
     }];

     $httpProvider.responseInterceptors.push(interceptor);

})



