function LoginController($rootScope,$scope, $routeParams, $location, $window, apiFactory,  influencersModel, influencerInterestModel, $templateCache) {
	$scope.instagram = {};

	console.log('in LoginController');
	
	$scope.apiFactory = {};
	$scope.influencer = {};
	
	$scope.error = null;
	
	$rootScope.signin = false;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step20 = false;
	$rootScope.step30 = false;
	$rootScope.step10 = false;
	$rootScope.step21 = false;
	$rootScope.step31 = false;
	$rootScope.stepbar = false;	
	
	$templateCache.removeAll();
	
	apiFactory.post('getInstagramLoginUrl', $scope).then(function(response) {
		if(response.data.data &&  response.data.data.loginUrl){
		
			$scope.instagram.loginUrl = response.data.data.loginUrl;
			
		}
	}, function(error) {
		console.log(error);
		alert('Error ' + error.data.message);
		
	});
	
	$scope.login = function(instagram){
		apiFactory.post('clearCookie', $scope).then(function(response) {
			
			$window.location.href = $scope.instagram.loginUrl
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
		
	}
	
}
