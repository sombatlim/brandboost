function HomeController($rootScope,$scope, $routeParams, $location, $cookies, $cookieStore, campaignsModel,campaignInterestModel) {
	
	$rootScope.signin = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step20 = false;
	$rootScope.step30 = false;
	$rootScope.step10 = false;
	$rootScope.step21 = false;
	$rootScope.step31 = false;
	$rootScope.stepbar = false;
    $scope.login=false;
    $rootScope.loading=true;

    campaignsModel.checkAuth(campaignsModel.notificationAuth);
	//-- start checkAuth
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { 
		
		if(response.data.data.auth) { 
    	
		$scope.login= true;
		
		campaignsModel.getDashboard(campaignsModel.notificationDashboard);
		
		$scope.$on(campaignsModel.notificationDashboard, function(event, response) {	// This is called on return fromserver based on campaignsModel.notification
			
			$scope.dashdata = response.data.data; 
			//console.log($scope.dashdata.user);
			$rootScope.loading=false;
	    });
		
		campaignsModel.getAllPostedCampaigns(campaignsModel.notificationUpdated);
		
		$scope.$on(campaignsModel.notificationUpdated, function(event, response) {	$scope.campaigns = response.data.data.campaigns; });
		
			} // --end if
	    });

}
	
	