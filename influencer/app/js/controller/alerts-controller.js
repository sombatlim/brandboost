function AlertsController($rootScope, $scope, $routeParams, $location,  alertsModel, $cookies, $interval, campaignsModel) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	//-- start checkAuth
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { 
		
		if(response.data.data.auth) { 
	
	$rootScope.login= true; 
	
	$scope.refreshInterval = 30; // For every 30 sec
	
	$scope.alerts = {};
	
	$scope.follower = 1000;
	
	$interval(function() { 
		
			alertsModel.getAlerts(alertsModel.notificationUpdated);	// Calls to server.			

	}, $scope.refreshInterval * 1000); // the refresh interval must be in millisec		
	
	if($cookies.followed_by == null) { alertsModel.getFollower(alertsModel.notificationGetfollowers); } else { $scope.followed_by = $cookies.followed_by; }	// get followers.
	$scope.$on(alertsModel.notificationGetfollowers, function(event, response) {
		//console.log(response.data.data);
		$cookies.followed_by = response.data.data.followed_by; $scope.followed_by = response.data.data.followed_by;
		if($cookies.instagram_id == null) $cookies.instagram_id = response.data.data.instagram_id;
	});
	
	//initial
	if($cookies.influencer_id==null) $cookies.authfid=response.data.data.authfid;
	alertsModel.getAlerts(alertsModel.notificationUpdated);	// Calls to server.
	$scope.$on(alertsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
			$scope.alerts = response.data.data.alerts;
			if($scope.alerts != null && $scope.alerts.length>0) { $rootScope.alertcss="btn-danger"; } else { $rootScope.alertcss="btn-normal"; } 
			
			});
		}
	});	//-- end auth
	
		
		}
	
	