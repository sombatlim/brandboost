function InterestsController($rootScope,$scope, $routeParams, $location, $window, apiFactory, $cookies, influencersModel, influencerInterestModel, campaignsModel) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	//-- start checkAuth
	$scope.$on(campaignsModel.notificationAuth, function(event, response) {  
		//console.log(response.data.data.auth2);
		
		if(response.data.data.auth2) {
		
		$rootScope.login= true; 
		
		$rootScope.step10 = true;
		$rootScope.step21 = true;
		$rootScope.step30 = true;
		$rootScope.stepbar = true;
		$rootScope.navbar = true;
		
		influencersModel.getPageData(influencersModel.notificationUpdated);	// Calls to server.
		
		}
	});
	
	$scope.influencer = {};
	$scope.interests = {};
	$scope.influencer_interest = {};
	$scope.apiFactory = {};
	$scope.media = {};
	
	$scope.checkIfChecked = function(item){
		var found = false;
		angular.forEach($scope.influencer_interest, function(value, key) {
				//console.log(value);
				if(value.interest_id == item.id){
					
					found = true;
				}
				  
			});
		
		return found;
	}
	
	/************************/
	// Called on return of influencerInterestModel.getPageData();
	$scope.$on(influencerInterestModel.notificationUpdated, function(event, response) {
		
		$scope.influencer_interest =  response.data.data.influencer_interest;
		
		apiFactory.post('getInterests', $scope).then(function(response) {
			
			if(response.data.data){
				$scope.interests = response.data.data;
			}
			
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
	 });
	
	
	/************************/
	// Called on return of influencersModel.getPageData or influencersModel.updateModel
	$scope.$on(influencersModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
		$scope.influencer = response.data.data.influencers; 
		
		influencerInterestModel.data.influencer_id = $scope.influencer.influencer_id;
		influencerInterestModel.getPageData(influencerInterestModel.notificationUpdated);
		
    });
 
	
	$scope.updateInterest = function(item, checkStatus){
		
				
		influencerInterestModel.data.interest_id = item.id;
		influencerInterestModel.data.influencer_id = $scope.influencer.influencer_id;
		
		if(!checkStatus)	//TODO:  Don't know why it's in reverse.  Fix later.
			influencerInterestModel.updateModel();
		else
			influencerInterestModel.deleteModel();
		
		influencersModel.getPageData(influencersModel.notificationUpdated);
		
	}

	$scope.goProfile = function(){
		
		if($scope.influencer_interest) { $location.url('/profile'); } else { $scope.done=true; $scope.status="Please select at least 1 interest."; }
		
	}
	
	
	
}