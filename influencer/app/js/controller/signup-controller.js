function SignupController($rootScope,$scope, $routeParams, $location, $window, apiFactory, influencersModel, influencerInterestModel) {
	$scope.instagram = {};

	console.log('in SignupController');
	
	$scope.apiFactory = {};
	$scope.influencer = {};
	$scope.media = {};
	$scope.error = null;
	
	$rootScope.step11 = true;
	$rootScope.step20 = true;
	$rootScope.step30 = true;
	$rootScope.stepbar = true;
	$rootScope.navbar = true;
	
	
	apiFactory.post('getInstagramLoginUrl', $scope).then(function(response) {
		if(response.data.data &&  response.data.data.loginUrl){
			$scope.instagram.loginUrl = response.data.data.loginUrl;
			console.log(response.data.data.loginUrl);	
		}
	}, function(error) {
		console.log(error);
		alert('Error ' + error.data.message);
		
	});
	
	
	$scope.signup = function(instagram){
		
		$scope.instagram = angular.copy(instagram);
		$scope.apiFactory.email = $scope.instagram.email;
		apiFactory.post('setEmailAddress', $scope).then(function(response) {
			
			//Successfully checked that this email isn't already registered.
			//$cookies.brandboost_email = $scope.apiFactory.email; 
			$window.location.href = $scope.instagram.loginUrl
			
		}, function(error) {
			console.log(error);
			//alert('Error ' + error.data.message);
			$scope.error = error.data.message;
		});
	}
	
	
}


