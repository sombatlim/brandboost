function LogoutController($scope, $routeParams, $location, $window, apiFactory,$cookies, $cookieStore,$http,$timeout) {
	
	console.log('in LogoutController');
	
	$scope.apiFactory = {};
	$scope.influencer = {};
	
	$scope.home = "/";
	
	$scope.error = null;
	
	apiFactory.post('logout', $scope).then(function(response) {
		
		angular.forEach($cookies, function (v, k) {
		    $cookieStore.remove(k);
		});
		
		//$window.location.href = "https://instagram.com/accounts/logout/"; 
		$timeout($scope.logout2bb, 1000);//$window.location.href = "http://www.brandboost.asia/"; //$scope.home;
		 
		
	}, function(error) {
		console.log(error);
		alert('Error ' + error.data.message);
		
	});
	
	$scope.logout2bb = function() {
		$window.location.href = "http://www.brandboost.asia/"
	}
	

}
