function CampaignsController($rootScope, $scope, $routeParams, $location, $window, apiFactory, $cookies, campaignsModel,campaignInterestModel) {
	
	 campaignsModel.checkAuth(campaignsModel.notificationAuth);
		//-- start checkAuth
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { 
		
		if(response.data.data.auth) { 
	
	$rootScope.login= true; 	
	$rootScope.step10 = true;
	$rootScope.step20 = true;
	$rootScope.step31 = true;
	$rootScope.stepbar = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step21 = false;
	$rootScope.step30 = false;
	$rootScope.loading=true;
	
	//alert($location.path());
	if($location.path()=="/requested-campaigns") campaignsModel.getNewCampaigns(campaignsModel.notificationUpdated);
	if($location.path()=="/advertiser-requests") campaignsModel.getAllNewCampaigns(campaignsModel.notificationUpdated);
	if($location.path()=="/posted-campaigns" || $location.path()=="/revenue-earned") campaignsModel.getPostedCampaigns(campaignsModel.notificationUpdated);
	
	/// ----- Get Interests
	if($location.path()=="/create-campaign") {
		
		apiFactory.post('getInterests', $scope).then(function(response) {
			
			if(response.data.data){
				$scope.interests = response.data.data;
			}
			
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
	}
	
		} else { $location.path('/'); }
	
}); // -- end auth
	
	$scope.Math=Math;
	$scope.bonus = 100;  // 100 likes = 1 THB
	$scope.campaign = {};
	var scopeData = {};
	scopeData.apiFactory = {};
	
	$scope.campaign.interests = [];

	
	$scope.done = false;
	$scope.home = "/";
	
	$scope.word = "/^[a-zA-Z0-9]*$/";
	
	
	
	$scope.checkIfChecked = function(item){
		var found = false;
		angular.forEach($scope.campaign.interests, function(value, key) {
				console.log(value);
				if(value.interest_id == item.id){
					
					found = true;
				}
				  
			});
		
		return found;
	}
	
	var xxx = {};
    $scope.adid = [];
	$scope.getInterest = function(advertiser_id) {
		
		var adata = {};
		adata.apiFactory = {};
		var $chk = $scope.adid.indexOf(advertiser_id);
		
		if($chk<0) { 
		$scope.adid.push(advertiser_id);
		adata.apiFactory.data = advertiser_id;
		//console.log(adata);
			apiFactory.post('getInterestsByAdId', adata ).then(function(response) {
				xxx[advertiser_id] = response.data.data;
			});
		
		
		}
		return xxx[advertiser_id];
	}
	
	
	$scope.updateInterest = function(item, checkStatus){
		
		if(!checkStatus) {
			$scope.campaign.interests.push(item.id);
		} else 	{
			index = $scope.campaign.interests.indexOf(item.id)
			$scope.campaign.interests.splice(index, 1);
		
		}
			
		console.log($scope.campaign.interests);
	}
	///--End interests
	
	$scope.$on(campaignsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on campaignsModel.notification
		$scope.campaigns = response.data.data.campaigns; 
		$rootScope.loading=false;
		//console.log($scope.campaigns);

    });
 
	
	$scope.postAd = function(campaign){
		//campaignsModel.data = $scope.campaign;
		//campaignsModel.updateModel();

		$scope.master = angular.copy(campaign);
		console.log($scope.master);
		campaignsModel.data = $scope.master;
		
		campaignsModel.postToInstagram(campaignsModel.notificationUpdated);
		
		$scope.done = "btn-success";
		$scope.status = "Sending the advertiser to your email!";

	}
	
	$scope.$on('uploadAdImage', function(event, data) {	// This is called on return fromserver based on influencersModel.notification
		
		$scope.campaign.advert_image = data.campaign.advert_image; 
		
    });
	
	$scope.saveCampaign = function(){
		
		var chk = false;
		
		if($scope.campaign.advert_image == null) { $scope.status = "Please upload Advertising Image"; 
		
		} else if($scope.campaign.interests.length <= 0) { $scope.status = "Please select at least one interest";
		
		} else if(!$scope.newAd.$valid) { $scope.status = "Invalid Tag , No space allow"; } else { chk= true; }
					
		if(chk) {
			console.log($scope.campaign);
			scopeData.apiFactory.data = JSON.stringify($scope.campaign);
			console.log(scopeData);
			apiFactory.post('saveNewCampaign', scopeData).then(function(response) {
				
				//if(response.data.data){
					
					//alert("DONE");
					$scope.done = "btn-success";
					$scope.status = "New campaign was recorded successfully!";
					
				//}
				
				
			}, function(error) {
				console.log(error);
				$scope.status = error.data.message;
			});

		} else { $scope.done = 'btn-danger'; }
		
	}	
	
	$scope.requestedAd = function(advertiser_id,name){
	
				console.log($scope.campaign);
				scopeData.apiFactory.data = { 'interests' : xxx[advertiser_id],'id': advertiser_id, 'tag': name };
				console.log(scopeData);
				
				apiFactory.post('sendRequest', scopeData).then(function(response) {

						$scope.done = "btn-success";
						$scope.status = "DONE!";
	
				}, function(error) {
					console.log(error);
					alert('Error ' + error.data.message);
				});
				
	}

	$scope.getPostedInfluencers = function(advertiser_id){

		scopeData.apiFactory.data = { 'id': advertiser_id };
		console.log(scopeData);
		apiFactory.post('getPostedInfluencers', scopeData).then(function(response) {
			
			$scope.PostedInfluencers = response.data.data;
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
	
	}
	
	$scope.getTotal = function(){
	    var total = 0;
	    for(var i = 0; i < $scope.campaigns.length; i++){
	        var cprice = $scope.campaigns[i].price;
	        var clikes = $scope.campaigns[i].likes;
	    	//console.log($scope.campaigns[i].price);
	        total += parseInt(cprice) + Math.floor(parseInt(clikes)/$scope.bonus);
	    }
	    return total;
	}
	
	$scope.getAdSummary = function(){
	    var total = [];
	    var status0=0,status1=0,status2=0;
	    for(var i = 0; i < $scope.campaigns.length; i++){
	        var cstatus = $scope.campaigns[i].status;

	        if(cstatus==0) { status0++; } else if(cstatus==1) {status1++;} else if(cstatus==2) {status2++;}
	        
	    }
	    total.push(status0);
	    total.push(status1);
	    total.push(status2);
	    return total;
	}

   
}