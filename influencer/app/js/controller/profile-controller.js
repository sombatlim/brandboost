function ProfileController($rootScope, $scope, $routeParams, $location, $window, apiFactory, $cookies, influencersModel, alertsModel, campaignsModel) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	//-- start checkAuth
	$scope.$on(campaignsModel.notificationAuth, function(event, response) {  
		
		if(response.data.data.auth2) {		
	$rootScope.login= true; 
	influencersModel.getPageData(influencersModel.notificationUpdated);	// Calls to server.
	
		}
	});
		
	$rootScope.login= true; 	
	
	$rootScope.step10 = true;
	$rootScope.step20 = true;
	$rootScope.step31 = true;
	$rootScope.stepbar = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step21 = false;
	$rootScope.step30 = false;

	//}
	
	$scope.influencer = {};
	$scope.done = false;
	$scope.home = "/";
	
	$scope.$on(influencersModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
		$scope.influencer = response.data.data.influencers; 
		if($cookies.influencer_id == null)	$cookies.influencer_id	= $scope.influencer.influencer_id;
		if($cookies.instagram_id == null)	$cookies.instagram_id	= $scope.influencer.instagram_id;
		if($cookies.fullname == null)	$cookies.fullname	= $scope.influencer.fullname;
		//console.log($cookies.influencer_id);
    });
 
	
	$scope.$on('uploadImage', function(event, data) {	// This is called on return fromserver based on influencersModel.notification
		$scope.influencer = data; 
		
    });
	
	
	$scope.saveProfile = function(){
		influencersModel.data = $scope.influencer;
		influencersModel.updateModel();
		
		//alert("DONE");
		$scope.done = true;
		$scope.status = "Your profile has been updated!";
		
		if(influencersModel.data.influencer_id > 0){
			campaignsModel.checkAuth(campaignsModel.notificationAuth);
			$scope.$on(campaignsModel.notificationAuth, function(event, response) {  				
				if(!response.data.data.auth) { $window.location.href = $scope.home; }
			});
		}
	}
	
}