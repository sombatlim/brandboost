app.directive('fileDropzone', function() {
   return {
     restrict: 'A',
     $scope: {
       file: '=',
       fileName: '='
     },
     link: function($scope, element, attrs) {
    	 
    	   var checkSize, isTypeValid, validMimeTypes;
    	       	  
    	   var uploadAction = attrs.uploadAction;
    	   validMimeTypes = attrs.fileDropzone;
    	   
    	  // console.log(attrs.uploadAction);
    	   
    	   checkSize = function(size) {
    	         var _ref;
    	         if (((_ref = attrs.maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < attrs.maxFileSize) {
    	           return true;
    	         } 
    	         alert("File must be smaller than " + attrs.maxFileSize + " MB");
    	         return false;
    	         
    	       };
    	       
    	   checkFileCount = function(count){
    		   
    		   var _ref;
    		   if (((_ref = attrs.fileMaxCount) === (void 0) || _ref === '') || count <= attrs.fileMaxCount) {
    			   return true;
    		   }
    		      
    		   $scope.formUploadMessage = 'Please only upload ' +attrs.fileMaxCount+ ' file';
        	   $scope.formErrorUpload = true;
    		   return false;
    			 
    	   }
    	       
    	       isTypeValid = function(type) {
    	    	
    	         if (type && ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1)) {
    	           return true;
    	         } 
    	         
    	         $scope.formUploadMessage = attrs.fileErrorMessage;
    	         $scope.formErrorUpload = true;
    	         return false;
    	         
    	       };
    	
    	  //============== DRAG & DROP =============
    	    // source for drag&drop: http://www.webappers.com/2011/09/28/drag-drop-file-upload-with-html5-javascript/
    	    var dropbox = document.getElementById("dropbox")
    	    $scope.dropText = $scope.dropTextOk;

    	    // init event handlers
    	    function dragEnterLeave(evt) {
    	        evt.stopPropagation()
    	        evt.preventDefault()
    	        $scope.$apply(function(){
    	            $scope.dropText = $scope.dropTextOk;
    	            $scope.dropClass = ''
    	        })
    	    }
    	    dropbox.addEventListener("dragenter", dragEnterLeave, false);
    	    dropbox.addEventListener("dragleave", dragEnterLeave, false);
    	    
    	    
    	    
    	    dropbox.addEventListener("dragover", function(evt) {
    	        evt.stopPropagation()
    	        evt.preventDefault()
    	        var ok = true;
    	        try {
    	            if(evt.originalEvent.dataTransfer.types.indexOf('Files') == -1){
    	            	ok = false;
    	            }
    	        } catch(E){}

    	        try {
    	            if(!evt.originalEvent.dataTransfer.types.contains("application/x-moz-file")){
    	            	ok = false;
    	            }
    	        } catch(E){}
    	       
    	        $scope.$apply(function(){
    	            $scope.dropText = ok ? $scope.dropTextOk : $scope.dropTextError;
    	            $scope.dropClass = ok ? 'over' : 'not-available'
    	        })
    	    }, false);
    	    
    	    
    	    dropbox.addEventListener("drop", function(evt) {
    	    	$scope.formErrorUpload = false;
    	    	$scope.formOkUpload = false;
    	        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
    	        evt.stopPropagation()
    	        evt.preventDefault()
    	        $scope.$apply(function(){
    	            $scope.dropText = $scope.dropTextOk;
    	            $scope.dropClass = ''
    	        });
    	        
    	        var files = evt.dataTransfer.files
    	        $scope.populateFiles(files);
    	        
    	    }, false);
    	    //============== DRAG & DROP =============
    	    
    	  
    	    $scope.setImages = function(){
    	    	var holder = document.getElementById('holder');
    	    	for (var i in $scope.files) {
    	    		
    	    		 type = $scope.files[i].type;
    	    		 //alert(type + ' -- ' + type.indexOf('image'));
    	    		 if(typeof holder != 'undefined' && type.indexOf('image') >= 0 ){
    	    			 
	    	    		 reader = new FileReader();
	                     reader.onload = function(evt) {
	                       
	                    	if(holder){
		                    	var image = new Image();
		                    	image.src = evt.target.result;
		                    	image.width = 250; // a fake resize
		                    	
		                    	holder.appendChild(image);
	                    	}
	                     };
	                     reader.readAsDataURL($scope.files[i]); 
    	    		 }
    	        }
    	    	
    	    }

    	    $scope.populateFiles = function(files){
    	    	
    	    	if (files.length > 0) {
    	        	
    	        	$scope.$apply(function(){
    	        		
    	        		var hasError = false;
    	        		if(checkFileCount(files.length)){
	    	                $scope.files = []
	    	                for (var i = 0; i < files.length; i++) {
	
	    	                     file = files[i];
	    	                     name = file.name;
	    	                     type = file.type;
	    	                     size = file.size;
	
	    	                     if (checkSize(size) && isTypeValid(type)){
	    	                    	 $scope.files.push(file);	 
	    	                     }else{
	    	                    	 hasError = true;
	    	                    	 
	    	                    	 break; // there was an error, don't process anything.
	    	                     }
	    	                }
	    	                
	    	                
	    	                if(!hasError){
		    	                $scope.setImages();
		    	                console.log('Check uploadOnDrop');
		    	                if($scope.uploadOnDrop){
		    	                	
		    	                	$scope.uploadFile(); // todo, need to wait until the reader onload has finished with everything.
		    	                }else{
		    	                	$scope.formErrorUpload = true; // this is a generic error, tells user to refresh the page and try again.
		        					$scope.formUploadMessage = $scope.lang_imagePreviewMessage;
		    	                }
	    	                }
	    	        	}
    	            })
    	        }
    	    	
    	    }
    	    
    	    
    	    
    	    
    	    
    	    $scope.setFiles = function(element){
    	    	
    	    	$scope.populateFiles(element.files);
    	    	/*
    	    	 $scope.setFiles(element);
    	    	 if($scope.uploadOnDrop)
    	    		 $scope.uploadFile();
    	    		 */
    	    }

    	    /*
    	    $scope.setFiles = function(element) {
    	    	
    	    	
    	    $scope.$apply(function($scope) {
    	      console.log('files:', element.files);
    	      // Turn the FileList object into an Array
    	        $scope.files = []
    	        for (var i = 0; i < element.files.length; i++) {
    	          $scope.files.push(element.files[i])
    	        }
    	        $scope.progressVisible = false
    	      });
    	    };
    	    */

    	    $scope.uploadFile = function() {
    	    	$scope.formErrorUpload = false; 
    	    	$scope.formOkUpload = false;
	    		
    	    	
    	    	if($scope.files && $scope.files.length > 0){
	    	        var fd = new FormData()
	    	        for (var i in $scope.files) {
	    	            fd.append("uploadedFile[]", $scope.files[i]);
	    	        }
	    	        
	    	        if($scope.offer && $scope.offer.offerId){
	    	        	 fd.append("offerId", $scope.offer.offerId);
	    	        }
	    	        
	    	        var xhr = new XMLHttpRequest()
	    	        xhr.upload.addEventListener("progress", uploadProgress, false)
	    	        xhr.addEventListener("load", uploadComplete, false)
	    	        xhr.addEventListener("error", uploadFailed, false)
	    	        xhr.addEventListener("abort", uploadCanceled, false)
	    	        xhr.open("POST", "/api/api.php?act=" + uploadAction)
	    	        $scope.progressVisible = true
	    	        xhr.send(fd)
    	    	}
    	    }

    	    function uploadProgress(evt) {
    	        $scope.$apply(function(){
    	            if (evt.lengthComputable) {
    	                $scope.progress = Math.round(evt.loaded * 100 / evt.total)
    	                
    	                if($scope.progress == 100){
    	                	$scope.formOkUpload = true;
    	                	$scope.formUploadMessage = $scope.processingMessage;
    	                }
    	            } else {
    	                $scope.progress = 'unable to compute'
    	            }
    	        })
    	    }

    	    function uploadComplete(evt) {
    	    	$scope.$apply(function($rootScope, $scope) {
    	    		//$scope.progressVisible = false;
    	    		
    	    		var response = JSON.parse(evt.target.responseText); console.log(evt);
    	    		//$scope.formOkUpload = false;
    	    		//$scope.formUploadMessage = '';
    	    		if(response.status != 0){
    					//$scope.formErrorUpload = true; // this is a generic error, tells user to refresh the page and try again.
    					//$scope.formUploadMessage = response.message;
    					
    				}else{
    					$rootScope.formOkUpload = true;
        	    		$rootScope.formUploadMessage =  response.message + '.jpg';
         					
    					console.log(response.data.influencers);
    					$rootScope.$broadcast(uploadAction, response.data.influencers);
     				}
    	    	});
    	    }

    	    function uploadFailed(evt) {
    	        alert("There was an error attempting to upload the file.")
    	    }

    	    function uploadCanceled(evt) {
    	        $scope.$apply(function(){
    	            $scope.progressVisible = false
    	        })
    	        alert("The upload has been canceled by the user or the browser dropped the connection.")
    	    }
    	   
    	    
    	    
     }
   };
});