
app.service("campaignInterestModel", ['$rootScope', 'apiFactory', 'baseModel', function($rootScope, apiFactory, baseModel) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'campaignInterestModel::updated';
	this.notificationDeleted = 'campaignInterestModel::deleted';
	
	this.data = { 
			campaign_id: null,
			interest_id: null,
	};

	this.getPageData = function(notification){
		baseModel.getModel('campaign_interest', this.data, notification);
	}
	
	
	this.updateModel = function(notification){
		baseModel.updateModel('campaign_interest', this.data, notification);
		
	}
	this.deleteModel = function(notification){
		baseModel.deleteModel('campaign_interest', this.data, notification);
		
	}
    
}]);
	