app.service("baseModel", ['$rootScope', 'apiFactory', function($rootScope,  apiFactory) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	

	this.updateModel = function(model, data, notification){
		scopeData.apiFactory.data = JSON.stringify(data);
		scopeData.apiFactory.model = model;
		apiFactory.post('updateModel', scopeData).then(function(response) {
			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	}
	
	this.deleteModel = function(model, data, notification){
		
		scopeData.apiFactory.data = JSON.stringify(data);
		scopeData.apiFactory.model = model;
		
		apiFactory.post('deleteModel', scopeData).then(function(response) {
			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	}
	
	this.getModel = function(model, data, notification){
		scopeData.apiFactory.data = JSON.stringify(data);
		scopeData.apiFactory.model = model;
		apiFactory.post('getModel', scopeData).then(function(response) {
			
			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
		
	}
	
	    
}]);