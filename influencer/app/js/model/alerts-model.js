app.service("alertsModel", ['$rootScope', 'apiFactory', 'baseModel', '$cookies', function($rootScope, apiFactory, baseModel, $cookies) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'alertsModel::updated';
	this.notificationDeleted = 'alertsModel::deleted';
	this.notificationGetfollowers = 'alertsModel::getfollowers';
	
	
	this.data = { 
			alert_id: null,
			influencer_id: null,
			message: null,
			read: null,
	};

	this.updateModel = function(notification){
		baseModel.updateModel('alerts', this.data, notification);
	}
	
	this.getAlerts = function(notification){
		//this.data.influencer_id = 68;	 // TODO, this needs to be set in session
		var id = $cookies.influencer_id;
		if(id==null) id=$cookies.authfid;
		this.data.influencer_id = id;
		this.data.status = 0;
		baseModel.getModel('alerts', this.data, notification);
	}
	
	this.getFollower = function(notification){
		
			apiFactory.post('getFollower', scopeData).then(function(response) {
				//console.log(response.data.data.followed_by);
				if(notification != undefined)
					$rootScope.$broadcast(notification, response);				
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});
			
		}
	
	

	    
}]);