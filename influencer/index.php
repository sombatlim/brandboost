<?php 
require_once 'api/db.php';
session_start();

$cookieObject = cCookie::getCookie();

?>
<!DOCTYPE html>
<html lang="en" xmlns:ng="http://angularjs.org" id="ng-app" ng-app='myApp' >

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Brand Boost Influencer</title>

    
    <!-- Bootstrap Core CSS -->
    <link href="/bootstrap-template/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/bootstrap-template/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/bootstrap-template/css/plugins/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/bootstrap-template/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/bootstrap-template/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/bootstrap-template/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/app/css/style.css" rel="stylesheet">

</head>

<body>
<div ng-show="loading" id="load-screen"></div>
<?php 

if (isset($cookieObject->email) && isset($cookieObject->influencer_id)) { 

?>
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/logo.png"></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
            
            <li ng-cloak class="alert-text" ng-show="followed_by" ng-controller="AlertsController">You have {{followed_by}} followers less than {{follower}}, you are not influencer!</li>
            
                 <li class="dropdown" ng-cloak ng-controller="MessagesController">
                    <a class="dropdown-toggle" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="badge" ng-style="msgStyle">{{total[0]}}</i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages" ng-if="total[0]>0">
                        <li ng-repeat="message in messages" ng-if="message.status==0 && message.msgto==bbid">
                            <a href ng-click="readMessage1(message)">
                                <div>
                                    <strong>{{message.full_name}}</strong>
                                    <span class="pull-right text-muted">
                                        <em>{{message.createddate | moment: 'fromNow' }}</em>
                                    </span>
                                </div>
                                <div><strong>{{message.subject}}</strong></div>
                                <div ng-if="message.id==readid">{{message.message}}</div>
                            </a>
                            
                        </li>
                        <li class="divider"></li>
                       
                        <li>
                            <a class="text-center" href="/inbox">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
  				
                <li class="dropdown" ng-cloak ng-controller="AlertsController">
                    <a  class="{{alertcss}}" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i><i class="badge">{{alerts.length}}</i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" ng-show="alerts.length > 0" >
                    	<li ng-repeat="alert in alerts">
                    		<a ng-if="alert.message == 'New Campaign Request' " href="/requested-campaigns">
                    			<i class="fa fa-comment fa-fw"></i> {{alert.message}} <span class="pull-right text-muted small">{{ alert.createddtm | moment: 'fromNow' }}</span>
                    		</a>
                    		<span class="divider"></span>
                    	</li>
                                               
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="/profile"><i class="fa fa-user fa-fw"></i> Profile</a>
                        </li>
                        <li><a href="/interests"><i class="fa fa-table fa-fw"></i> Interests</a>
                        </li>
                       
                        <li class="divider"></li>
                        <li><a href="/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                 
                        <li>
                            <a class="active" href="/"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                          <li>
                            <a href="/campaigns"><i class="fa fa-edit fa-fw"></i> Campaigns<span class="fa arrow"></span></a>
                             <ul class="nav nav-second-level">
                             	<li>
                                    <a href="requested-campaigns"> Requested Campaigns</a>
                                </li>
                                <li>
                                    <a href="posted-campaigns"> Posted Campaigns</a>
                                </li>
                               
                            </ul>
                        </li>
                       
                        
                      
                       <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                             	<li>
                                    <a href="/revenue-earned"> Revenue Earned</a>
                                </li>
                                <li>
                                    <a href="/advertiser-requests"> Advertiser Requests</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        
                        <li ng-cloak ng-controller="MessagesController">
                            <a href="#"><i class="fa fa-envelope-o fa-fw"></i> Messages<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                             	<li>
                                    <a href="/inbox"> Inbox ({{total[0]}} new)</a>
                                </li>
                                <li>
                                    <a href="/outbox"> Send Message ({{total[1]}})</a>
                                </li>
                               
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

       <div id="page-wrapper" ng-cloak  ng-view></div>

    </div>

<?php 
} else {
?>
    <div  id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
         <span ng-show="signin" style="float:right; margin:15px 10px 0 0;"><a  href="/signup">Sign up</a> | <a  href="/login">Login</a></span>
        <div ng-show="navbar" class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/logo.png"></a>
               
        </div>
  
        </nav>
		 <div class="navbar-default sidebar" role="navigation">
			<div style="margin:0 auto;" class="activecircle" ng-cloak ng-show="step11">Step 1</div>
			<div style="margin:0 auto;" class="circle" ng-cloak ng-show="step10">Step 1</div>
			<div style="margin:0 auto;" class="bar" ng-cloak ng-show="stepbar"></div>			
			<div style="margin:0 auto;" class="activecircle" ng-cloak ng-show="step21">Step 2</div>
			<div style="margin:0 auto;" class="circle" ng-cloak ng-show="step20">Step 2</div>
			<div style="margin:0 auto;" class="bar" ng-cloak ng-show="stepbar"></div>
			<div style="margin:0 auto;" class="activecircle" ng-cloak ng-show="step31">Step 3</div>
			<div style="margin:0 auto;" class="circle" ng-cloak ng-show="step30">Step 3</div>
			
		</div>
       <div id="page-wrapper" ng-cloak ng-view></div>

    </div>
<?php 
 }
?>

<!-- jQuery -->
    <script src="/bootstrap-template/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/bootstrap-template/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/bootstrap-template/js/plugins/metisMenu/metisMenu.min.js"></script>



    <!-- Custom Theme JavaScript -->
    <script src="/bootstrap-template/js/sb-admin-2.js"></script>
    
    <script src="/dist/js/moment.min.js"></script>
     
    <!-- AngularJS
    ================================================== -->
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js"></script>
  	<script src="/dist/angularjs/1.2.17/angular-route.js"></script>
  	<script src="/dist/angularjs/1.2.17/angular-cookies.js"></script>
    
    <!-- DIST MODULES
    ================================================== -->
    <script src="/dist/angularjs/ng-progress.js"></script>
    <script src="/dist/angularjs/ng-infinite-scroll.js"></script>
    <script src="/dist/angularjs/angular-translate.js"></script>
	<script src="/dist/angularjs/ui/ui-utils.js"></script>
    <script src="/dist/angularjs/ui-bootstrap-tpls-0.11.0.js"></script>
    
    <script src="/app/js/app.js"></script>
    <script src="/app/js/route.js"></script>
    
    <script src="/app/js/controller/home-controller.js"></script>
    <script src="/app/js/controller/signup-controller.js"></script>
    <script src="/app/js/controller/login-controller.js"></script>
    <script src="/app/js/controller/logout-controller.js"></script>
    <script src="/app/js/controller/profile-controller.js"></script>
    <script src="/app/js/controller/interests-controller.js"></script>
    <script src="/app/js/controller/instagram-controller.js"></script>
    <script src="/app/js/controller/alerts-controller.js"></script>
    <script src="/app/js/model/campaign-interest-model.js"></script>
    <script src="/app/js/controller/campaigns-controller.js"></script>
    <script src="/app/js/controller/messages-controller.js"></script>
    
    <script src="/app/js/factory/api-factory.js"></script>
    
    <script src="/app/js/model/base-model.js"></script>
    <script src="/app/js/model/influencers-model.js"></script>
    <script src="/app/js/model/influencer-interest-model.js"></script>
    <script src="/app/js/model/alerts-model.js"></script>
    <script src="/app/js/model/campaigns-model.js"></script>
    <script src="/app/js/model/messages-model.js"></script>
    
    <script src="/app/js/directive/file-upload.js"></script>
    
    
    
    
    <script>
     	<?php if(isset($config['xdebugurl'])):?>
       	window.xdebug = '<?php echo $config['xdebugurl'];?>';
       	<?php else: ?>
       	window.xdebug = 0;
       	<?php endif;?>
    </script>

</body>

</html>

