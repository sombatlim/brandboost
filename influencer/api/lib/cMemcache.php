<?php


/*

http://www.dotdeb.org/2008/08/25/storing-your-php-sessions-using-memcached/


; Use memcache as a session handler
session.save_handler=memcache
 ; Defines a comma separated of server urls to use for session storage
session.save_path="tcp://localhost:11211?persistent=1&weight=1&timeout=1&retry_interval=15″

*/


class cMemcache extends Memcache{
	
	
	
	/*
		http://docstore.mik.ua/orelly/webprog/pcook/ch05_06.htm
		While static variables retain their values between function calls, they do so only during one invocation of a script. 
		A static variable accessed in one request doesn't keep its value for the next request to the same page.
	*/
	public static $sessionKey = null;	// This will not be shared accross multiple requests.  Refer to above statement.
	
	public function __construct($config)
	{
		try
		{
			if( parent::addServer($config['memcache_ip'], $config['memcache_port']) == FALSE )
				throw new Exception('Failed to connect to memcache server');
		}
		catch( Exception $e )
		{
			throw new Exception($e->getMessage(), $e->getCode());
		}
	}	
	public function __destruct()
	{
		parent::close();
	}
	
	// default expires after one day
	public function set($key, $obj, $expire = 86400)
	{
		if(parent::set($key, $obj, MEMCACHE_COMPRESSED, $expire) == FALSE )
		{
			; //error	
		}
	}
	
	/*
		http://blog.simonholywell.com/post/374207622/installing-apc-and-memcached-for-php-sessions-on-redhat
		; Use memcache as a session handler
		session.save_handler=memcache
		 ; Defines a comma separated of server urls to use for session storage
		session.save_path="tcp://localhost:11211?persistent=1&weight=1&timeout=1&retry_interval=15″
	*/
	public static function getSession( $localSessionKey )
	{
		
		if( isset($_SESSION[$localSessionKey]) )
			return $_SESSION[$localSessionKey];
		else
			return NULL;
		/*
		
		global $memcache;
		
		if( cMemCache::$sessionKey && $memcache )
		{
			$sessionObj = $memcache->get(cMemCache::$sessionKey);

				
			if(is_array($sessionObj) && array_key_exists($localSessionKey, $sessionObj))
				return $sessionObj[$localSessionKey];
		}
		return NULL;
		*/
	}
	
	public static function deleteSession( $localSessionKey )
	{
		
		$_SESSION[$localSessionKey] = NULL;
		unset($_SESSION[$localSessionKey]);
		/*
		global $memcache;
		if( cMemCache::$sessionKey && $memcache )
		{
			global $memcache;
			$sessionObj = $memcache->get(cMemCache::$sessionKey);
	
			unset($sessionObj[$localSessionKey]);
			
			$memcache->set(cMemCache::$sessionKey, $sessionObj, SESSION_EXPIRE);
		
		}	
		*/
	}
	
	public static function setSession( $localSessionKey, $sessionVal )
	{
		if( strlen($localSessionKey) > 0 )
			$_SESSION[$localSessionKey] = $sessionVal;
		/*
		global $memcache;	

		if( cMemCache::$sessionKey && $memcache && $localSessionKey )
		{
			$sessionObj = $memcache->get(cMemCache::$sessionKey);
			 
			if( !$sessionObj )
			{
				$sessionObj = array();
				$sessionObj[$localSessionKey] =  $sessionVal;
			}
			else
			{
				$sessionObj[$localSessionKey] =  $sessionVal;
			}
			
			$memcache->set(cMemCache::$sessionKey, $sessionObj, SESSION_EXPIRE);
		}
		*/
	}
	
	
	public static function generateKey()
	{
		return (
				isset($_SERVER['REMOTE_ADDR']) && strlen($_SERVER['REMOTE_ADDR'])> 0 && 
				isset($_SERVER['HTTP_USER_AGENT']) && strlen($_SERVER['HTTP_USER_AGENT']) > 0
				) 
				? md5('SESS_'.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'].time()) 
				: NULL;
	}
	
	
	public static function startSession()
	{
		//session_start();
		/*
		cMemCache::$sessionKey = NULL;
		
		
		if( $key = cCommon::getCookie('sessionKey') )
		{
			cMemCache::$sessionKey = $key;

		}
		else
		{
			cMemCache::$sessionKey = cMemCache::generateKey();
		}
		
		
		if( strlen(cMemCache::$sessionKey) > 0)
		{
			setcookie('sessionKey', cMemCache::$sessionKey, time() + SESSION_EXPIRE, '/' );

		}
		else
		{
			//die('invalid key');	
		}
		
		global $memcache;
		if( cMemCache::$sessionKey && $memcache )
		{
			$sessionObj = $memcache->get(cMemCache::$sessionKey);
			
				
			if(is_array($sessionObj) && array_key_exists(cMemCache::$sessionKey, $sessionObj))
			{
				$memcache->set(cMemCache::$sessionKey, $sessionObj, SESSION_EXPIRE); // reset the session timeout.	
			}	
		}
		*/
		
			
	}
	
}