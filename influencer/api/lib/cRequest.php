<?php 
class cRequest{
	
	public $postdata;
	public $getdata;
		
	function __construct(){
		
		global $lang;
		$this->getdata = $_GET;
		$this->postdata = $this->getPostOrInput();
		
		
		// TODO: need to check against SQL injection
		
		if($errorkey = $this->validate_input($this->postdata)) {
			throw new cException($lang['errorInvalidInput'], cStatus::$INVALID_ACTION);
		}
		if($errorkey = $this->validate_input($this->getdata)) {
			throw new cException($lang['errorInvalidInput'], cStatus::$INVALID_ACTION);
		}
		
		
		
		
		
		// check valid email
		
	}
	
	
	public function getPostOrInput(){
	
		$postdata1 = array();
		$postdata2 = array();
		
		
		if(isset($_POST)){
			$postdata1 = $_POST;
		}
		
		if(isset($GLOBALS['HTTP_RAW_POST_DATA'])){
		
			$data = file_get_contents("php://input");
			
			if(!is_null($data) && strlen($data) > 0){
				// sent from angularjs app.  HTTP_RAW_POST
				
				$request = json_decode($data);
			
				$postdata2 = (array) $request;
			}
		}
		$postdata = array_merge($postdata1, $postdata2);
		
		return $postdata;
	
	}
	

	function validate_val( $key, $val ){
		
		if( stristr($val, '<script' ) ){
			return $key;
		}
		if( stristr($val, '<a' ) ){
			return $key;
		}
		if( stristr($val , 'javascript:' ) ){
			return $key;
		}
		// check for html input
		if( preg_match('/(<\/)|(<\/.+?>)|(<.+?\/.+?>)|(<.+?\/>)|(<\/>)/ism', stripslashes($val) ) ){
			return $key;
		}
		// check for href input
		if( preg_match('/(.+?<a.+?href.+?)|(<a.+?href.+?)/ism', stripslashes($val) ) ){
			return $key;
		}
	
	}
	function validate_input( $input ){
	
		$error = null;
		if( is_array($input) && sizeof($input) > 0 ){
			foreach($input as $key => $val ){
				if( is_array($val) ){
					foreach($val as $k => $v ){
						$x = $this->validate_val($k, $v);
						if( strlen($x) > 0 ){
							$error = $x;
							break;
						}
					}
				}else if( is_string($val) && strlen($val) > 0){
					$x = $this->validate_val($key, $val);
					if( strlen($x) > 0 ){
						$error = $x;
						break;
					}
				}
			}
		}
		if( $error != null ){
			
		}
		return $error;
	}
	
	function validate_mysql_date( $date ){
		$error = true;
		if(eregi('^[0-9][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$', $date)){
			$error = false; // IS OK.
		}
		if( $error ){
			
		}
		return $error;
	}
	
	function validate_email( $email )
	{
		$error = true;
	
		if(strlen($email) < 0)
			return $error;
	
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			return $error;
		}
	
		$error = false;
		/*
		if( eregi('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$', $email))	{
			$error = false; // Is OK
		}
	
	
	
		if( $error ){
			//cLog::write(PRI_INFO, sprintf('(%s) Invalid email entered %s', __FUNCTION__, $email ) );
		}
		*/
		return $error;
	}
	
	
	public static function validate_webaddress( $url, $useStrict = false ){
		$error = true;
		$checkUrl = parse_url($url);
		if(!isset($checkUrl['scheme']) || strlen($checkUrl['scheme']) == 0 || ($checkUrl['scheme'] != 'http' && $checkUrl['scheme'] != 'https' && $checkUrl['scheme'] != 'ftp') ){
			$url = 'http://'.$url;
		}
	
		//$checkUrl = parse_url($url);
		//echo $checkUrl['host'].'--';
	
		if(!filter_var($url, FILTER_VALIDATE_URL)){
			return $error;
		}
	
		$v = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
		return !(bool)preg_match($v, $url);
	
	}
	
	
	
}
?>