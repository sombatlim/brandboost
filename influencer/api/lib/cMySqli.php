<?php
/*
	$Id: //depot/ECI/LinuxSys/dev/prj/webdocs/src/cfg/webroot/phplib/cMySqli.class.php#4 $
 */
/*
// - - - - - - - - - - - - - - - -  - - - - - EXAMPLES - - - - - - - - - - - - - - - - - - - - - - - 

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// 
// Connect to the DB
//
	try
	{
		$db = new cMySqli("WagonView");
	}
	catch( Exception $e )
	{
		echo  $e->getMessage();
		exit(1);
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
// Get multiple result sets stored procedure
//
	if( ($result = $db->multi_query("CALL p_MultipleResultSets( %d, %d, '%s' )", param1, param2, param3 )) == FALSE )
	{
		echo $db->last_error();
	}
	else if( $result )
	{
	
		do 
		{
			while($row = $result->fetch_assoc()) 
			{
				printf("%s<br/>", var_dump($row));
			}
			//$result->free();  // not sure if needed
	
			if ($db->more_results()) 
			{
				printf("-----------------<br/>");
			}
		} 
		while( ($result = $db->next_result_set()) );
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
// Get a single result set stored procedure
//
	if( ($result = $db->multi_query( "CALL p_SingleResultSet( '%s', '%s' )", param1, param2 )) == FALSE ) 
	{
		echo $db->last_error();
	}
	else if( $result ) 
	{
	
		while($row = $result->fetch_row()) 
		{
			printf("%s<br/>", var_dump($row));
		}
		$result->free();
	}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
//
// A normal query
//
	if ( ($result = $db->query( "select * from table where id = %d", idval )) == FALSE )
	{
		echo $db->last_error();	
	}
	else if( $result )
	{
		while( $row = $result->fetch_assoc() ) 
		{
			printf("%s", var_dump($row));
		}
		$result->free();
	}

//
// EXAMPLE CLASS 
//
	class xml_meeting extends cMySqli
	{
		
		public function __construct( $mtg )
		{
			try
			{
				parent::cMySqli("WagonView");
			}
			catch( Exception $e )
			{
				echo $e->getMessage();
				return FALSE;
			}
		}
		
		public function get_something()
		{
			$query = "select * from rdb.Race" ;
			if( ($result = $this->query($query)) == FALSE )
			{
				echo $this->last_error();
				return FALSE;
			}
			else if( $result  )
			{
				while( $row = $result->fetch_assoc() )
				{
					printf("%s<br/>", var_dump($row));
				}
				$result->free();
			}
		}
	}

	$xmls = new xml_meeting( 63 );
	$xmls->get_something();
	
*/

define( 'NO_RESULT', 1 );
//mysqli_debug("d:t:0,/media/client.trace");


class cMySqli extends mysqli
{
	protected $last_error = null
	;
	//public $affected_rows = 0;

			
	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	public function cMySqli( $config ){
		try{
			
			
			@parent::mysqli($config['host'], $config['user'], $config['password'], $config['dbname'] );

			if( mysqli_connect_errno() ) 
			{
				throw new Exception(mysqli_connect_errno());
			}
		}
		catch( Exception $e ){
			
			$this->last_error = sprintf('Connection failed: '. mysqli_connect_errno().' '.mysqli_connect_error()) ;
			throw new Exception( $this->last_error );
		}
	}
	
	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	public function __destruct() 
	{
		if( $this )
		{
			@$this->close();
		}
	}
	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	public function close_connection() 
	{
		if( $this )
		{
			@$this->close();
		}
	}
	

	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	public function last_error()
	{
		return $this->last_error;
	}
	
	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	private function process_args( $args,  $zeroidx = true )
	{
		try
		{
			$numargs = sizeof($args);
			
			$idx = ($zeroidx) ? 0 : 1;
			
			$format  = $args[$idx];
			$arglist = array();
			$query 	 = "";
			
			for( $idx = ($idx+1); $idx < $numargs; $idx++ )
			{
				$arg = $args[$idx];
	
				if(is_string($arg))
				{
					$arg = parent::real_escape_string($arg);	
				}
				array_push( $arglist, $arg );
			}
			
		
			$query = ( sizeof( $arglist ) > 0 ) ? vsprintf( $format, $arglist ) : $format;
			if( strlen($query) == 0 )
			{
				
				throw new Exception( sprintf('(%s) Invalid query length of (0)', __FUNCTION__) );
			}
		}
		catch( Exception $e )
		{
			throw new Exception( $e->getMessage() );
		}
	
		
		return $query;
		
	}
	
	/* ------------------------------------------------------------------------
	Execute the query and store the result set into $result. 
	--------------------------------------------------------------------------*/		
	public function query()
	{
		try{
		 	
			$query = $this->process_args( func_get_args() );

			if( ($result = parent::query($query, MYSQLI_STORE_RESULT)) == FALSE ){
				throw new Exception('query error '.$this->errno.' '.$this->error );
			}
			

			return $result;
			
		}
		catch( Exception $e ){
			$this->last_error = $e->getMessage();
			throw new cException('Invalid database action', cStatus::$QUERY_ERROR, $e->getMessage());
		}
		
	}

	public function prepare_statement($query){
		try{
			$stmt = null;
			
			if( ($stmt = $this->prepare($query)) == FALSE ){
				throw new Exception('query error '.$this->errno.' '.$this->error );
			}
			return $stmt;
		}
		catch( Exception $e ){
			$this->last_error = $e->getMessage();
			throw new cException('Invalid database action', cStatus::$QUERY_ERROR, $e->getMessage());
		}
		
	}
	
	public function execute_statement($stmt){
		
		try{
			if( $stmt->execute() == FALSE ){
				throw new Exception('query error '.$this->errno.' '.$this->error );
			}
			/*$result = $stmt->get_result();
			
			return $result;*/
			
		}
		catch( Exception $e ){
			$this->last_error = $e->getMessage();
			throw new cException('Invalid database action', cStatus::$QUERY_ERROR, $e->getMessage());
		}
	}
	

	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/		
	public function multi_query()
	{
		try
		{
			$storeresult = ( func_get_arg(0) == NO_RESULT ) ? false : true;
			
			$query = $this->process_args( func_get_args(), $storeresult );
			
			if( ($result = parent::multi_query($query)) == FALSE )
				throw new Exception('query error '.$this->errno.' '.$this->error );
			
			if( $storeresult ){
				$result = parent::store_result();
			}
			
			return $result;
		}
		catch( Exception $e ){
			$this->last_error = $e->getMessage();
			throw new cException('Invalid database action', cStatus::$QUERY_ERROR, $e->getMessage());
		}
	}

	/* ------------------------------------------------------------------------
	--------------------------------------------------------------------------*/
	public function next_result_set()
	{
		try
		{
			$result = FALSE;
			if( parent::next_result() )
			{
				if( ($result = parent::store_result()) == FALSE )
				{
					if(	$this->errno != 0 )
					{
						throw new Exception(sprintf('errno(%s) %s', $this->errno, $this->error ));	
					}
				}
				
				return $result;
			}
			return FALSE;
		}
		catch( Exception $e )
		{
			$this->last_error = $e->getMessage();
			//cLog::write(PRI_FATAL, $this->last_error );
			return FALSE;
		}
	}
	
	
	
}


?>