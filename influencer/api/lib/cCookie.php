<?php



/* 
 * usage:
 * 
 * to write cookie
 * 	$cookieObject = cCookie::getCookie();
 * 	$cookieObject->email = 'ajbarry99@gmail.com';
 * 	cCookie::setCookie($cookieObject);
 * 
 * 
 * to get cookie.
 * 
 * 	$cookieObject = cCookie::getCookie();
 * 	$email = $cookieObject->email;
 * 
 
 * 
 * 
 */


class cCookie{
	
	public static $encryptKey = 'slkj909823sdssdf23sllsl';
	
	public static $cookieKey = 'bb_data__883737328';
	
	public static function setCookie($cookieObject){
		
		$expire = 60 * 60 * 24 * 500;
		
		$data =  serialize($cookieObject);
		setcookie(cCookie::$cookieKey,cUtils::encrypt($data, cCookie::$encryptKey), time() + $expire, '/', '.brandboost.asia');
	}
	
	public static function getCookie(){
	
		//global $clientApiKey;
	
		$cookieObject = new stdClass();
		if(isset($_COOKIE[cCookie::$cookieKey])){
			$data = cUtils::decrypt($_COOKIE[cCookie::$cookieKey], cCookie::$encryptKey);
			$cookieObject = unserialize($data);
				
		}
		return $cookieObject;
	}
	
	public static function resetCookie(){
		setcookie(cCookie::$cookieKey,'');
	}
	
}
	
	?>