<?php


class cAccount{
	
	const LOGIN_SUCCESS = 0;
	const SIGNUP_ACCOUNT_SUCCESS = 0;
	const ACCOUNT_UPDATE_SUCCESS = 0;
	
	const LOGIN_FAILED = 1;
	const LOGIN_BAD_USER_PASS = 2;
	const SIGNUP_ACCOUNT_EXISTS = 3;
	const SIGNUP_ACCOUNT_FAILED = 4;
	const ACCOUNT_UPDATE_FAILED = 5;
	const ACCOUNT_NOT_EXIST = 6;
	const INVALID_DATA = 7; 
	
	
	
	public static function authorize(){
	
		if(!cAccount::isLoggedIn()){
			throw new cException('Not authorized - no login ID', 403);
		}
	
		if(!isset($_SESSION['defaultLanguage']) || ($_SESSION['defaultLanguage'] != 'en_US' && $_SESSION['defaultLanguage'] != 'th_TH'))
			throw new cException('Not authorized - no default language ('.$_SESSION['defaultLanguage'].')', 403);
	
	}
	
	public static function initLanguage(){
		

		global $config;
		
	
		$_SESSION['lang'] = '';
		$_SESSION['defaultLanguage'] = $config['default_language'];
		
		if($_SESSION['defaultLanguage'] != 'en_US' && $_SESSION['defaultLanguage'] != 'th_TH'){
			die('default language not set in config.ini');
		}
		
		$cookieObject = cCookie::getCookie();
		if(isset($cookieObject) && ($cookieObject->lang == 'en_US' || $cookieObject->lang == 'th_TH')){
			$_SESSION['lang'] = $cookieObject->lang;
		}
		
		
		
		if($_SESSION['lang'] != 'en_US' && $_SESSION['lang'] != 'th_TH'){
			$_SESSION['lang'] = $_SESSION['defaultLanguage'];
		}
	
		
	}
	
	public static function isLoggedIn(){
		return (isset($_SESSION['merchantId']) && (int)$_SESSION['merchantId'] > 0 );
	}
	
	
	public static function logout(){
		cMerchant::destroySessionData();
		cCookie::resetCookie();
	}
	
	public static function checkResetLink($request, /*$matchSessionId,*/ &$message){
		
		global $db;
		global $lang;
		global $config;
		
		if(!isset($request->getdata['resetToken']) || strlen(trim($request->getdata['resetToken'])) == 0){
			throw new cException('Invalid reset token', cStatus::$INVALID_ACTION);
		}
		
		$query = 'select r.*, a.contact_email 
					from pc_merchants.reset_password r 
					join pc_merchants.accounts a on a.merchant_id = r.merchant_id  
					where r.reset_token = "'.$request->getdata['resetToken'].'" 
					AND r.created >= NOW() - INTERVAL 1 DAY';
		
		if($result = $db->query($query)){
			if( $row = $result->fetch_assoc() ){
				/*if($matchSessionId && $row['merchantId'] != $_SESSION['merchantId']){ // this might be someone from a direct link.. 
					throw new cException($lang['resetLinkNoLongerValid'],  cStatus::$INVALID_ACTION);
				}*/
				$data['merchantId'] = $row['merchant_id'];
				$data['contactEmail'] = $row['contact_email'];
				$_SESSION['passwordReset'] = true;
				return $data;
			}else{
				throw new cException($lang['resetLinkNoLongerValid'],  cStatus::$INVALID_ACTION);
			}
			
		}
		
		throw new cException($lang['resetLinkNoLongerValid'],  cStatus::$INVALID_ACTION);
	}
	
	
	public static function sendResetPasswordLink($request, $merchantId, &$message){
		
		global $db;
		global $lang;
		global $config;
		
		$query = 'SELECT merchant_id,
						 contact_title as contactTitle,
						 contact_name_first as contactNameFirst,
						 contact_name_last as contactNameLast,
						 contact_email as contactEmail,
						 default_language as defaultLanguage
				FROM pc_merchants.accounts 
				WHERE ';
		
		if($merchantId > 0){
			$query .= ' merchant_id = '.$merchantId;
		}else if(isset($request->postdata['contactEmail'])){
			if(trim(strlen($request->postdata['contactEmail'])) == 0)
				throw new cException('Must enter email', cStatus::$INVALID_ACTION);
			$query .= ' contact_email = "'.$request->postdata['contactEmail'].'"';
		}
		
		if($result = $db->query($query)){
			if( $row = $result->fetch_assoc() ){
				
				$resetToken = sha1(uniqid($merchantId.$row['contactEmail'].rand()));
				
				$resetLink = $config['base_url'].'/PWReset/'.$resetToken;
				
				$stmt = $db->prepare_statement('call pc_merchants.sp_insert_reset_token(?, ?)');
				
				$stmt->bind_param('si'
						,$resetToken
						,$row['merchant_id']
				);
					
				$db->execute_statement($stmt);
				
				// Send email
				
				require_once '/home/tuktuk/www/api/lib/cEmail.php';
				$email = new cEmail($config, $lang['resetPasswordSubject'], $row);
				$email->loadTemplate('/home/tuktuk/www/app/view/email/resetpassword_'.$row['defaultLanguage'].'.html');
				$email->setCustomKey('{resetToken}', $resetLink);
				$email->sendSES();
				
				$message = $lang['resetLinkSent'];
				
			}else{
				
				throw new cException($lang['errorEmailNotExist'],  cStatus::$INVALID_ACTION);
			}
		}
		return cStatus::$OK;
		
	}
	public static function getAccountDetails($merchantId, $getAllDetails = false){

		global $db;
		global $lang;
		
		$query = 'SELECT  
						merchant_id as merchantId,
						company_name as companyName,
						web_address as webAddress,
						store_phone as storePhone,
						store_name as storeName,
						store_email as storeEmail,
						store_logo as storeLogo,
						REPLACE(store_logo, ".jpg", "") as profileImage,
						store_introduction as storeIntroduction,
						shopping_mall as shoppingMall,
						address1,
						address2,
						city,
						state,
						country_code as countryCode,
						post_code as postCode,
						default_language as defaultLanguage,
						';
		
		if($merchantId == $_SESSION['merchantId'] || $getAllDetails){
			$query .= '
						contact_title as contactTitle,
						contact_name_first as contactNameFirst,
						contact_name_last as contactNameLast,
						contact_email as contactEmail,
						';
		}
		
		
		$query = rtrim(trim($query), ',');
		
		$query .= ' FROM pc_merchants.accounts WHERE merchant_id = '.$merchantId;
		
		
		if($result = $db->query($query)){
			if( $row = $result->fetch_assoc() ){
				return $row;
			}
		}
		
		
		return null;
	}
	
	public static function checkEmailExists($email, &$message){
		
		global $db;
		global $lang;
		$status = 0;
			
		
		$email = trim($email);
		
		if(!isset($email) || strlen($email) == 0 ){
			throw new cException('No email supplied');
		}
		
		/*if(isset($_SESSION['email']) && $email == $_SESSION['email']){
			return $status; // no need to check, this email address belongs to this user.
		}*/
		
		$query = '	SELECT *
					FROM brandboost_app.influencers
					WHERE email = "'.$email.'"';
		
		
		if($result =  $db->query($query)){
			if( $row = $result->fetch_assoc() ){
				throw new cException('Email already exists');
			}
		}
		
		
		return $status;
	}
	
	public static function login($request, &$message){
		
		global $db;
		global $lang;
		

		
		if(	!isset($request->postdata['contactEmail']) || 
			!isset($request->postdata['password']) || 
			strlen($request->postdata['contactEmail']) == 0 || 
			strlen($request->postdata['password']) == 0 ){
			throw new cException($lang['errorInvalidEmailPassword'], cStatus::$INVALID_ACTION);
		}
		
		
		
		
		/*$query = '	SELECT * 
					FROM pc_merchants.accounts 
					WHERE contact_email = "'.$request->postdata['contactEmail'].'"';
					
		
		if(($result =  $db->query($query)) == FALSE ){
			cLog::write(PRI_ERROR,  sprintf('(%s) %s', __FILE__, $db->error )  );
			throw new Exception('db error '. __CLASS__.'::'.__FUNCTION__ );
		}
		else if( $result )
		{
			if( $row = $result->fetch_assoc() ){
				*/
				$query = '	SELECT * 
							FROM pc_merchants.accounts 
							WHERE contact_email = "'.$request->postdata['contactEmail'].'" 
							AND password = "'.$request->postdata['password'].'"';
				
				if( $result =  $db->query($query) ){
					
					if( $row = $result->fetch_assoc() ){
						$cookieObject = cCookie::getCookie();
						$cookieObject->email = $row['contact_email'];
						cCookie::setCookie($cookieObject);
						cMerchant::setupSessionData($row);
						
					}else{
						// Account exists, but the password is invalid.
						throw new cException($lang['errorInvalidEmailPassword'], cAccount::LOGIN_BAD_USER_PASS);
					}
				}
				
				/*
			}
			else{
				// Account doesn't exist
				return cAccount::ACCOUNT_NOT_EXIST;
			}
		}*/
		
		
		$message = 'Login Success';
		return cAccount::LOGIN_SUCCESS;
		
	}
	
	
	public static function loginOrSignup($request, &$message){
		
		cAccount::logout();
		
		$status = cAccount::LOGIN_FAILED;
		if($request->getdata['act'] == 'login'){
				
			$status = cAccount::login($request, $message);
				
		}else if($request->getdata['act'] == 'facebookLogin'){
			
			try{

				$status = cAccount::login($request, $message);
			
			}catch(cException $e){
				
				if($e->getCode() == cAccount::LOGIN_BAD_USER_PASS){
					$status = cAccount::signup($request, $message);
				}
				
			}
			
		}
		else{
			
			$status = cAccount::checkEmailExists($request, $message);
			
			if($status != cAccount::SIGNUP_ACCOUNT_EXISTS){
				$status = cAccount::signup($request, $message);
			}
		}
		

		if($request->getdata['act'] != 'login' && $status == cAccount::ACCOUNT_UPDATE_SUCCESS){ // this is a signup through facebook or form.
			$status = cAccount::login($request, $message);
		}

		return $status;
	}
	
	
	
	public static function facebookLoginOrSignup($request, &$message){
		
		global $config;
		$status = cAccount::LOGIN_FAILED;
		$message = '';
			
		$facebook = new Facebook(array(
				'appId'  => $config['fbAppId'],
				'secret' => $config['fbSecret'],
		));
			
		// Get User ID
		$userId = $facebook->getUser();
				
		if ($userId) {
			try {
				// Proceed knowing you have a logged in user who's authenticated.
				// if we are in here, it means there is a valid cookie.. Othewise, the details must be passed in.
				$fbuser = $facebook->api('/me');
				
				$request->postdata['facebook_user_name'] = $fbuser['username'];
				$request->postdata['contactEmail'] = $fbuser['email'];
				$request->postdata['contactNameFirst'] = $fbuser['first_name'];
				$request->postdata['contactNameLast'] = $fbuser['last_name'];
				$request->postdata['contactTitle'] = ($fbuser['gender'] == 'male') ? 'Mr' : 'Ms';
				
					
			} catch (FacebookApiException $e) {
				throw new cException('Facebook signin error', cStatus::$INVALID_ACTION, $e->getMessage());
			}
		}
		
		
		//error_log(json_encode($request->getdata));
		
		if(!isset($request->postdata['contactEmail']))
			throw new cException('Facebook signin error', cStatus::$INVALID_ACTION);
			
		$request->postdata['password'] = cUtils::encrypt($request->postdata['contactEmail']);
		
		$status = cAccount::loginOrSignup($request, $message);
		
		return $status;
		
	}
	
	
	
	
	
	public static function updatePassword($request, &$message){
		global $db;
		global $lang;
		if(!cAccount::isLoggedIn()){
			throw new cException('Invalid session', cStatus::$INVALID_ACTION);
		}
		
		
		$password = trim($request->postdata['password']);
		$newPassword = trim($request->postdata['newPassword']);
		
		if(is_null($_SESSION['passwordReset']) && (strlen($password) == 0 || strlen($newPassword) == 0) ){
			
			throw new cException($lang['errorRequiredFields'], cStatus::$INVALID_ACTION);
		}
		
		if(strlen($newPassword) < 2){
			throw new cException($lang['errorInvalidPassword'], cStatus::$INVALID_ACTION);
		}
			

		if(isset($request->postdata['confirmPassword']) && $newPassword != $request->postdata['confirmPassword']){
			throw new cException($lang['errorPasswordNotMatch'], cStatus::$INVALID_ACTION);
		}
		
		
		$query = '	SELECT *
							FROM pc_merchants.accounts
							WHERE contact_email = "'.$_SESSION['contactEmail'].'"
									';
		if(is_null($_SESSION['passwordReset']))
			$query .= ' AND password = "'.$password.'"';
		
		
		if( $result = $db->query($query) ){
				
			if( $row = $result->fetch_assoc() ){
				
				$query = 'update pc_merchants.accounts set password = "'.$newPassword.'" where merchant_id = '.$_SESSION['merchantId'];
				
				if($result = $db->query($query)){
					$message = $lang['passwordUpdated'];
					unset($_SESSION['passwordReset']);
					$_SESSION['passwordReset'] = null;
					$_SESSION['password'] = $newPassword;
					return cAccount::ACCOUNT_UPDATE_SUCCESS;
				}
				
			}else{
				// Account exists, but the password is invalid.
				throw new cException($lang['errorWrongCurrentPassword'], cAccount::LOGIN_BAD_USER_PASS);
			}
		}
		
		// first select the old password from the account.. throw invalid password error if it doesn't exist.
		
		// next update the account.
		
	}
	
	public static function updateAccount($request, &$message, $isSignup = false){
		
		global $db;
		global $lang;
		global $config;
		
	
		
		throw new cException('Could not login with new details', cAccount::ACCOUNT_UPDATE_FAILED);
		
	}
	
	

	
	
	
	
	
	
	
	
}
?>