<?php 
$timeStart = microtime(true);
session_start();
register_shutdown_function( "shutdownFunction" );
require_once 'db.php';
require_once 'cEmail.php';

$data = null;
$status = 0;
$message = null;
$response = null;
$action = '';

function shutDownFunction() { 
    $error = error_get_last();
    if ($error['type'] == 1) {
        $response = new cResponse();
	    $response->setStatus($error["type"]);
	    $response->setMessage($error['message']); // TODO: change this error message to a public one.
	    header('HTTP/1.1 555 Server Error');
	    $response->jsonOutput();
	    exit(1);
    } 
}

// throw  new cException('No action defined', cStatus::$INVALID_ACTION);


try
{
	$response = new cResponse();
	
	$request = new cRequest(); // fills the data and validates the input.// getPostOrInput(); 
	
	if(!isset($request->getdata['act']) && !isset($request->postdata['act'])){
		throw  new cException('Invalid request', cStatus::$INVALID_ACTION);
	}
	
	$action = 'noAction';
	
	if(isset($request->postdata['act'])){
		$action = $request->postdata['act'];
	}else{
		$action = $request->getdata['act'];
	}
	
	switch($action){
		
		default:
		case 'noAction' : {
			throw  new cException('No action defined', cStatus::$INVALID_ACTION); 
		}break;
	
		
		case 'getInfluencer' : {
			
			$query = '';
			
			$cookie = cCookie::getCookie();
			if(strlen($cookie->influencer_id) > 0){
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
			}else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';			
				
			}
			
			if(strlen($query)){
				
				$data['influencers'] = cModel::query('select * from influencers where '.$query )[0];
				
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'getNewCampaigns' : {
			
			//$data = cModel::fetch('campaigns', '*');
				
			$query = '';
				
			$cookie = cCookie::getCookie();
			if(strlen($cookie->influencer_id) > 0){
				//set alert read
				$q = "UPDATE alerts SET status=1 WHERE message='New Campaign Request' AND status=0 AND influencer_id=".$cookie->influencer_id;
				$db->query($q);
				$query1 = ' campaigns.influencer_id = "'.$cookie->influencer_id.'"';
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
				
				//get waitlist
				$q = "select name from campaigns WHERE status=1 AND influencer_id=".$cookie->influencer_id;
				$list = $db->query($q);
				
				// check advertising tag in instagram
				
				foreach ($list as $value) {
					$chk=0;
				    $name = $value['name'];
					$tag = $instagram->getTagMedia($name);
					$limit = 1;
					// Show results
					// Using for loop will cause error if there are less photos than the limit
					foreach(array_slice($tag->data, 0, $limit) as $tagdata)
					{  $e = $tagdata->link; if(!is_null($e)) $chk++; }
					
					error_log($e);
					
					if($chk>0) {						
						$q = "UPDATE campaigns SET status=2, posteddate=now() WHERE name='".$name."' AND status=1 AND influencer_id=".$cookie->influencer_id;
						$db->query($q);
					}
				}
				
			} else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';
		
			}
				
			if(strlen($query)){				
				
				//load to model
				$data['campaigns'] = cModel::query('select * from campaigns INNER JOIN influencers ON campaigns.influencer_id=influencers.influencer_id where  status=0 OR status=1  AND '.$query1 );
				//$data['campaigns'] = cModel::query('select * from campaigns where status=0 AND '.$query1 );
				
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'getPostedCampaigns' : {
				
			//$data = cModel::fetch('campaigns', '*');
		
			$query = '';
		
			$cookie = cCookie::getCookie();
			if(strlen($cookie->influencer_id) > 0){
				$query1 = ' campaigns.influencer_id = "'.$cookie->influencer_id.'"';
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
			}
		
			if(strlen($query)){
		       
				$data['campaigns'] = cModel::query('select * from campaigns INNER JOIN influencers ON campaigns.influencer_id=influencers.influencer_id where status=2 AND '.$query1 );
				//$data['campaigns'] = cModel::query('select * from campaigns where status=2 AND '.$query);
		
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'postToInstagram' : {

			$cookie = cCookie::getCookie();
			
			//$data = cModel::fetch('interests', '*');
			$modelData = json_decode($request->postdata['data']);
				
			$subject = "New Adverting : ".$modelData->name;
			$To=$modelData->email; //"dropoutunit@gmail.com"; //$cookie->email;
			
			$file=$modelData->advert_image;
			$path= $docRoot.$file;
			$fname=substr($file,9);
			
			//$path= $docRoot.'/uploads/bigdot.jpg';
			//$fname='bigdot.jpg';
			
			//$data['contactEmail'] = "dropoutunit@gmail.com"; // test sendSES
			//$data['contactNameFirst'] = "sombat";
			//$data['contactNameLast'] = "Lim";
			//$data['message'] = "TEST";
			
			$email = new cEmail($config, $subject, $data);
			//$chk = $email->sendSES();
			$chk = $email->sendSES2($path,$subject,$To,$fname);
			
			//updated campaign status to 1 when send email was successful
			if (empty($chk)){ echo "DONE"; 
				$name = $modelData ->name;
				$q = "UPDATE campaigns SET status=1, posteddate=now() WHERE name='".$name."' AND status=0 AND influencer_id=".$cookie->influencer_id;
				$db->query($q);			
			}
			
			
			// reload model
			$query = '';
			if(strlen($cookie->influencer_id) > 0){
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
			}else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';
			
			}
			
			if(strlen($query)){
			
				$data['campaigns'] = cModel::query('select * from campaigns where status=0 OR status=1 AND '.$query )[0];
			
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
						
				
		} break;
		
		case 'uploadImage' : {
				
			$image = new cImage();
			$image->uploadImages($request, $message);
			
			$cookie = cCookie::getCookie();
			$Id = $cookie->influencer_id;
			
			$newImage = '/uploads/'.$image->uploadImages($request, $message);
			
			if($Id>0) { 				
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE influencer_id='.$Id;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where influencer_id='.$Id )[0];
				
			} else {
				
				$w = ' email = "'.$cookie->email.'"';
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE '.$w;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where '.$w )[0];
				
			}
			
			
			
				
		} break;
		
		case 'getModel' : 
		case 'deleteModel' : 
		case 'updateModel' : {
			
			$cookie = cCookie::getCookie();
			
			if(isset($request->postdata['data']) && isset($request->postdata['model'])){
				$modelData = json_decode($request->postdata['data']);
				$modelTable = $request->postdata['model'];
				
				$modelObj = null;
				
				switch($modelTable){
					case 'alerts' : $modelObj = new Alerts(); break;
					case 'influencers' : $modelObj = new Influencers(); break;
					case 'influencer_interest' : $modelObj = new Influencer_Interest(); break;
				}
				
				$modelObj->set($request->postdata['data']);
				
				switch($action){
					case 'getModel' : {
						$data[$modelTable] = $modelObj->get(); 
					}break;
					case 'deleteModel' : {
						$modelObj->delete();
					}break;
					case 'updateModel' : {
						if(($data[$modelTable] = $modelObj->update()) == null){
							$data[$modelTable] = $modelObj->insert();
						}
						
						$modelObj->post_insert_update($modelData);
						// Update session data
						
					}break;
				}
			}
			
		}break;
	
		case 'getInterests' : {
			
			$data = cModel::fetch('interests', '*');
			
		}break;
		
		case 'clearCookie' : {
			
			$cookie = null;
			cCookie::setCookie($cookie);
		} break;
		
		case 'setEmailAddress' : {
			
			// TODO validate email.
			if(cAccount::checkEmailExists($request->postdata['email'], $message)){
				throw new cException($message, 401);
			}
			
			$cookie = new stdClass();
			$cookie->email = $request->postdata['email'];
			cCookie::setCookie($cookie);
			
		} break;
		
		case 'getFollower' : {
			
			$cookie = cCookie::getCookie();
			
			if(isset($cookie->followed_by)) { $data['followed_by']= $cookie->followed_by; } else {
				
				$q = "select instagram_id, access_token from influencers  WHERE  influencer_id=".$cookie->influencer_id;
				$r = $db->query($q);
				
			$rows = array();
			while($row = $r->fetch_object())
			{
			    $rows[] = $row;
			}
			
			$url = 'https://api.instagram.com/v1/users/'.$rows[0]->instagram_id.'?access_token='.$rows[0]->access_token;
			$api_response = file_get_contents($url);
			$record = json_decode($api_response);
			
			$data['followed_by']=$record->data->counts->followed_by;
			
			}
			
			//$cookie = new stdClass();
			//$cookie->follower = $data['follower'];
			//cCookie::setCookie($cookie);
	
		} break;
		
		case 'getInstagramUser' :{
			
			$cookie = cCookie::getCookie();
			
			
			error_log($cookie->email.' COOKIE');
			
			
			/*if(cAccount::checkEmailExists($cookie->email, $message)){
				throw new cException($message, 555);
			}*/
			
			
			$code = $request->postdata['code'];
			
			if (isset($code)) {
				$user = $instagram->getOAuthToken($code);
				$instagram->setAccessToken($user);
				
				$data['influencer']->instagram_id = $user->user->id;
				
				//$data['influencer'] = $user->user;
				$data['influencer']->username = $user->user->username;
				$data['influencer']->full_name = $user->user->full_name;
				$data['influencer']->bio = $user->user->bio;
				$data['influencer']->website = $user->user->website;
				$data['influencer']->profile_picture = $user->user->profile_picture;
				
				$data['influencer']->followed_by = $user->user->counts->followed_by;
				
				//$data['media'] = $instagram->getUserMedia();
				$data['influencer']->access_token = $user->access_token;
				$data['influencer']->email = $cookie->email;
				// Update DB if they don't already exist.
				//cModel::update('influencers', $data['influencer']);
				$d = cModel::query('select * from influencers where instagram_id = "'.$data['influencer']->instagram_id.'"')[0];
				
				if(!isset($cookie->email) && !isset($d['email'])){
					throw new cException('Account does not exist, please signup first');
				} 
				
				if(isset($d['influencer_id'])){
					if(isset($cookie->email) && $cookie->email != $d['email'] ){
						throw new cException('This instagram account is already in use with another email address',  401);
					}
					
					$data['influencer'] = $d;
					$cookie->influencer_id = $d['influencer_id'];
					$cookie->email = $d['email'];
					$cookie->followed_by = $data['influencer']->followed_by;
					cCookie::setCookie($cookie);
				}
				
			} else {
				// check whether an error occurred
				if (isset($request->postdata['error'])) {
					throw new cException('An error occurred: ' . $request->postdata['error_description'], 555);
				}else{
					throw new cException('Unknown error', 555);
				}
			}
			
			
		}	break;
		case 'getInstagramLoginUrl' :{
			$data['loginUrl'] = $instagram->getLoginUrl();
			
		}break;
		
		case 'logout' : {
			$cookie = null;
			cCookie::setCookie($cookie);
			$message = 'logged out';
		
		}break;
	}
	
	$response->setData($data);
	$response->setStatus($status);
	$response->setMessage($message);
	$response->jsonOutput();


	
}
catch( cException $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage($e->getPublicMessage());
	
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

	

}catch( Exception $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage('Could not complete request');
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

}


try {
	if(strlen($action) == 0){
		$action = 'Not defined';
	}
	
	$timeEnd = microtime(true);
	//dividing with 60 will give the execution time in minutes other wise seconds
	$executionTime = ($timeEnd - $timeStart);
		
	$query = 'call pc3.sp_insert_exec_time("'.$action.'", '.$executionTime.')';
	$result = $db->query($query);
	
}
catch( cException $e ){
	cException::error_log($e);
}catch( Exception $e ){
	cException::error_log($e);
}
// record this.


?>