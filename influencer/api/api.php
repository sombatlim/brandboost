<?php 
$timeStart = microtime(true);
session_start();
register_shutdown_function( "shutdownFunction" );
require_once 'db.php';
require_once 'cEmail.php';

$data = null;
$status = 0;
$message = null;
$response = null;
$action = '';

function shutDownFunction() { 
    $error = error_get_last();
    if ($error['type'] == 1) {
        $response = new cResponse();
	    $response->setStatus($error["type"]);
	    $response->setMessage($error['message']); // TODO: change this error message to a public one.
	    header('HTTP/1.1 555 Server Error');
	    $response->jsonOutput();
	    exit(1);
    } 
}

// throw  new cException('No action defined', cStatus::$INVALID_ACTION);


try
{
	$response = new cResponse();
	
	$request = new cRequest(); // fills the data and validates the input.// getPostOrInput(); 
	
	if(!isset($request->getdata['act']) && !isset($request->postdata['act'])){
		throw  new cException('Invalid request', cStatus::$INVALID_ACTION);
	}
	
	$action = 'noAction';
	
	if(isset($request->postdata['act'])){
		$action = $request->postdata['act'];
	}else{
		$action = $request->getdata['act'];
	}
	
	switch($action){
		
		default:
		case 'noAction' : {
			throw  new cException('No action defined', cStatus::$INVALID_ACTION); 
		}break;
		
		case 'getMessages' : {
		
			$cookie = cCookie::getCookie();
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				
				if(isset($cookie->instagram_id)) { $id=$cookie->instagram_id; } else {
					$r=$db->query('select instagram_id from influencers where influencer_id='.$cookie->influencer_id);
					$obj=$r->fetch_object();
					$id=$obj->instagram_id;
					$cookie->instagram_id = $id;
					cCookie::setCookie($cookie);
				}
				
				$q = @'select a.fbid as bbid,a.fullname as fname from accounts a inner join campaign_influencer b
						on a.fbid=b.fbid where b.influencer_id='.$cookie->influencer_id.' group by b.fbid';
				$data['contacts'] = cModel::query($q);
				
				$data['messages'] = cModel::query('select * from messages where msgto='.$id.' OR msgfrom='.$id.' order by createddate DESC' );
				
			}else if(isset($cookie->fbid) && strlen($cookie->fbid) ){
		
				$id=$cookie->fbid;
		
				$q = @'select a.instagram_id as bbid,a.full_name as fname from influencers a inner join campaign_influencer b
						on a.influencer_id=b.influencer_id where b.fbid='.$id.' group by b.influencer_id';
				$data['contacts'] = cModel::query($q);
		
				$data['messages'] = cModel::query('select * from messages where msgto='.$id.' OR msgfrom='.$id.' order by createddate DESC' );
		
			} else {
				throw new cException('Invalid ID', 401);
			}
		} break;
	
		
		case 'getInfluencer' : {
			
			$query = '';
			
			$cookie = cCookie::getCookie();
			if(strlen($cookie->influencer_id) > 0){
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
			}else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';			
				
			}
			
			if(strlen($query)){
				
				$data['influencers'] = cModel::query('select * from influencers where '.$query )[0];
				
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'getUser' : {
				
			$query = '';
				
			$cookie = cCookie::getCookie();
			if(strlen($cookie->fbid) > 0){
				$query = ' fbid = "'.$cookie->fbid.'"';
			}else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';
		
			}
				
			if(strlen($query)){
		
				$data['accounts'] = cModel::query('select * from accounts where '.$query )[0];
		
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'sendRequest' : {

			
		} break;
		
		case 'saveNewCampaign' : {

			
		} break;
		
		case 'getNewCampaigns' : {
				
			$cookie = cCookie::getCookie();
			
			if( isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				//set alert read
				$q = "UPDATE alerts SET status=1 WHERE message='New Campaign Request' AND status=0 AND influencer_id=".$cookie->influencer_id;
				$db->query($q);
				
				$query1 = ' influencer_id = "'.$cookie->influencer_id.'" order by createddate DESC';
				//updated model
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status in (0,1) AND '.$query1);
				
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
				
					$data['campaigns'] = cModel::query('select * from campaigns  where status IN (0,1) AND fbid ='.$cookie->fbid.' order by createddate DESC' );
					
			}else{
				throw new cException('Invalid ID', 401);
			}
			
		} break;
		
		case 'getAllNewCampaigns' : {
		
			$cookie = cCookie::getCookie();
				
			if( isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
						
				$query1 = ' influencer_id = "'.$cookie->influencer_id.'" order by createddate DESC';
				//updated model
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where '.$query1);
		
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
		
				$data['campaigns'] = cModel::query('select * from campaigns  where fbid ='.$cookie->fbid.' order by createddate DESC' );
					
			}else{
				throw new cException('Invalid ID', 401);
			}
				
		} break;
		
		case 'getPostedCampaigns' : {

			$cookie = cCookie::getCookie();
			
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				
				$query = ' influencer_id = "'.$cookie->influencer_id.'" order by posteddate DESC';
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=2 AND '.$query);
				
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
				
				$query1 = ' fbid = "'.$cookie->fbid.'"  group by advertiser_id order by createddate DESC';
				
				$q = 'select *, tag as name ,count(advertiser_id) as amount, min(posteddate) as first ,	max(posteddate) as last from campaign_influencer where status=2 AND '.$query1;
				
				$data['campaigns'] = cModel::query($q);

			} else { throw new cException('Invalid ID', 401); }
		} break;
		
		case 'getAllPostedCampaigns' : {
		
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){

				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=2 order by posteddate DESC LIMIT 5');

			} else { throw new cException('Invalid ID', 401); }
		} break;
		
		case 'getPostedInfluencers' : {
		
			$modelData = $request->postdata['data'];
		    $adid = $modelData ->id;
			if($adid>0){
				
				$qs = 'select username from campaign_influencer where status=2 AND advertiser_id='.$adid;
				$r= $db->query($qs);
				$data = ''; //array();             // The array we're going to be returning
				while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) { 
					//$data .= '<a href="http://instagram.com/'.$row['username'].'" target="_blank">'.$row['username'].'</a>  '; 
					$data .= $row['username'].'  ';
				}
		
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'postToInstagram' : {

			$cookie = cCookie::getCookie();
			
			//$data = cModel::fetch('interests', '*');
			$modelData = json_decode($request->postdata['data']);
				
			$subject = "New Advertising : #".$modelData->name;
			$To=$cookie->email; 
			
			$file=$modelData->advert_image;
			//$path= $docRoot.$file;
			$path= '/home/brandboost/advertiser'.$file;
			$fname=substr($file,9);
			
			$email = new cEmail($config, $subject, $data);
			//$chk = $email->sendSES();
			
			$chk = $email->sendSES2($path,$subject,$To,$fname);
			
			//updated campaign status to 1 when send email was successful
			if (empty($chk) && isset($cookie->influencer_id)){
				  
				$name = $modelData ->name;
				$q = "UPDATE campaign_influencer SET status=1, posteddate=now() WHERE tag='".$name."' AND status=0 AND influencer_id=".$cookie->influencer_id;
				$db->query($q);	
				
				//update model
				$data['campaigns'] = cModel::query('select *,tag as name from campaign_influencer where status=0 OR status=1 AND influencer_id ='.$cookie->influencer_id.' order by createddate DESC');
				
			} else{
				throw new cException(' AWS email send failed', 400);
			}
					
				
		} break;
		
		case 'uploadImage' : {
				
			$image = new cImage();
			$image->uploadImages($request, $message);
			
			$cookie = cCookie::getCookie();
			$Id = $cookie->influencer_id;
			
			$newImage = '/uploads/'.$image->uploadImages($request, $message);
			
			if($Id>0) { 				
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE influencer_id='.$Id;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where influencer_id='.$Id )[0];
				
			} else {
				
				$w = ' email = "'.$cookie->email.'"';
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE '.$w;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where '.$w )[0];
				
			}
		
		} break;
		
		case 'uploadAdImage' : {
		
			$image = new cImage();
			$data['campaign']->advert_image = '/uploads/'.$image->uploadImages($request, $message);
	
		} break;
		
		case 'getModel' : 
		case 'deleteModel' : 
		case 'updateModel' : {
			
			$cookie = cCookie::getCookie();
			
			if(isset($request->postdata['data']) && isset($request->postdata['model'])){
				$modelData = json_decode($request->postdata['data']);
				$modelTable = $request->postdata['model'];
				
				$modelObj = null;
				
				switch($modelTable){
					case 'alerts' : $modelObj = new Alerts(); break;
					case 'messages' : $modelObj = new Messages(); break;
					case 'influencers' : $modelObj = new Influencers(); break;
					case 'accounts' : $modelObj = new Accounts(); break;
					case 'influencer_interest' : $modelObj = new Influencer_Interest(); break;
				}
				
				$modelObj->set($request->postdata['data']);
				
				switch($action){
					case 'getModel' : {
						$data[$modelTable] = $modelObj->get(); 
					}break;
					case 'deleteModel' : {
						$modelObj->delete();
					}break;
					case 'updateModel' : {
						if(($data[$modelTable] = $modelObj->update()) == null){
							$data[$modelTable] = $modelObj->insert();
						}
						
						$modelObj->post_insert_update($modelData);
						// Update session data
						
					}break;
				}
			}
			
		}break;
	
		case 'getInterests' : {
			
			$data = cModel::fetch('interests', '*');
			
		}break;
		
		case 'getDashboard' : {
			
			$cookie = cCookie::getCookie();
			
			$r = $db->query('select *  from campaign_influencer where status in (0,1) AND influencer_id='.$cookie->influencer_id );
			$data['rcount'] = $r->num_rows;
			
			$r= $db->query('select count(*) as pcount , SUM(price) as earned from campaign_influencer where status=2 AND posteddate  >= CURDATE() - INTERVAL 1 YEAR AND influencer_id='.$cookie->influencer_id );
			$obj = $r->fetch_object();
			$data['pcount'] = $obj->pcount;
			$data['earned'] = $obj->earned;
			
			$instaId = $cookie->instagram_id; 	
			
			$user = $instagram->getUser($instaId);
			$data['user']=$user;
			
			$limit = $user->data->counts->media; //counts($media);
			$data['cmedias']=$limit;
			if($limit>5) $limit=5;
			
			$media = $instagram->getUserMedia($instaId,$limit);
			
			$data['medias']= $media;
			
		}break;
		
		case 'getInterestsByAdId' : {
			//$data1 = cModel::fetch('interests', '*');
			//error_log(print_r($data1,true));
			
			$modelData = json_decode($request->postdata['data']);			
			$r= $db->query('SELECT interest_id FROM campaign_interest WHERE advertiser_id='.$modelData);
			$data = array();             // The array we're going to be returning
			while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) { $data[]= $row['interest_id']; }
			
		}break;
		
		
		case 'clearCookie' : {
			
			$cookie = null;
			cCookie::setCookie($cookie);
		} break;
		
		case 'setEmailAddress' : {
			
			// TODO validate email.
			if(cAccount::checkEmailExists($request->postdata['email'], $message)){
				throw new cException($message, 401);
			}
			
			$cookie = new stdClass();
			$cookie->email = $request->postdata['email'];
			cCookie::setCookie($cookie);
			
		} break;
		
		case 'getFollower' : {
			
			$cookie = cCookie::getCookie();						
			
			if(!isset($cookie->instagram_id)) {	
				$r = $db->query('select instagram_id from influencers where influencer_id='.$cookie->influencer_id);
				$obj = $r->fetch_object();
				$instaID=$obj->instagram_id;
			$cookie->instagram_id = $instaID;
			cCookie::setCookie($cookie); } else { $instaID=$cookie->instagram_id; }
			
			$user = $instagram->getUser($instaID);
			$data['followed_by']=$user->data->counts->followed_by;
			$data['instagram_id']=$instaID;						

	
		} break;
		
		case 'getInstagramUser' :{
			
			$cookie = cCookie::getCookie();
			error_log($cookie->email.' COOKIE');
	
			$code = $request->postdata['code'];
			
			if (isset($code)) {
				$user = $instagram->getOAuthToken($code);
				$instagram->setAccessToken($user);
				
				$data['influencer']->instagram_id = $user->user->id;
				
				//$data['influencer'] = $user->user;
				$data['influencer']->username = $user->user->username;
				$data['influencer']->full_name = $user->user->full_name;
				$data['influencer']->bio = $user->user->bio;
				$data['influencer']->website = $user->user->website;
				$data['influencer']->profile_picture = $user->user->profile_picture;
				
				$data['influencer']->followers = $user->user->counts->followed_by;
				
				//$data['media'] = $instagram->getUserMedia();
				$data['influencer']->access_token = $user->access_token;
				$data['influencer']->email = $cookie->email;
				// Update DB if they don't already exist.
				//cModel::update('influencers', $data['influencer']);
				$d = cModel::query('select * from influencers where instagram_id = "'.$data['influencer']->instagram_id.'"')[0];
				
				if(!isset($cookie->email) && !isset($d['email'])){
					throw new cException('Account does not exist, please signup first');
				} 
				
				if(isset($d['influencer_id'])){
					if(isset($cookie->email) && $cookie->email != $d['email'] ){
						throw new cException('This instagram account is already in use with another email address',  401);
					}
					
					$data['influencer'] = $d;
					$cookie->influencer_id = $d['influencer_id'];
					$cookie->instagram_id = $d['instagram_id'];
					$cookie->email = $d['email'];
					$cookie->followed_by = $data['influencer']->followers;
					cCookie::setCookie($cookie);
				} 
				
			} else {
				// check whether an error occurred
				if (isset($request->postdata['error'])) {
					throw new cException('An error occurred: ' . $request->postdata['error_description'], 555);
				}else{
					throw new cException('Unknown error', 555);
				}
			}
			
			
		} break;
		
		case 'getInstagramLoginUrl' :{
			$data['loginUrl'] = $instagram->getLoginUrl();
			
		} break;
		
		case 'getFacebookLoginUrl' :{
			$data['loginUrl'] = $facebook->getLoginUrl(array(
			 'scope' => 'email', // Permissions to request from the user
			 ));
				
		} break;
		
		case 'getFacebookUser' :{
			
			$user = $facebook->getUser();
			
			if ($user) {
				
				try {
					$user_profile = $facebook->api('/me');
					$fbid = $user_profile['id'];                 // To Get Facebook ID
					$fbuname = $user_profile['username'];  // To Get Facebook Username
					$fbfullname = $user_profile['name']; // To Get Facebook full name
					$femail = $user_profile['email'];    // To Get Facebook email ID
					/* ---- Session Variables -----*/
					$_SESSION['FBID'] = $fbid;
					$_SESSION['USERNAME'] = $fbuname;
					$_SESSION['FULLNAME'] = $fbfullname;
					$_SESSION['EMAIL'] =  $femail;
					//       checkuser($fbid,$fbuname,$fbfullname,$femail);    // To update local DB
					$data['account']->fbid =$fbid;
					$data['account']->username =$fbuname;
					$data['account']->fullname =$fbfullname;
					$data['account']->email = $femail;
					
				} catch (FacebookApiException $e) {
					error_log($e);
					$user = null;
				}
				
				$d = cModel::query('select * from accounts where fbid = "'.$data['account']->fbid.'"')[0];
				
				if(isset($d['account_id'])){
					if(isset($cookie->email) && $cookie->email != $d['email'] ){
						throw new cException('This instagram account is already in use with another email address',  401);
					}
					
					$cookie->email = $d['email'];
					$cookie->fbid  = $d['fbid'];
					cCookie::setCookie($cookie);
				} else { cModel::update('accounts', $data['account']); }
			} else {
				
				// check whether an error occurred
				if (isset($request->postdata['error'])) {
					throw new cException('An error occurred: ' . $request->postdata['error_description'], 555);
				}else{
					throw new cException('Unknown error', 555);
				}
			}
			
		} break;
		
		case 'logout' : {
			$cookie = null;
			cCookie::setCookie($cookie);
			
			session_unset();
			$_SESSION['FBID'] = NULL;
			$_SESSION['USERNAME'] = NULL;
			$_SESSION['FULLNAME'] = NULL;
			$_SESSION['EMAIL'] =  NULL;
			$_SESSION['LOGOUT'] = NULL;
			
			$message = 'logged out';
		
		} break;
		
		case 'checkAuth' :{
		
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->fbid) || isset($cookie->influencer_id)) $data['auth']=true; else $data['auth']=false;
			if(isset($cookie->email)) $data['auth2']=true; else $data['auth2']=false;
			if( isset($cookie->influencer_id)) $data['authfid']=$cookie->influencer_id;
		
		} break;
	}
	
	$response->setData($data);
	$response->setStatus($status);
	$response->setMessage($message);
	$response->jsonOutput();


	
}
catch( cException $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage($e->getPublicMessage());
	
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

	

}catch( Exception $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage('Could not complete request');
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

}


try {
	if(strlen($action) == 0){
		$action = 'Not defined';
	}
	
	$timeEnd = microtime(true);
	//dividing with 60 will give the execution time in minutes other wise seconds
	$executionTime = ($timeEnd - $timeStart);
		
	$query = 'call pc3.sp_insert_exec_time("'.$action.'", '.$executionTime.')';
	$result = $db->query($query);
	
}
catch( cException $e ){
	cException::error_log($e);
}catch( Exception $e ){
	cException::error_log($e);
}
// record this.


?>