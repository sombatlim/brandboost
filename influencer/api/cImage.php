<?php 

require_once $docRoot.'/api/lib/class.upload.php';
class cImage {
	public function uploadImages($request, &$message){
	
		global $db;
	
		// Need to get the highest seq number
	
		if(!isset($_FILES["uploadedFile"])){
			throw new cException('UploadedFile empty', cStatus::$INVALID_ACTION);
		}
	
		$fileCount = count($_FILES["uploadedFile"]['name']);
	
		if($fileCount > 0){
				
			$imageDirectory = '/home/brandboost/influencer/uploads';
				
			if (!file_exists($imageDirectory)) {
				mkdir($imageDirectory, 0777, true);
			}
	
			for($i = 0; $i < $fileCount; $i++){
	
				if ($_FILES["uploadedFile"]["error"][$i] > 0){
					throw new cException($lang['errorImageUpload'], cStatus::$INVALID_ACTION, "Return Code: " . $_FILES["uploadedFile"]["error"][$i] );
				}
				else{
						
					$file = array(  'name' => $_FILES["uploadedFile"]['name'][$i],
							'error' => $_FILES["uploadedFile"]['error'][$i],
							'type' => $_FILES["uploadedFile"]['type'][$i],
							'tmp_name' => $_FILES["uploadedFile"]['tmp_name'][$i],
							'size' => $_FILES["uploadedFile"]['size'][$i]
					);
						
						
					$newName = '';
					$width = 0;
					$height = 0;
						
					$origSrc = $_FILES["uploadedFile"]["name"][$i];
						
					$fileNameNoExt = md5($request->postdata['influencer_id'].$origSrc);
						
					$mime = mime_content_type($file['tmp_name']);
						
					// this code for video
					if(strstr($mime, "image/") !== FALSE){
	
						// ensure is image.
						if (exif_imagetype($file['tmp_name']) != IMAGETYPE_GIF &&
								exif_imagetype($file['tmp_name']) != IMAGETYPE_JPEG &&
								exif_imagetype($file['tmp_name']) != IMAGETYPE_PNG) {
									throw new cException('Invalid image type', cStatus::$INVALID_ACTION);
						}
	
						// TODO: check size.
	
						$newName = $this->processImage($file, $imageDirectory, $fileNameNoExt, $width, $height);
						//$newName = $merchantId.'/'.$newName;
						$message = $fileNameNoExt;
						return $newName;
	
					}					
				
				}
			}
		}
	
		throw new cException('Error uploading images', cStatus::$INVALID_ACTION);
	}
	
	public function processImage($file, $imageDirectory, $fileNameNoExt, &$width, &$height){
	
		$foo = new Upload($file);
		if ($foo->uploaded) {
				
			// save uploaded image with no changes
				
			/*$foo->Process();
				if ($foo->processed) {
			;
			} else {
			throw new cException('Error processing image', cStatus::$INVALID_ACTION, $foo->error);
			}*/
				
				
			// save uploaded image with a new name
			if($width == 0){
				$foo->file_new_name_body = $fileNameNoExt;
				$foo->image_convert = 'jpg';
				$foo->Process($imageDirectory); // TODO: this needs fixing,, it's not resizing or converting for jpgs, but still taking ages and using 100% cpu.
				if ($foo->processed) {
					;//echo 'image renamed "foo" copied';
				} else {
					throw new cException('Error processing image', cStatus::$INVALID_ACTION, $foo->error);
				}
	
			}else{
	
				$foo->file_new_name_body = $fileNameNoExt;
				$foo->image_resize = true;
				$foo->image_convert = 'jpg';
				$foo->image_x = $width;
				$foo->image_ratio_y = true;
	
				$foo->Process($imageDirectory);
	
				if ($foo->processed) {
					$foo->Clean();
				} else {
					throw new cException('Error processing image', cStatus::$INVALID_ACTION, $foo->error);
				}
			}
				
			$width = $foo->image_dst_x;
			$height = $foo->image_dst_y;
			$newName =$foo->file_dst_name;
			return $newName;
		}
	
	
	}
	
	
}
