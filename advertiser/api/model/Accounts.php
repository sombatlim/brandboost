<?php
class Accounts extends Base{
	
	public $primary_keys = array('account_id');
	public $unique_keys = array('email');
	public $search_key = array('account_id','fbid');
	
	
	public function post_insert_update($modelData){
		if(isset($modelData->email)){
			$cookie->email = $modelData->email;
		}
		
		if(isset($modelData->account_id)){
			$cookie->account_id = $modelData->account_id;
		}
		
		if(isset($modelData->fbid)){
			$cookie->fbid = $modelData->fbid;
		}
		
		cCookie::setCookie($cookie);
	}
	
}


?>