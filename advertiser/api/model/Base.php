<?php 
class Base{
	
	public $data;
	public $table;
	
	
	function  __construct(){
		$this->table = strtolower(get_class($this));
		error_log(get_class($this));
	}
	
	public function set($data){
		$this->data = json_decode($data);
	}
	
	public function post_insert_update($modelData){
	
	}
	
	// Throws exception if user exists
	public function insert(){
	
		global $db;
		$keyValues = '';
		$data = $this->data;
		$primaryKeyVal = '';
		foreach($this->data as $key => $val){
			$keyValues .= $key.'="'.$val.'",';
		}
		$keyValues = rtrim($keyValues, ',');
	
		if($keyValues){
			$query = 'INSERT INTO '.$this->table.' SET '.$keyValues;
			$db->query($query);
			$query = 'SELECT LAST_INSERT_ID() as id FROM '.$this->table;
			if(($result = $db->query($query)) && $row = $result->fetch_assoc()){
	
				$query = 'SELECT * FROM '.$this->table.' WHERE '.$this->primary_keys[0].' = '.$row['id'];
				if(($result = $db->query($query)) && $row = $result->fetch_assoc()){
					$data = $row;
				}
			}
		}
		return $data;
	}
	
	
	public function update(){
	
		global $db;
		$data = null;
		$keyValues = '';
		$primaryKeyVal = '';
		foreach($this->data as $key => $val){
			if(in_array($key, $this->primary_keys)){
				$primaryKeyVal .= $key.' = "'.$val.'" AND ';
			}else{
				$keyValues .= $key.'="'.$val.'",';
			}
		}
		$keyValues = rtrim($keyValues, ',');
		$primaryKeyVal = rtrim($primaryKeyVal, ' AND ');
	
		if($keyValues && $primaryKeyVal){
			$query = 'UPDATE '.$this->table.' SET '.$keyValues.' WHERE '.$primaryKeyVal;
			$db->query($query);
			$data = $this->data;
				
		}
		return $data;
	
	}
	
	
	
	public function delete(){
	
		global $db;
		$deleted = false;
		$primaryKeyVal = '';
		foreach($this->data as $key => $val){
			if(in_array($key, $this->primary_keys)){
				$primaryKeyVal .= $key.' = "'.$val.'" AND ';
			}
		}
		$primaryKeyVal = rtrim($primaryKeyVal, ' AND ');
	
		if($primaryKeyVal){
			$query = 'DELETE FROM '.$this->table.' WHERE '.$primaryKeyVal;
			$db->query($query);
			$deleted = true;
	
		}
		return $deleted;
	}
	

	
	public function get(){
		global $db;
		$data = null;
		$keyValues = '';
		$primaryKeyVal = '';
		foreach($this->data as $key => $val){
			if(in_array($key, $this->search_key)){
				$primaryKeyVal .= $key.' = "'.$val.'" AND ';
			}
		}
	
		$primaryKeyVal = rtrim($primaryKeyVal, ' AND ');
	
		if($primaryKeyVal){
			$query = 'SELECT * FROM '.$this->table.' WHERE '.$primaryKeyVal;
			if($result = $db->query($query)){
				while($row = $result->fetch_assoc()){
					$data[]  = $row;
				}
			}
		}
		return $data;
	}
	
	
}

?>