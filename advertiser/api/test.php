<?php
/*
 * this file run via crontab every 10 minutes , to change  using crontab -e command
 */
$docRoot = '/home/brandboost/advertiser';
$config = parse_ini_file($docRoot.'/config.ini');
ini_set('session.cookie_domain', '.brandboost.asia');

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$config['base_url'] = str_replace('http://', 'https://', $config['base_url']);
}

require_once $docRoot.'/api/lib/cModel.php';
require_once $docRoot.'/api/lib/cException.php';
require_once $docRoot.'/api/lib/cRequest.php';
require_once $docRoot.'/api/lib/cResponse.php';
require_once $docRoot.'/api/lib/cStatus.php';
//require_once $docRoot.'/api/lib/cMemcache.php';
require_once $docRoot.'/api/lib/cUtils.php';
require_once $docRoot.'/api/lib/cMySqli.php';
require_once $docRoot.'/api/lib/cCookie.php';

require_once $docRoot.'/api/cInstagram.php';
require_once $docRoot.'/api/cAccount.php';
require_once $docRoot.'/api/cImage.php';

$instagram = new cInstagram(array(
		'apiKey'      => $config['instagramApiKey'],
		'apiSecret'   => $config['instagramApiSecret'],
		'apiCallback' => $config['instagramApiCallback'] // must point to success.php
));


$db = NULL;
$memcache = NULL;
date_default_timezone_set("Asia/Bangkok");

try
{
	
	$db = new cMySqli($config);
		
	//$memcache = new cMemcache($config);
	
	error_log('Hello this run by crontab ----!');
	//$instagram->setAccessToken('1560924424.29cde60.c64b894411a340b0ba6f6b591a57b74d');
    //$user = $instagram->getUser('1560924424');
   // $user = $instagram->getOAuthToken($code);
    //$instagram->setAccessToken($user);
    $location = $instagram->getLocation('1560924424');
    //$likes = $instagram->getMediaLikes('1560924424');
    //$medias = $instagram->getUserMedia('1560924424');
    
    
    error_log(print_r($location,true));
    
    

	
}
catch( Exception $e ){
	cException::error_log($e);
	header('HTTP/1.1 503 Service Unavailable');
	exit(1);
}

function getlink($tag,$username,$access_token) {

	$api = file_get_contents("https://api.instagram.com/v1/tags/".$tag."/media/recent?access_token=".$access_token);
	$json = json_decode($api,true);
	$link = NULL;
	$created_time = NULL;
	
	foreach($json['data'] as $data){
	if($data['user']['username']==$username){ $link = $data['link']; $GLOBALS['created_time'] = $data['created_time']; } 
	
	}
	
	return $link; 
}


?>


