<?php
/*
 * this file run via crontab every 10 minutes , to change  using crontab -e command
 */
$docRoot = '/home/brandboost/advertiser';
$config = parse_ini_file($docRoot.'/config.ini');
ini_set('session.cookie_domain', '.brandboost.asia');

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$config['base_url'] = str_replace('http://', 'https://', $config['base_url']);
}

require_once $docRoot.'/api/lib/cModel.php';
require_once $docRoot.'/api/lib/cException.php';
require_once $docRoot.'/api/lib/cRequest.php';
require_once $docRoot.'/api/lib/cResponse.php';
require_once $docRoot.'/api/lib/cStatus.php';
//require_once $docRoot.'/api/lib/cMemcache.php';
require_once $docRoot.'/api/lib/cUtils.php';
require_once $docRoot.'/api/lib/cMySqli.php';
require_once $docRoot.'/api/lib/cCookie.php';

require_once $docRoot.'/api/cInstagram.php';
require_once $docRoot.'/api/cAccount.php';
require_once $docRoot.'/api/cImage.php';

$instagram = new cInstagram(array(
		'apiKey'      => $config['instagramApiKey'],
		'apiSecret'   => $config['instagramApiSecret'],
		'apiCallback' => $config['instagramApiCallback'] // must point to success.php
));


$db = NULL;
$memcache = NULL;
date_default_timezone_set("Asia/Bangkok");

try
{
	
	$db = new cMySqli($config);
		
	//$memcache = new cMemcache($config);
	
	error_log('Hello this run by crontab ----!');
	
	$q = 'select * from campaign_influencer WHERE status=1';
	$list = $db->query($q);
	
	foreach ($list as $value) {
	$chk=0;
	$e=null;
	$tag=$value['tag'];
	$username=$value['username'];
	$access_token=$value['access_token'];
	
	$e = getlink($tag,$username,$access_token);
	error_log('Time:'.$created_time.' Tag #'.$tag.' URL = '.$e);
		
		if($e) {

		$date = new DateTime("@$created_time");
        $posteddtm = $date->format('Y-m-d H:i:s');
		    $db->query("UPDATE campaigns SET status=2, posteddate=now() WHERE status=1 AND name='".$tag."'");
			$db->query("UPDATE campaign_influencer SET status=2, posteddate='".$posteddtm."', instagram_url='".$e."' WHERE status=1 AND tag='".$tag."'");
			$created_time = null;
			} // end $e 
	} // end $list

	
}
catch( Exception $e ){
	cException::error_log($e);
	header('HTTP/1.1 503 Service Unavailable');
	exit(1);
}

function getlink($tag,$username,$access_token) {

	$api = file_get_contents("https://api.instagram.com/v1/tags/".$tag."/media/recent?access_token=".$access_token);
	$json = json_decode($api,true);
	$link = NULL;
	$created_time = NULL;
	
	foreach($json['data'] as $data){
	if($data['user']['username']==$username){ $link = $data['link']; $GLOBALS['created_time'] = $data['created_time']; } 
	
	}
	
	return $link; 
}


?>


