<?php 
$timeStart = microtime(true);
session_start();
register_shutdown_function( "shutdownFunction" );
require_once 'db.php';
require_once 'cEmail.php';

$data = null;
$status = 0;
$message = null;
$response = null;
$action = '';

function shutDownFunction() { 
    $error = error_get_last();
    if ($error['type'] == 1) {
        $response = new cResponse();
	    $response->setStatus($error["type"]);
	    $response->setMessage($error['message']); // TODO: change this error message to a public one.
	    header('HTTP/1.1 555 Server Error');
	    $response->jsonOutput();
	    exit(1);
    } 
}

// throw  new cException('No action defined', cStatus::$INVALID_ACTION);


try
{
	$response = new cResponse();
	
	$request = new cRequest(); // fills the data and validates the input.// getPostOrInput(); 
	
	if(!isset($request->getdata['act']) && !isset($request->postdata['act'])){
		throw  new cException('Invalid request', cStatus::$INVALID_ACTION);
	}
	
	$action = 'noAction';
	
	if(isset($request->postdata['act'])){
		$action = $request->postdata['act'];
	}else{
		$action = $request->getdata['act'];
	}
	
	switch($action){
		
		default:
		case 'noAction' : {
			throw  new cException('No action defined', cStatus::$INVALID_ACTION); 
		}break;
		
		case 'getMessages' : {
				
			$cookie = cCookie::getCookie();
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				
				if(isset($cookie->instagram_id)) { $id=$cookie->instagram_id; } else {
					$r=$db->query('select instagram_id from influencers where influencer_id='.$cookie->influencer_id);
					$obj=$r->fetch_object();
					$id=$obj->instagram_id;
					$cookie->instagram_id = $id;
					cCookie::setCookie($cookie);
				}
				
				$q = @'select a.fbid as bbid,a.fullname as fname from accounts a inner join campaign_influencer b
						on a.fbid=b.fbid where b.influencer_id='.$cookie->influencer_id.' group by b.fbid';
				$data['contacts'] = cModel::query($q);
				
				$data['messages'] = cModel::query('select * from messages where msgto='.$id.' OR msgfrom='.$id.' order by createddate DESC' );
				
			} else if(isset($cookie->fbid) && strlen($cookie->fbid) ){
				
				$id=$cookie->fbid;
				
				$q = @'select a.instagram_id as bbid,a.full_name as fname from influencers a inner join campaign_influencer b 
						on a.influencer_id=b.influencer_id where b.fbid='.$id.' group by b.influencer_id';
				$data['contacts'] = cModel::query($q);
				
				$data['messages'] = cModel::query('select * from messages where msgto='.$id.' OR msgfrom='.$id.' order by createddate DESC' );
		
			} else {
				throw new cException('Invalid ID', 401);
			}
		} break;
	
		
		case 'getInfluencer' : {
			
			$query = '';
			
			$cookie = cCookie::getCookie();
			if(strlen($cookie->influencer_id) > 0){
				$query = ' influencer_id = "'.$cookie->influencer_id.'"';
			}else if(strlen($cookie->email) ){
				$query = ' email = "'.$cookie->email.'"';			
				
			}
			
			if(strlen($query)){
				
				$data['influencers'] = cModel::query('select * from influencers where '.$query )[0];
				
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'getUser' : {
				
			$query = '';
				
			$cookie = cCookie::getCookie();
			if(strlen($cookie->fbid) > 0){
				$query = ' fbid='.$cookie->fbid;
			}else if(strlen($cookie->email) ){
				$query = ' email= "'.$cookie->email.'"';
		
			}
				
			if(strlen($query)){
		
				$data['accounts'] = cModel::query('select * from accounts where '.$query)[0];
				//error_log('getuser_id'.$cookie->account_id);
			}else{
				throw new cException('Invalid ID or Email', 401);
			}
		} break;
		
		case 'sendRequest_old' : {
			
			$cookie = cCookie::getCookie();
			
			$modelData = $request->postdata['data'];
			$advertiser_id = $modelData ->id;
			$tagname = $modelData ->tag;
			$advert_image=$modelData ->advert_image;
			
			//get advertiser interrests
			$interests = '(0';
			$rr = $db->query('select * from campaign_interest where advertiser_id='.$advertiser_id);
			foreach ($rr as $inlist) { 	$interests .= ','.$inlist['interest_id'];	}
			$interests .= ')';
			
			$rc=false;

			$f = @'select *,b.username as username,b.access_token as access_token from influencer_interest  a 
					inner join influencers  b
					on a.influencer_id=b.influencer_id 
					where a.interest_id in '.$interests.' group by a.influencer_id';
			
			$r = $db->query($f);
			
			//error_log($f.' LOG '.print_r($r,true));
			//foreach ($r as $value) { error_log(' xxx '.$value['influencer_id']); }
			
			if($r) {
			foreach ($r as $value) {
			//check wether advertisment_id and ifluencer_id exist or not
			$q2 = 'select * from campaign_influencer where advertiser_id="'.$advertiser_id.'" AND influencer_id="'.$value['influencer_id'].'"';
			$chk = $db->query($q2);
			
			//error_log($chk->num_rows);
			
				if($chk->num_rows == 0) {
				$qcf = @'INSERT INTO campaign_influencer (`advertiser_id`,`influencer_id`,`username`,`access_token`,`tag`,`advert_image`,`createddate`,`fbid`) 
						VALUES ('.$advertiser_id.','.$value['influencer_id'].',"'.$value['username'].'","'.$value['access_token'].'","'.$tagname.'","'.$advert_image.'",now(),'.$cookie->fbid.')';
				$db->query($qcf); 
				
				//create alert
				$db->query('insert into alerts (`influencer_id`,`advertiser_id`,`message`,`createddtm`) values ('.$value['influencer_id'].','.$advertiser_id.',"New Campaign Request",now())');
				
				$rc=true;
				}
					} // end $value
					
					if($rc) { $db->query('UPDATE campaigns SET `status`=1 WHERE status=0 AND advertiser_id='.$advertiser_id); }
						
					// updated model
					
					$data['campaigns'] = cModel::query('select * from campaigns  where status IN (0,1) AND fbid ='.$cookie->fbid.' order by createddate DESC');
					
			} else {
				throw new cException('cannot send request now, try it again later', 401);
				}
				
			
		} break;
		
		case 'sendRequest' : {
				
			$cookie = cCookie::getCookie();
				
			$modelData = $request->postdata['data'];
			$advertiser_id = $modelData ->advertiser_id;
			$tagname = $modelData ->name;
			$advert_image=$modelData ->advert_image;
			$f=	$modelData ->influencers;
			//list influencers
			$influencers = '(0';
			foreach ($f as $inlist) { 	$influencers .= ','.$inlist;	}
			$influencers .= ')';
				
			$rc=false;
				
			$r = $db->query('select * from  influencers where influencer_id in '.$influencers);
				
			//error_log($f.' LOG '.print_r($r,true));
			//foreach ($r as $value) { error_log(' xxx '.$value['influencer_id']); }
				
			if($r) {
				foreach ($r as $value) {
					//check wether advertisment_id and ifluencer_id exist or not
					$q2 = 'select * from campaign_influencer where advertiser_id="'.$advertiser_id.'" AND influencer_id="'.$value['influencer_id'].'"';
					$chk = $db->query($q2);
						
					//error_log($chk->num_rows);
						
					if($chk->num_rows == 0) {
						$qcf = @'INSERT INTO campaign_influencer (`advertiser_id`,`influencer_id`,`username`,`access_token`,`tag`,`advert_image`,`createddate`,`fbid`,`price`)
						VALUES ('.$advertiser_id.','.$value['influencer_id'].',"'.$value['username'].'","'.$value['access_token'].'","'.$tagname.'","'.$advert_image.'",now(),'.$cookie->fbid.','.$value['value'].')';
						$db->query($qcf);
		
						//create alert
						$db->query('insert into alerts (`influencer_id`,`advertiser_id`,`message`,`createddtm`) values ('.$value['influencer_id'].','.$advertiser_id.',"New Campaign Request",now())');
		
						$rc=true;
					}
				} // end $value
					
				if($rc) { $db->query('UPDATE campaigns SET `status`=1 WHERE status=0 AND advertiser_id='.$advertiser_id); }
		
				// updated model
					
				$data['campaign'] = cModel::query('select * from campaigns  where advertiser_id='.$advertiser_id);
					
			} else {
				throw new cException('cannot send request now, try it again later', 401);
			}
		
				
		} break;
		
		case 'saveNewCampaign' : {

			$cookie = cCookie::getCookie();

			$modelData = json_decode($request->postdata['data']);
			$chk =0;
			$name = $modelData->name;
			$tag = $instagram->getTagMedia($name);
			$limit = 1;
			//error_log($name);
			// Show results
			// Using for loop will cause error if there are less photos than the limit
			foreach(array_slice($tag->data, 0, $limit) as $tagdata)
			{  $e = $tagdata->link; if(!is_null($e)) $chk++; }
			//error_log('link '.$e);
			if($chk>0) { throw new cException('Tag already exist in instagram', 401);  } else {
			
			$inlist = $modelData->interests;
			
			// check exist tag name
			$r = $db->query('select * from campaigns where name="'.$name.'"'); 
			if($r->num_rows) throw new cException('Tag Name Already Exist', 401);
			
			//insert new campaign			
			if(isset($cookie->fbid)){

				$d = cModel::update('campaigns', $modelData);				
				

				if(isset($db->insert_id)) {
					
					$newc_id = $db->insert_id;
					
					for($i=0;$i<count($inlist);$i++) {
						$q = 'INSERT INTO campaign_interest (`advertiser_id`,`interest_id`) VALUES ('.$newc_id.','.$inlist[$i].')';
						$db->query($q);						
						
					}
					
					$data['advertiser_id']=$newc_id;
					
				} else {
				throw new cException('cannot record new campaign', 401);
				}
				
				} //end fbid
			} // end $chk
			
			
		} break;
		
		case 'updatedCampaign' : {
		
				
			$cookie = cCookie::getCookie();
		
			$modelData = json_decode($request->postdata['data']);
			$advertiser_id = $modelData->advertiser_id;
			$chk =0;
			$name = $modelData->name;
			$tag = $instagram->getTagMedia($name);
			$limit = 1;
			//error_log($name);
			
			// Show results
			// Using for loop will cause error if there are less photos than the limit
			foreach(array_slice($tag->data, 0, $limit) as $tagdata)
			{  $e = $tagdata->link; if(!is_null($e)) $chk++; }
			//error_log('link '.$e);
			if($chk>0) { throw new cException('Tag already exist in instagram', 401);  } else {
					
				$inlist = $modelData->interests;
					
				//Updated campaign
					
				if(isset($cookie->fbid)){
					
					$d = cModel::update('campaigns', $modelData);

					if($d) {

						$db->query('DELETE FROM campaign_interest WHERE advertiser_id='.$advertiser_id);
						
						for($i=0;$i<count($inlist);$i++) {
							$q = 'INSERT INTO campaign_interest (`advertiser_id`,`interest_id`) VALUES ('.$advertiser_id.','.$inlist[$i].')';
							$db->query($q);

						//updated model
						$data['campaigns'] = cModel::query('select * from campaigns  where status=0 AND fbid ='.$cookie->fbid.' order by createddate DESC');
						$data['interest'] = array("57"=>"TEST2");
						}
							
					} else {
						throw new cException('cannot record new campaign', 401);
					}
		
				} //end fbid
			} // end $chk
				
				
		} break;
		
		case 'saveCampaignInfluencers' : {
			$modelData = $request->postdata['data'];
			$modelData->influencers = implode(",",$modelData->influencers);
			$modelData->interests = implode(",",$modelData->interests);
			$data = cModel::update('campaigns', $modelData);
			
		} break;
		
		case 'saveCampaignPayment' : {
			$modelData = $request->postdata['data'];
			$data = cModel::update('campaigns', $modelData);
				
		} break;
		
		case 'getDraftCampaigns' : {
				
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
		
				$data['campaigns'] = cModel::query('select * from campaigns  where status=0 AND fbid ='.$cookie->fbid.' order by createddate DESC');
				
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getNewCampaigns' : {
				
			$cookie = cCookie::getCookie();
			
			if( isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				//set alert read
				$q = "UPDATE alerts SET status=1 WHERE message='New Campaign Request' AND status=0 AND influencer_id=".$cookie->influencer_id;
				$db->query($q);
				
				$query = ' influencer_id = "'.$cookie->influencer_id.'" order by createddate DESC';
				//updated model
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=0 OR status=1 AND '.$query);
				
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
				
					$data['campaigns'] = cModel::query('select * from campaigns  where status=1 AND fbid ='.$cookie->fbid.' order by createddate DESC');
					foreach($data['campaigns'] as &$dcp) if($dcp['influencers']) $dcp['influencers'] = explode(",",$dcp['influencers']);
					//$data['interest'] = array("57"=>"TEST1");
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getNewCampaignByID' : {
		
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
				
				$modelData = $request->postdata['data'];
				$advertiser_id = $modelData ->id;
		
				$data['campaigns'] = cModel::query('select * from campaigns  where advertiser_id='.$advertiser_id);
				
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getAllNewCampaigns' : {
		
			$cookie = cCookie::getCookie();
		
			if( isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
		
				$query1 = ' influencer_id = "'.$cookie->influencer_id.'" order by createddate DESC';
				//updated model
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where '.$query1);
		
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
		
				$data['campaigns'] = cModel::query('select * from campaigns  where fbid ='.$cookie->fbid.' order by createddate DESC' );
					
			}else{
				throw new cException('Invalid ID', 401);
			}
		
		} break;
		
		case 'getPostedCampaigns' : {
		
			$cookie = cCookie::getCookie();
			
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
				
				$query = ' influencer_id = "'.$cookie->influencer_id.'" order by posteddate DESC';
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=2 AND '.$query);
				
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
				
				$query1 = ' fbid = "'.$cookie->fbid.'"  group by advertiser_id order by createddate DESC';
				
				$q = 'select *, tag as name ,count(advertiser_id) as amount, min(posteddate) as first ,	max(posteddate) as last from campaign_influencer where status=2 AND '.$query1;
				
				$data['campaigns'] = cModel::query($q);
		
			} else {
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getFinishedCampaigns' : { //status=3 ???
		
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
		
				$query = ' influencer_id = "'.$cookie->influencer_id.'" order by posteddate DESC';
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=3 AND '.$query);
		
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
		
				$query1 = ' fbid = "'.$cookie->fbid.'"  group by advertiser_id order by createddate DESC';
		
				$q = 'select *, tag as name ,count(advertiser_id) as amount, min(posteddate) as first ,	max(posteddate) as last from campaign_influencer where status=3 AND '.$query1;
		
				$data['campaigns'] = cModel::query($q);
		
			} else {
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getArchivedCampaigns' : { //status=4 ???
		
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->influencer_id) && strlen($cookie->influencer_id) > 0){
		
				$query = ' influencer_id = "'.$cookie->influencer_id.'" order by posteddate DESC';
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=4 AND '.$query);
		
			} else if(isset($cookie->fbid) && strlen($cookie->fbid)>0){
		
				$query1 = ' fbid = "'.$cookie->fbid.'"  group by advertiser_id order by createddate DESC';
		
				$q = 'select *, tag as name ,count(advertiser_id) as amount, min(posteddate) as first ,	max(posteddate) as last from campaign_influencer where status=4 AND '.$query1;
		
				$data['campaigns'] = cModel::query($q);
		
			} else {
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getAllPostedCampaigns' : {
			
			
			$cookie = cCookie::getCookie();
			
			if(isset($cookie->influencer_id) || isset($cookie->fbid))	{
		
				$data['campaigns'] = cModel::query('select *, tag as name from  campaign_influencer where status=2 order by posteddate DESC LIMIT 5');
		
			} else { throw new cException('Invalid ID', 401); }
			
		} break;
		
		case 'getPaymentSummary' : {
				
				
			$cookie = cCookie::getCookie();
				
			if(isset($cookie->influencer_id) || isset($cookie->fbid))	{
		
				$data['campaigns'] = cModel::query('select * from  campaigns where status in (0,1,2,3) order by createddate');
				foreach($data['campaigns'] as &$dcp) if($dcp['influencers']) $dcp['influencers'] = explode(",",$dcp['influencers']);
			} else { throw new cException('Invalid ID', 401); }
				
		} break;
		
		case 'getPostedInfluencers' : {
		
			$modelData = $request->postdata['data'];
		    $adid = $modelData ->id;
			if($adid>0){
				
				$qs = 'select username, instagram_url from campaign_influencer where status=2 AND advertiser_id='.$adid;
				$r= $db->query($qs);
				$data = ''; //array();             // The array we're going to be returning
				while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) { 
					//$data .= '<a href="http://instagram.com/'.$row['username'].'" target="_blank">'.$row['username'].'</a>  '; 
					$data .= $row['username'].' url= '.$row['instagram_url'].' ';
				}
		
			}else{
				throw new cException('Invalid ID', 401);
			}
		} break;
		
		case 'getDashboard' : {

			$cookie = cCookie::getCookie();
			if(isset($cookie->influencer_id)) { 
				
			$r = $db->query('select *  from campaign_influencer where status in (0,1) AND influencer_id='.$cookie->influencer_id);
			$data['rcount'] = $r->num_rows;
				
			$r= $db->query('select count(*) as pcount , SUM(price) as earned from campaign_influencer where status=2 AND posteddate  >= CURDATE() - INTERVAL 1 YEAR AND influencer_id='.$cookie->influencer_id);
			$obj = $r->fetch_object();
			$data['pcount'] = $obj->pcount;
			$data['earned'] = $obj->earned;
			 
			} elseif (isset($cookie->fbid))	{
				 
			$data['dashdata'] = cModel::query('select facebookpage  from accounts where fbid='.$cookie->fbid);
			
			
			} else { throw new cException('Invalid ID', 401); }

				
		}break;
		
		case 'postToInstagram' : {

				
				
		} break;
		
		case 'getAdByID' : {
			
			$modelData = $request->postdata['data'];
			$advertiser_id = $modelData->id;
			$data['campaign'] = cModel::query('select * from campaigns where advertiser_id='.$advertiser_id)[0];
			if($data['campaign']['influencers']) $data['campaign']['influencers'] = explode(",",$data['campaign']['influencers']);
			if($data['campaign']['interests']) $data['campaign']['interests'] = explode(",",$data['campaign']['interests']);
			$data['interest'] = cModel::query('select * from campaign_interest where advertiser_id='.$advertiser_id);
				
		}break;
		
		case 'deleteAd' : {
			
			$cookie = cCookie::getCookie();
			
			$modelData = $request->postdata['data'];
			$advertiser_id = $modelData->id;
			
			$db->query('DELETE FROM campaigns WHERE advertiser_id='.$advertiser_id);
			$db->query('DELETE FROM campaign_interest WHERE advertiser_id='.$advertiser_id);
			
			// updated model
			$query2 = ' fbid = "'.$cookie->fbid.'"  ORDER BY campaigns.createddate DESC';
			$data['campaigns'] = cModel::query('select * from campaigns where status IN (0,1) AND '.$query2 );

		} break;
		
		case 'uploadImage' : {
				
			$image = new cImage();
			$image->uploadImages($request, $message);
			
			$cookie = cCookie::getCookie();
			$Id = $cookie->influencer_id;
			
			$newImage = '/uploads/'.$image->uploadImages($request, $message);
			
			if($Id>0) { 				
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE influencer_id='.$Id;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where influencer_id='.$Id )[0];
				
			} else {
				
				$w = ' email = "'.$cookie->email.'"';
				
				$query = 'UPDATE influencers SET `profile_picture`="'.$newImage.'" WHERE '.$w;
				$db->query($query);
				
				$data['influencers'] = cModel::query('select * from influencers where '.$w )[0];
				
			}
		
		} break;
		
		case 'uploadAdImage' : {
		
			$image = new cImage();
			$data['campaign']->advert_image = '/uploads/'.$image->uploadImages($request, $message);
	
		} break;
		
		case 'getModel' : 
		case 'deleteModel' : 
		case 'updateModel' : {
			
			$cookie = cCookie::getCookie();
			
			if(isset($request->postdata['data']) && isset($request->postdata['model'])){
				$modelData = json_decode($request->postdata['data']);
				$modelTable = $request->postdata['model'];
				
				$modelObj = null;
				
				switch($modelTable){
					case 'alerts' : $modelObj = new Alerts(); break;
					case 'messages' : $modelObj = new Messages(); break;
					case 'influencers' : $modelObj = new Influencers(); break;
					case 'accounts' : $modelObj = new Accounts(); break;
					case 'influencer_interest' : $modelObj = new Influencer_Interest(); break;
					case 'campaign_interest' : $modelObj = new Campaign_Interest(); break;
				}
				
				$modelObj->set($request->postdata['data']);
				
				switch($action){
					case 'getModel' : {
						$data[$modelTable] = $modelObj->get(); 
					}break;
					case 'deleteModel' : {
						$modelObj->delete();
					}break;
					case 'updateModel' : {
						if(($data[$modelTable] = $modelObj->update()) == null){
							$data[$modelTable] = $modelObj->insert();
						}
						
						$modelObj->post_insert_update($modelData);
						// Update session data
						
					}break;
				}
			}
			
		}break;
	
		case 'getInterests' : {
			
			$data = cModel::fetch('interests', '*');
			
		}break;
		
		case 'getInterestsByAdId' : {

			$modelData = json_decode($request->postdata['data']);			
			//$r= $db->query('SELECT interest_id FROM campaign_interest WHERE advertiser_id='.$modelData);
			$r= $db->query('SELECT a.interest_id,b.name as name FROM campaign_interest a INNER JOIN interests b ON a.interest_id=b.id WHERE a.advertiser_id='.$modelData);
			$data = '';             // The array we're going to be returning
			while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) { $data.= $row['name'].' '; }
			
		}break;
		
		case 'getCampaignAlert' : {
			$cookie = cCookie::getCookie();
			$q = 'select sum(status=0) as draft, sum(status=1) as requests, sum(status=2) as active,sum(status=3) as finished,sum(status=4) as archived from campaigns where fbid='.$cookie->fbid;
			$data['alerts'] = cModel::query($q);
			
			$a = 'select advertiser_id,name from campaigns where status=2 AND fbid='.$cookie->fbid;
			$data['active'] = cModel::query($a);
			foreach ($data['active'] as &$d) { 
				$r = $db->query('select count(advertiser_id) as total, count(instagram_url) as posts, sum(likes) as likes from campaign_influencer where advertiser_id='.$d['advertiser_id']);
				$obj = $r->fetch_object();
				$d['total']=$obj->total;
				$d['posts']=$obj->posts;
				$d['likes']=$obj->likes;
			}
		
		} break;
		
		case 'clearCookie' : {
			
			$cookie = null;
			cCookie::setCookie($cookie);
		} break;
		
		case 'setEmailAddress' : {
			
			// TODO validate email.
			if(cAccount::checkEmailExists($request->postdata['email'], $message)){
				throw new cException($message, 401);
			}
			
			$cookie = new stdClass();
			$cookie->email = $request->postdata['email'];
			cCookie::setCookie($cookie);
			
		} break;
		
		case 'getFollower' : {
			
			$cookie = cCookie::getCookie();
			
			if(isset($cookie->followed_by)) { $data['followed_by']= $cookie->followed_by; } else {
				
				$q = "select instagram_id, access_token from influencers  WHERE  influencer_id=".$cookie->influencer_id;
				$r = $db->query($q);
				
			$rows = array();
			while($row = $r->fetch_object())
			{
			    $rows[] = $row;
			}
			
			$url = 'https://api.instagram.com/v1/users/'.$rows[0]->instagram_id.'?access_token='.$rows[0]->access_token;
			$api_response = file_get_contents($url);
			$record = json_decode($api_response);
			
			$data['followed_by']=$record->data->counts->followed_by;
			
			}
			
			//$cookie = new stdClass();
			//$cookie->follower = $data['follower'];
			//cCookie::setCookie($cookie);
	
		} break;
		
		case 'getInfluencerInstadata' :{
			
			$modelData = $request->postdata['data'];
			$indata = $modelData ->interests;
			
			//create interrests set
			$interests = '(0';
			foreach ($indata as $inlist) { 	$interests .= ','.$inlist;	}
			$interests .= ')';
			
			$q = @'select * from influencers a inner join influencer_interest b 
					on a.influencer_id=b.influencer_id  where b.interest_id in '.$interests.' group by a.influencer_id';
			
			//error_log('test '.$q);
			
			$data['influencers'] = cModel::query($q);
			
			foreach ($data['influencers'] as &$datainflu) {
				
				$instaId = $datainflu['instagram_id']; //error_log('instaid '.print_r($instaId,true).count($data['influencers']));
				
				$media = $instagram->getUserMedia($instaId);
					
				//also can get comments->count , likes->count
				
				$instagram->setAccessToken($datainflu['access_token']);
				$user = $instagram->getUser($instaId);
				$followers = $user->data->counts->followed_by; 
				//error_log(print_r($user->counts->media,true));
				if($followers!=null) $datainflu['followers']= $followers; else $datainflu['followers']=0;
				$limit = $user->data->counts->media; //counts($media);
				if($limit>20) $limit=20;
				
				$sumlikes = 0;
				$sumcomments = 0;
				
				$thumnail=array();
				foreach(array_slice($media->data, 0, $limit) as $mdata)
				{  $e = $mdata->images->thumbnail->url;
				if(!is_null($e)) $thumnail[]=$e; 
				$sumlikes += $mdata->likes->count;
				$sumcomments += $mdata->comments->count;
				}
					
				$data['medias'][$instaId]= $thumnail;
				$datainflu['likes']=$sumlikes;
				$datainflu['comments']=$sumcomments;
			} 
			
		} break;
		
	
		case 'getInstagramUser' :{
			
			$cookie = cCookie::getCookie();
			//error_log($cookie->email.' COOKIE');
	
			$code = $request->postdata['code'];
			
			if (isset($code)) {
				$user = $instagram->getOAuthToken($code);
				$instagram->setAccessToken($user);
				
				$data['influencer']->instagram_id = $user->user->id;
				
				//$data['influencer'] = $user->user;
				$data['influencer']->username = $user->user->username;
				$data['influencer']->full_name = $user->user->full_name;
				$data['influencer']->bio = $user->user->bio;
				$data['influencer']->website = $user->user->website;
				$data['influencer']->profile_picture = $user->user->profile_picture;
				
				$data['influencer']->followed_by = $user->user->counts->followed_by;
				
				//$data['media'] = $instagram->getUserMedia();
				$data['influencer']->access_token = $user->access_token;
				$data['influencer']->email = $cookie->email;
				// Update DB if they don't already exist.
				//cModel::update('influencers', $data['influencer']);
				$d = cModel::query('select * from influencers where instagram_id = "'.$data['influencer']->instagram_id.'"')[0];
				
				if(!isset($cookie->email) && !isset($d['email'])){
					throw new cException('Account does not exist, please signup first');
				} 
				
				if(isset($d['influencer_id'])){
					if(isset($cookie->email) && $cookie->email != $d['email'] ){
						throw new cException('This instagram account is already in use with another email address',  401);
					}
					
					$data['influencer'] = $d;
					$cookie->influencer_id = $d['influencer_id'];
					$cookie->email = $d['email'];
					$cookie->followed_by = $data['influencer']->followed_by;
					cCookie::setCookie($cookie);
				}
				
			} else {
				// check whether an error occurred
				if (isset($request->postdata['error'])) {
					throw new cException('An error occurred: ' . $request->postdata['error_description'], 555);
				}else{
					throw new cException('Unknown error', 555);
				}
			}
			
			
		} break;
		
		case 'getInstagramLoginUrl' :{
			$data['loginUrl'] = $instagram->getLoginUrl();
			
		} break;
		
		case 'getFacebookLoginUrl' :{
			$data['loginUrl'] = $facebook->getLoginUrl(array(
			 'scope' => 'email', // Permissions to request from the user
			 ));
				
		} break;
		
		case 'getFacebookUser' :{
			
			$cookie = cCookie::getCookie();
			$user = $facebook->getUser();
			
			if ($user) {
				
				try {
					$user_profile = $facebook->api('/me');
					$fbid = $user_profile['id'];                 // To Get Facebook ID
					$fbuname = $user_profile['username'];  // To Get Facebook Username
					$fbfullname = $user_profile['name']; // To Get Facebook full name
					$femail = $user_profile['email'];    // To Get Facebook email ID
					/* ---- Session Variables -----*/
					$_SESSION['FBID'] = $fbid;
					$_SESSION['USERNAME'] = $fbuname;
					$_SESSION['FULLNAME'] = $fbfullname;
					$_SESSION['EMAIL'] =  $femail;
					//       checkuser($fbid,$fbuname,$fbfullname,$femail);    // To update local DB
					$data['account']->fbid =$fbid;
					$data['account']->username =$fbuname;
					$data['account']->fullname =$fbfullname;
					$data['account']->email = $femail;
					
				} catch (FacebookApiException $e) {
					error_log($e);
					$user = null;
				}
				
				$d = cModel::query('select * from accounts where fbid ="'.$data['account']->fbid.'"')[0];
				error_log(print_r($d,true));
				
				if(isset($d['account_id'])){
					
					if(isset($cookie->email) && $cookie->email != $d['email'] ){
						throw new cException('This instagram account is already in use with another email address',  401);
					}
					
					$cookie->email = $d['email'];
					$cookie->fbid  = $d['fbid'];
					$cookie->account_id = $d['account_id'];
					cCookie::setCookie($cookie);
					
					
				} else { 
					
					cModel::update('accounts', $data['account']);
					$data['signup']=1;
					$cookie->email = $data['account']->email;
					$cookie->fbid  = $data['account']->fbid;
					cCookie::setCookie($cookie);
				}

			} else {
				
				// check whether an error occurred
				if (isset($request->postdata['error'])) {
					throw new cException('An error occurred: ' . $request->postdata['error_description'], 555);
				}else{
					throw new cException('Unknown error', 555);
				}
			}
			
		} break;
		
		case 'logout' : {
			$cookie = null;
			cCookie::setCookie($cookie);
			
			session_unset();
			$_SESSION['FBID'] = NULL;
			$_SESSION['USERNAME'] = NULL;
			$_SESSION['FULLNAME'] = NULL;
			$_SESSION['EMAIL'] =  NULL;
			$_SESSION['LOGOUT'] = NULL;			
	
			$message = 'logged out';
		
		} break;
		
		case 'checkAuth' :{
				
			$cookie = cCookie::getCookie();
			
			if(isset($cookie->fbid) || isset($cookie->influencer_id)) $data['auth']=true; else $data['auth']=false;
			if(isset($cookie->email)) $data['auth2']=true; else $data['auth2']=false;
		
		} break;
		
	}
	
	$response->setData($data);
	$response->setStatus($status);
	$response->setMessage($message);
	$response->jsonOutput();


	
}
catch( cException $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage($e->getPublicMessage());
	
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

	

}catch( Exception $e ){
	
	cException::error_log($e);
	
	$response->setStatus($e->getCode());
	$response->setMessage('Could not complete request');
	header('HTTP/1.1 555 Server Error');
	$response->jsonOutput();

}


try {
	if(strlen($action) == 0){
		$action = 'Not defined';
	}
	
	$timeEnd = microtime(true);
	//dividing with 60 will give the execution time in minutes other wise seconds
	$executionTime = ($timeEnd - $timeStart);
		
	$query = 'call pc3.sp_insert_exec_time("'.$action.'", '.$executionTime.')';
	$result = $db->query($query);
	
}
catch( cException $e ){
	cException::error_log($e);
}catch( Exception $e ){
	cException::error_log($e);
}
// record this.


function getInterestName($adid) {
	
	$q = 'select b.name from campaign_interests a inner join interests b on a.interest_id=b.id where a.advertiser_id='.$adid.' roder by a.advertiser_id';
	
	$r = $db->query($q);
	
	
	
}


?>