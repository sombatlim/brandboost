<?php 

class cResponse{
	public $status = null;
	public $data = null;
	public $message = null;

	function __construct() {
		$this->status = cStatus::$OK;
	}
	public function setData($data){
		$this->data = $data;
	}
	public function setStatus($status){
		$this->status = $status;
	}
	public function setMessage($message){
		$this->message = $message;
	}

	public function jsonOutput(){
		echo json_encode($this);
	}


}

?>