<?php 
class cException extends Exception
{
	
	private $publicMessage;
	private $privateMessage;
	
	public function __construct($publicMessage, $code, $privateMessage = null, Exception $previous = null) {

		$this->publicMessage = $publicMessage;
		$this->privateMessage = $privateMessage;
		
		parent::__construct($publicMessage, $code, $previous);
	}


	public function getPublicMessage() {
		return $this->publicMessage;
	}
	
	public function getPrivateMessage(){
		
		if(!is_null($this->privateMessage)){
			$trace = $this->getTraceAsString();
			return __CLASS__ . ": [{$this->code}]: {$this->privateMessage} in {$this->file} on line {$this->line} Stack Trace:{$trace}";
		}
		
		return null;
	}
	
	public static function error_log($e){
	
		$isPublic = method_exists($e, 'getPrivateMessage');
		if( $isPublic && !is_null($e->getPrivateMessage())){
			error_log($e->getPrivateMessage(), 0);
				
		}else if(!$isPublic){
			error_log($e->getMessage(), 0);
		}
	}
	
	
}


?>