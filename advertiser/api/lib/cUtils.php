<?php
class cUtils{
	


public static $encryptKey = 'Check ckkkslso';
public static function encrypt($sData, $sKey='mysecretkey'){ 
    $sResult = ''; 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) + ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    return cUtils::encode_base64($sResult); 
} 

public static function decrypt($sData, $sKey='mysecretkey'){ 
    $sResult = ''; 
    $sData   = cUtils::decode_base64($sData); 
    for($i=0;$i<strlen($sData);$i++){ 
        $sChar    = substr($sData, $i, 1); 
        $sKeyChar = substr($sKey, ($i % strlen($sKey)) - 1, 1); 
        $sChar    = chr(ord($sChar) - ord($sKeyChar)); 
        $sResult .= $sChar; 
    } 
    return $sResult; 
} 


public static function encode_base64($sData){ 
    $sBase64 = base64_encode($sData); 
    return strtr($sBase64, '+/', '-_'); 
} 

public static function decode_base64($sData){ 
    $sBase64 = strtr($sData, '-_', '+/'); 
    return base64_decode($sBase64); 
}  

public static function calcBidAmount($bidAmount){

	return round($bidAmount * 0.8, 2); // Shopping merchant, only get 80% revenue share.$bidAmount;
}

public static function highlight($text, $qstring) {
	
	/*$qstring = '';
	if(isset($_GET['q']))
		$qstring = trim($_GET['q']);
	else if(isset($_GET['q2']))
		$qstring = trim($_GET['q2']);
	*/
	if($qstring == NULL)
		return $text;
	
	$qstring = str_replace('-', ' ', $qstring);
		
	$result = $text;
			
	if(strlen($qstring)){	

		$qstring = preg_replace('/ {2,}/i', ' ', trim($qstring) );
		$words = explode(' ', $qstring);
		$count = 0;
		$offset = 0;
		foreach($words as $w){

			$offset = stripos($text, $w);
			
			if($offset !== false)
				$count++;
			
		}
		
		if($count == sizeof($words)){

			preg_match_all('~\w+~', $qstring, $m);
		    if(!$m)
		        return $result;
	    	$re = '~\\b(' . implode('|', $m[0]) . ')\\b~i';
	    	$result = preg_replace($re, '<b>$0</b>', $text);
		}
		
	
		$t = preg_replace('!([0-9]+)!', ' $1 ', $text);
		foreach($words as $w){
			if(is_numeric($w)){
				if( stripos($t, ' '.$w.' ') && stripos($t, '<b>'.$w.'</b>') === false){
					// If there's a number that hasn't been bolded. 
					$re = '~(' . $w . ')(?=[a-z])~i';		
					$result = preg_replace($re, '<b>$1</b>', $result);
				}
			}
		}
	}
	return $result;
}





public static function getFileExtension($path){
	return pathinfo($path, PATHINFO_EXTENSION);
}


public static function limitChars( $text, $char_limit ){
	$length = 0;
	$text = trim($text);
	$newtext = $text;

	if( !is_null($text) && strlen($text) > $char_limit ){
		while( $length <= $char_limit )	{
			$length++;
				
			if( substr($text, $length-1, 1) == ' ' ){
				$newtext = substr($text, 0, $length-1 );
			}
		}
	}

	if( $newtext != $text )
		$newtext .= '...';

	return $newtext;
}


public static function appendUtm($url, $utmType, $val){

	
	// $type = utm_source, utm_medium
	
	
	
	$pos = strpos($url, '#');
	if($pos !== FALSE){
		$delim = '&';
	}else{
		$pos = strpos($url, '?');
		$delim = ($pos === FALSE) ? '#' : '&';
	}
	
	//if($delim == '?'){
		
	//}

	if(($pos = strpos($url, $utmType)) === FALSE){

		if(substr($url, -strlen('&')) === '&' || substr($url, -strlen('#')) === '#' ) { // if url is http://domain.com/?test=1& or http://domain.com/test=1#
			$url .= $utmType.'='.$val;	// don't add delimiter
		}else if(substr($url, -strlen('?')) === '?'){ // if url is http://domain.com/?
			$url .= '#'.$utmType.'='.$val; // add hash only
		}
		else{
			$url .= $delim.$utmType.'='.$val;
		}
	}

	return $url;

}






}