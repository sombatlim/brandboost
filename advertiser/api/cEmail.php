<?php
//require_once '/home/tuktuk/AWSSDKforPHP/sdk.class.php';
require_once $docRoot.'/dist/aws-sdk-1.6.2/sdk.class.php';

class cEmail{
	
	private $config;
	private $headers;
	private $message;
	private $toEmail;
	private $fromEmail;
	private $subject;
	
	
	private $firstName;
	private $lastName;
	
	
	function __construct($config, $subject, $data){
		
		$this->config = $config;
		
		$this->fromEmail = $config['aws_verified_email'];
		$this->subject = $subject;
		$this->toEmail = $data['contactEmail'];
		$this->firstName = $data['contactNameFirst'];
		$this->lastName = $data['contactNameLast'];
		$this->message = $data['message'];
		
	}
	
	public function setHeaders(){
		$this->headers = "From: " . strip_tags($this->fromEmail) . "\r\n";
		$this->headers .= "Reply-To: ". strip_tags($this->fromEmail) . "\r\n";
		
		//$headers .= "CC: susan@example.com\r\n";
		
		$this->headers .= "MIME-Version: 1.0\r\n";
		$this->headers .= "Content-Type: text/html; charset=UTF-8\r\n";		
		
	}
	

	public function send(){
		
		$this->setHeaders();
		mail($this->toEmail, $this->subject, $this->message, $this->headers);
		
	}
	
	public function sendSES(){
		
		// git clone git://github.com/amazonwebservices/aws-sdk-for-php.git AWSSDKforPHP
		// http://www.alexkorn.com/blog/2011/04/sending-email-aws-php-with-amazonses/
		
		$amazonSes = new AmazonSES(array(
				'key' => $this->config['aws_key'],
				'secret' => $this->config['aws_secret_key']
		));
		
		$response = $amazonSes->send_email($this->fromEmail ,
				array('ToAddresses' => array($this->toEmail)),
				array(
						'Subject.Data' => $this->subject,
						'Body.Html.Data' => $this->message,
				)
		);
		if (!$response->isOK()){
			throw new cException('AWS Unable to send email', cStatus::$INVALID_ACTION, 'AWS email send failed');// handle error
		}
	}
	
	public function sendSES2($path,$subject,$dest,$fname){
		
		$CA = true;
		
		$amazonSes = new AmazonSES(array( "key" => $this->config['aws_key'], "secret" => $this->config['aws_secret_key'], "certificate_authority" => $CA ));
	
		$src = "noreply@tuktuk.com";
		$message= "To: ".$dest."\n";
		$message.= "From: ".$src."\n";
		$message.= "Subject: #".$subject."\n";
		$message.= "MIME-Version: 1.0\n";
		$message.= 'Content-Type: multipart/mixed; boundary="aRandomString_with_signs_or_9879497q8w7r8number"';
		$message.= "\n\n";
		$message.= "--aRandomString_with_signs_or_9879497q8w7r8number\n";
		$message.= 'Content-Type: text/plain; charset="utf-8"';
		$message.= "\n";
		$message.= "Content-Transfer-Encoding: 7bit\n";
		$message.= "Content-Disposition: inline\n";
		$message.= "\n";
		$message.= "Dear Influencer,\n\n";
		$message.= "Attached is the Requested Campaigns.\n";
		$message.= "\n\n";
		$message.= "--aRandomString_with_signs_or_9879497q8w7r8number\n";
		$message.= "Content-ID: \<77987_SOME_WEIRD_TOKEN_BUT_UNIQUE_SO_SOMETIMES_A_@domain.com_IS_ADDED\>\n";
		$message.= 'Content-Type: application/zip; name="'.$fname.'"';
		$message.= "\n";
		$message.= "Content-Transfer-Encoding: base64\n";
		$message.= 'Content-Disposition: attachment; filename="'.$fname.'"';
		$message.= "\n";
		$message.= base64_encode(file_get_contents($path));
		$message.= "\n";
		$message.= "--aRandomString_with_signs_or_9879497q8w7r8number--\n";
		
		$response = $amazonSes->send_raw_email(array(
		  				'Data'=> base64_encode($message)),
							 array('Source'=>$src, 'Destinations'=> $dest));
		                       /*$response = $amazonSes->send_raw_email($mail_array);*/
		print_r($response);
				if (!$response->isOK()){
					throw new cException('Unable to send email', cStatus::$INVALID_ACTION, 'AWS email send failed');// handle error
				}
	}
	
	public function loadTemplate($path){
		
		$type = pathinfo($path, PATHINFO_EXTENSION);
		$imgdata = file_get_contents($path);
		$base64 = 'data:image/' . $type . ';base64,' . base64_encode($imgdata);
		
		$content = '<html><body><img src="'.$base64.'"></body></html>';
		
		//$content = file_get_contents($path);		
		//$content = str_replace("{firstName}", $this->firstName, $content);
		//$content = str_replace("{lastName}", $this->lastName, $content);
		//$content = str_replace("{contactEmail}", $this->toEmail, $content);
		
		$this->message = $content;
	}
	
	public function setCustomKey($key, $value){
		$this->message = str_replace($key, $value, $this->message);
	}
	
	
	
	
}