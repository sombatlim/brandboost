<?php
$docRoot = '/home/brandboost/advertiser';
$config = parse_ini_file($docRoot.'/config.ini');
ini_set('session.cookie_domain', '.brandboost.asia');

if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$config['base_url'] = str_replace('http://', 'https://', $config['base_url']);
}

require_once $docRoot.'/api/lib/cModel.php';
require_once $docRoot.'/api/lib/cException.php';
require_once $docRoot.'/api/lib/cRequest.php';
require_once $docRoot.'/api/lib/cResponse.php';
require_once $docRoot.'/api/lib/cStatus.php';
//require_once $docRoot.'/api/lib/cMemcache.php';
require_once $docRoot.'/api/lib/cUtils.php';
require_once $docRoot.'/api/lib/cMySqli.php';
require_once $docRoot.'/api/lib/cCookie.php';

require_once $docRoot.'/api/cInstagram.php';
require_once $docRoot.'/api/cAccount.php';
require_once $docRoot.'/api/cImage.php';

require_once $docRoot.'/api/model/Base.php';
require_once $docRoot.'/api/model/Alerts.php';
require_once $docRoot.'/api/model/Messages.php';
require_once $docRoot.'/api/model/Accounts.php';
require_once $docRoot.'/api/model/Influencers.php';
require_once $docRoot.'/api/model/Influencer_Interest.php';
require_once $docRoot.'/api/model/Campaigns.php';
require_once $docRoot.'/api/model/Campaign_Interest.php';

require_once $docRoot.'/dist/facebook-php-sdk-master/src/facebook.php';

$instagram = new cInstagram(array(
		'apiKey'      => $config['instagramApiKey'],
		'apiSecret'   => $config['instagramApiSecret'],
		'apiCallback' => $config['instagramApiCallback'] // must point to success.php
));

$facebook = new Facebook(array(
		'appId'  => $config['fbAppId'],
		'secret' => $config['fbSecret'],
		'cookie' => true,
		'status' => true,
));

$db = NULL;
$memcache = NULL;

try
{
	
	$db = new cMySqli($config);
	$db->query("SET NAMES UTF8");
		
	//$memcache = new cMemcache($config);
}
catch( Exception $e ){
	cException::error_log($e);
	header('HTTP/1.1 503 Service Unavailable');
	exit(1);
}


