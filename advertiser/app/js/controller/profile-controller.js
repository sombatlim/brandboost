function ProfileController($rootScope, $scope, $routeParams, $location, $window, apiFactory, $cookies, influencersModel) {
	
	
	$scope.influencer = {};

	$rootScope.step10 = true;
	$rootScope.step20 = true;
	$rootScope.step31 = true;
	$rootScope.stepbar = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step21 = false;
	$rootScope.step30 = false;
	
	$scope.done = false;
	$scope.home = "/";
	
	influencersModel.getPageData(influencersModel.notificationUpdated);	// Calls to server.
	
	$scope.$on(influencersModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
		$scope.influencer = response.data.data.influencers; 
		
    });
 
	
	$scope.$on('uploadImage', function(event, data) {	// This is called on return fromserver based on influencersModel.notification
		$scope.influencer = data; 
		
    });
	
	$scope.saveProfile = function(){
		influencersModel.data = $scope.influencer;
		influencersModel.updateModel();
		
		//alert("DONE");
		$scope.done = true;
		$scope.status = "Your profile has been updated!";
		
		if(influencersModel.data.influencer_id > 0){
		//$window.location.href = $scope.home;
		}
	}
	
}