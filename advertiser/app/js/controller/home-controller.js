function HomeController($rootScope,$scope, $routeParams, $location, $cookies, campaignsModel,campaignInterestModel) {
	
	$rootScope.signin = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step20 = false;
	$rootScope.step30 = false;
	$rootScope.step10 = false;
	$rootScope.step21 = false;
	$rootScope.step31 = false;
	$rootScope.stepbar = false;
	$scope.facebookpage='thebigdot.web';

	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { //-- start checkAuth
		
		if(response.data.data.auth) { 
    	
		$scope.login= true;
		
		campaignsModel.getDashboard(campaignsModel.notificationDashboard);
		
		$scope.$on(campaignsModel.notificationDashboard, function(event, response) {	// This is called on return fromserver based on campaignsModel.notification
			
			$scope.dashdata = response.data.data.dashdata; 
			
			if($scope.dashdata[0].facebookpage) $scope.facebookpage=$scope.dashdata[0].facebookpage;
			
			//console.log($scope.dashdata[0].facebookpage);

	    });
		
		campaignsModel.getAllPostedCampaigns(campaignsModel.notificationUpdated);
		
		$scope.$on(campaignsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on campaignsModel.notification
			
			$scope.campaigns = response.data.data.campaigns; 
			
			//console.log($scope.campaigns);

	    });
		
		}

    }); // End - checkAuth

}