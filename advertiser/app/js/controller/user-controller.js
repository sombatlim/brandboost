function UserController($rootScope, $scope, $routeParams, $location, $window, apiFactory, $cookies, $cookieStore, accountsModel, ezfb, campaignsModel) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { //-- start checkAuth
		
		if(response.data.data.auth) {
	
	$rootScope.login = true;

	$rootScope.step10 = true;
	$rootScope.step21 = true;
	$rootScope.stepbar = true;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step20 = false;
	
	
	accountsModel.getUserData(accountsModel.notificationUpdated);	// Calls to server.
	
		} else { $window.location.href = '/';  } 
	}); // -- End checkAuth
	
	$scope.account = {};
	$scope.done = false;

	
	$scope.$on(accountsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on accountsModel.notification
		$scope.account = response.data.data.accounts; 
		
    });
 
	$scope.saveuserProfile = function(){
		chk = $scope.account.facebookpage.indexOf('//'); 
		if(chk>-1) { $scope.error = true; $scope.status = "Please use facebook page name, not url!"; } else {
		$scope.error = false;
		accountsModel.data = $scope.account;
		accountsModel.updateModel();
		
		//alert("DONE");
		$scope.done = true;
		$scope.status = "Your profile has been updated!";
		
		if(accountsModel.data.account_id > 0){
		//var timestamp = new Date().getTime();
		if($cookies.signup) { 
			$cookieStore.remove('signup');  
			accountsModel.getUserData(accountsModel.notificationUpdated);
			$location.path('/user'); 
			if (!$rootScope.$$phase) $rootScope.$apply();
			
			}
		
		  }
		} // end chk
	}
	
}