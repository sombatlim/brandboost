function MessagesController($rootScope, $scope, $routeParams, $location,  messagesModel, $cookies, $interval, campaignsModel ) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth); 
	
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { //-- start checkAuth
		
	if(response.data.data.auth) {
    	
	$scope.login= true; 
	
	$scope.refreshInterval = 60; // For every 30 sec
	
	$scope.messages = {};

	if($location.path()=='/inbox') $scope.inbox=true; 
	
	if($location.path()=='/outbox') $scope.outbox=true; 
	
	$interval(function() { 
		
			//alertsModel.getAlerts(alertsModel.notificationUpdated);	// Calls to server.
			//$scope.$on(alertsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
			//$scope.alerts = response.data.data.alerts;
			//$scope.activecampaigns = response.data.data.active;			
			//});	

	}, $scope.refreshInterval * 1000); // the refresh interval must be in millisec
	
	//initial
	messagesModel.getMessages(messagesModel.notificationUpdated);	// Calls to server.	
	
	$scope.$on(messagesModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
		
			if(angular.isDefined(response.data.data.messages)) { 
				
				    $scope.messages = response.data.data.messages;
				
				    $scope.total = [];
				    var newmsg=0,sendmsg=0;	    
				    
				    for(var i = 0; i < $scope.messages.length; i++){
				        var mstatus = $scope.messages[i].status;
				        var mto =  $scope.messages[i].msgto;
				        var mfrom =  $scope.messages[i].msgfrom;
				        
				        if(mto==$scope.bbid && mstatus==0) newmsg++;
				        if(mfrom==$scope.bbid) sendmsg++;
				    }
				    
				    if(newmsg>0) $scope.msgStyle={'background-color': '#f00'}; else $scope.msgStyle={'background-color': '#777'};
				    
				    $scope.total.push(newmsg);
				    $scope.total.push(sendmsg);				   		    
				 
			}
			
			if(response.data.data.contacts!=null) $scope.contacts = response.data.data.contacts;
			
			
	});	
	
	$scope.bbid=$cookies.fbid;
	//console.log($scope.fbid);
	$scope.sendMessage=function(newmessage){
		if(angular.isUndefined($scope.msgto) || $scope.bbid==null ) { $scope.error=true; $scope.status="No reciever" } else {
		messagesModel.data = newmessage;
		messagesModel.data.to_fname=$scope.msgto.fname;
		messagesModel.data.msgto=$scope.msgto.bbid;
		messagesModel.data.msgfrom=$cookies.fbid;
		messagesModel.data.full_name=$cookies.fullname;
		messagesModel.data.createddate=new Date();		
		//console.log(messagesModel.data);		
		messagesModel.updateModel(messagesModel.notificationUpdated);			
		messagesModel.getMessages(messagesModel.notificationUpdated);
		
		$scope.dismiss();
		$scope.dismiss2();
		}
		
	}
	
	$scope.readMessage1=function(message){	
		if($scope.readid==null) $scope.readid=message.id; else $scope.readid=null;
		
	}
	
	$scope.readMessage=function(message){			
		$scope.msgto={};
		messagesModel.data = {};
		$scope.msg = angular.copy(message);
		if($scope.msg.msgto==$scope.bbid && message.status==0) {
			messagesModel.data.id = message.id; messagesModel.data.status=1;
			messagesModel.updateModel(messagesModel.notificationUpdated);			
			messagesModel.getMessages(messagesModel.notificationUpdated);
		}
		
		$scope.msg.message = "";
		$scope.msg.subject = 'RE:'+ message.subject;
		$scope.msgto.fname=message.to_fname;
		$scope.msgto.bbid=message.msgfrom;
		//console.log($scope.msg);
		$scope.readmsg = message.message;
		delete $scope.msg['id'];		
		
		
	}
	
	$scope.deleteMessage=function(e){
		var id = e.target.attributes.data.value; 
		messagesModel.data={};
		messagesModel.data.id = id;				
		//console.log(messagesModel.data);		
		messagesModel.deleteModel();			
		messagesModel.getMessages(messagesModel.notificationUpdated);	
		$(e.target).val();
	}	
	
	 
	} //--end-auth	
	
	});
	
	
}

app.directive('myModal', function() {
	   return {
	     restrict: 'A',	     
	     link: function(scope, element, attr) {
	       scope.dismiss = function() {	    	   
                   element.modal("hide");               
	       };
	     }
	   } 
});

app.directive('myyModal', function() {
	   return {
	     restrict: 'A',	     
	     link: function(scope, element, attr) {
	       scope.dismiss2 = function() {	    	   
                element.modal("hide");               
	       };
	     }
	   } 
});

