function CampaignsController($rootScope, $scope, $routeParams, $location, $window, apiFactory, $cookies, $cookieStore, campaignsModel,campaignInterestModel,influencersModel) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { //-- start checkAuth
		
		if(response.data.data.auth) {	
	 
		$rootScope.login = true;
		
		$rootScope.step10 = true;
		$rootScope.step20 = true;
		$rootScope.step31 = true;
		$rootScope.stepbar = true;
		$rootScope.navbar = true;
		$rootScope.step11 = false;
		$rootScope.step21 = false;
		$rootScope.step30 = false;
		
		//alert($location.path());
		if($location.path()=="/draft-campaigns") campaignsModel.getDraftCampaigns(campaignsModel.notificationUpdated);
		if($location.path()=="/requested-campaigns") campaignsModel.getNewCampaigns(campaignsModel.notificationUpdated);	// Calls to server.
		if($location.path()=="/advertiser-requests") campaignsModel.getAllNewCampaigns(campaignsModel.notificationUpdated);
		if($location.path()=="/posted-campaigns" || $location.path()=="/revenue-earned") campaignsModel.getPostedCampaigns(campaignsModel.notificationUpdated);
		if($location.path()=="/payment-summary" || $location.path()=="/campaigns-summary") campaignsModel.getPaymentSummary(campaignsModel.notificationUpdated);
		if($location.path()=="/finished-campaigns") campaignsModel.getFinishedCampaigns(campaignsModel.notificationUpdated);
		if($location.path()=="/archived-campaigns") campaignsModel.getArchivedCampaigns(campaignsModel.notificationUpdated);
		 
	} else { $window.location.href = '/';  } 
		}); // -- End checkAuth

	$scope.interest={};
	
	$scope.campaign = {};
	$scope.influencer = {};
	var scopeData = {},scopeData2 = {};
	scopeData.apiFactory = {};
	scopeData2.apiFactory = {};
	
	$scope.selinterests = [];
	$scope.campaign_interest = {};
	
	$scope.editcampaign={};
	$scope.editcampaign.advert_image = '';
	$scope.done = false;
	$scope.home = "/";
	
	$scope.word = "/^[a-zA-Z0-9]*$/";
	$scope.paynext = false;
	$scope.influnext = false;
	
	
	var today = new Date(); 
	var dd = today.getDate();
	if(dd<10){ dd='0'+dd } 
	$scope.today = today.getFullYear() + '-' + today.getMonth()+1 + '-' +dd;
	
	$scope.sumFollowers=0;
	$scope.sumPrice=0;
	$scope.selInfluencers=[];
	
	$scope.sendrequest = true;
		
	$scope.$on(campaignsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on campaignsModel.notification
		$scope.campaigns = response.data.data.campaigns;
		$scope.interest = response.data.data.interest; // will fix this
		//console.log($scope.campaigns);
				
    });
	
	$scope.$on('uploadAdImage', function(event, data) {	// This is called on return fromserver based on influencersModel.notification
		
		$scope.campaign.advert_image = data.campaign.advert_image; 
		$scope.editcampaign.advert_image = data.campaign.advert_image; 
		
    });
		
	/// ----- Get Interests
	if($location.path()=="/create-campaign") {
		
		apiFactory.post('getInterests', $scope).then(function(response) {
			
			if(response.data.data){
				$scope.interests = response.data.data;
			}
			
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
	}
	
	$scope.checkIfChecked = function(item){
		var found = false;
		angular.forEach($scope.campaign.interests, function(value, key) {
				console.log(value);
				if(value.interest_id == item.id){
					
					found = true;
				}
				  
			});
		
		return found;
	}
	
	$scope.editcheckIfChecked = function(item){
		var found = false;
		angular.forEach($scope.campaign_interest, function(value, key) {
				//console.log(value);
				if(value.interest_id == item.id){
					
					found = true;	
					index = $scope.selinterests.indexOf(item.id)
					if(index<0) $scope.selinterests.push(item.id);
				}
				  
			});
		
		return found;
	}
	
	// not good
	var xxx = {};
    $scope.adid = [];
	$scope.getInterest = function(advertiser_id) {
		
		var adata = {};
		adata.apiFactory = {};
		var $chk = $scope.adid.indexOf(advertiser_id);
		
		if($chk<0) { 
		$scope.adid.push(advertiser_id);
		adata.apiFactory.data = advertiser_id;
		//console.log(adata);
			apiFactory.post('getInterestsByAdId', adata ).then(function(response) {
				xxx[advertiser_id] = response.data.data;
			});
		
		
		}
		return xxx[advertiser_id];
	}
	
	
	$scope.updateInterest = function(item, checkStatus){
		
		if(!checkStatus) {
			$scope.selinterests.push(item.id);
		} else 	{
			index = $scope.selinterests.indexOf(item.id)
			$scope.selinterests.splice(index, 1);
		
		}		
			
		console.log($scope.selinterests);
		console.log($scope.selInfluencers);
		
		if($location.path()=="/create-campaign/influencers") {
			$rootScope.loading=true;
			$scope.sumFollowers=0;
			$scope.sumPrice=0;
			
			campaignInterestModel.data.interest_id = item.id;
			campaignInterestModel.data.advertiser_id = $scope.campaign.advertiser_id;
			console.log(campaignInterestModel.data);
			if(!checkStatus)	//TODO:  Don't know why it's in reverse.  Fix later.
				campaignInterestModel.updateModel();
			else
				campaignInterestModel.deleteModel();
			
			
		scopeData2.apiFactory.data = {"interests" : $scope.selinterests };
		apiFactory.post('getInfluencerInstadata', scopeData2).then(function(response) {
			$rootScope.loading=false;
				$scope.influencers = response.data.data.influencers;
				$scope.medias = response.data.data.medias;
				console.log($scope.influencers);
				console.log($scope.medias);
				
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	} // --end path
		
	}
	//--End interests
	
	//---select influencers---//	
	
	$scope.updateSummary = function(influencer){
		
		console.log("updateSummary-when click button");
		
		if(influencer.selInfu==1) { 			
			index = $scope.selInfluencers.indexOf(influencer.influencer_id)
			if(index<0) { 
				$scope.selInfluencers.push(influencer.influencer_id);
				$scope.sumFollowers += parseInt(influencer.followers); $scope.sumPrice += parseInt(influencer.value);
			}
			} else {
				index = $scope.selInfluencers.indexOf(influencer.influencer_id)
				if(index>=0) {
			$scope.sumFollowers -= parseInt(influencer.followers); $scope.sumPrice -= parseInt(influencer.value);
			$scope.selInfluencers.splice(index, 1);
				}
		}
		console.log($scope.selInfluencers);
	} 
	
	$scope.checkInfluencer = function(influencer){
		
		index = -1;
		if($scope.selInfluencers) index = $scope.selInfluencers.indexOf(influencer.influencer_id); 
		
		if(index>-1) { 
			
			$scope.sumFollowers += parseInt(influencer.followers); $scope.sumPrice += parseInt(influencer.value);
			
			return(1); } else {  return(0); }
		
	}
		
	$scope.postAd = function(campaign){

		$scope.master = angular.copy(campaign);
		console.log($scope.master);
		campaignsModel.data = $scope.master;
		
		campaignsModel.postToInstagram(campaignsModel.notificationUpdated);
		
		if(campaignsModel.data.campaign_id > 0){
		//$window.location.href = $scope.home;
		}
	}	
	
	$scope.saveCampaign = function(){
		$scope.done = false;
		var chk = false;		
		var sd = new Date($scope.campaign.startdate);
		var ed = new Date($scope.campaign.enddate);
		
		if(!$scope.newAd.$valid) { $scope.status = "Invalid Tag , No space allow"; 
		
		} else if($scope.campaign.name.length > 40) { $scope.status = "Sorry , Tag name allow only 40 characters";
		
		} else if($scope.selinterests.length <= 0) { $scope.status = "Please select at least one interest";
		
		} else if(isNaN(sd)) { $scope.status = "Please Select Start Date"; 
		
		} else if(isNaN(ed)) { $scope.status = "Please Select End Date";
		
		} else if(sd>ed) { $scope.status = "Start Date must be less than End Date";
		
		} else if($scope.campaign.advert_image == null) { $scope.status = "Please upload Advertising Image"; 		
		
		} else  { chk= true; }
					
		if(chk) {
			//console.log($scope.campaign);
			$scope.campaign.interests = $scope.selinterests;
			$scope.campaign.createddate = new Date();	
			$scope.campaign.fbid = $cookies.fbid;
			scopeData.apiFactory.data = JSON.stringify($scope.campaign);			
			//console.log(scopeData);
			apiFactory.post('saveNewCampaign', scopeData).then(function(response) {				
			
					$scope.done = "btn-success";
					$scope.status = "New campaign was recorded successfully!";
					$cookies.advertiser_id =  response.data.data.advertiser_id;
					if($location.path()=="/create-campaign") { $scope.proceed(response.data.data.advertiser_id); }
				
			}, function(error) {
				console.log(error);
				$scope.done = 'btn-warning';
				$scope.status = error.data.message;
			}); 

		} else { $scope.done = 'btn-danger'; } 
		
	}	
	
	$scope.proceed = function(advertiser_id){ $cookies.advertiser_id = advertiser_id ;  $location.path('/create-campaign/influencers'); }
	
	$scope.saveCampaignInfluencers = function(){ 
		
		if($scope.selInfluencers.length==0) { $scope.done = "btn-warning"; $scope.status = "Please select atleast 1 influencer"; } else {
		scopeData.apiFactory.data = {};
		scopeData.apiFactory.data.advertiser_id = $cookies.advertiser_id;
		scopeData.apiFactory.data.influencers = $scope.selInfluencers;
		scopeData.apiFactory.data.interests = $scope.selinterests;
		scopeData.apiFactory.data.followers = $scope.sumFollowers;
		scopeData.apiFactory.data.price = $scope.sumPrice;
		scopeData.apiFactory.data.lastupdate = new Date();
		//console.log(scopeData);
		
		apiFactory.post('saveCampaignInfluencers', scopeData).then(function(response) {
			
			//response.data.data;
			$location.path('/create-campaign/payment'); 
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});		
		
		} //--end check
	}
	
	$scope.saveCampaignPayment = function(){ 
		if($scope.campaign.payment==null) { $scope.done = "btn-warning"; $scope.status = "Please select Payment Method"; } else {
		scopeData.apiFactory.data = {};
		scopeData.apiFactory.data.advertiser_id = $cookies.advertiser_id;
		scopeData.apiFactory.data.payment = $scope.campaign.payment;
		scopeData.apiFactory.data.lastupdate = new Date();
		//console.log(scopeData);
		
		apiFactory.post('saveCampaignPayment', scopeData).then(function(response) {
			
			//response.data.data;
			$location.path('/create-campaign/summary');
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});	
		
		}
		 
		}
	
	$scope.requestedAd = function(campaign){
		
		if (confirm("Are you sure you want to send request for this campaign ? ") == true) {
				//console.log($scope.campaign);
				scopeData.apiFactory.data = $scope.campaign;
				//console.log(scopeData);
				
				apiFactory.post('sendRequest', scopeData).then(function(response) {
					
					//$scope.campaign = response.data.data.campaign; console.log($scope.campaign);
						$scope.done = "btn-success";
						$scope.status = "Your request has been sent!";
						$scope.sendrequest = false;
					
					//remove advertiser_id
						$cookieStore.remove('advertiser_id');
	
				}, function(error) {
					console.log(error);
					$scope.done = 'btn-warning';
					alert('Error ' + error.data.message);
				});
		}		
	}

	$scope.getPostedInfluencers = function(advertiser_id){

		scopeData.apiFactory.data = { 'id': advertiser_id };
		console.log(scopeData);
		apiFactory.post('getPostedInfluencers', scopeData).then(function(response) {
			
			$scope.PostedInfluencers = response.data.data;
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
	
	}
	
	$scope.deleteAd = function(e){
		
		var id = e.target.attributes.data.value; alert(id);
		
		scopeData.apiFactory.data = { 'id': id };
		
		if (id>0) {
			
		apiFactory.post('deleteAd', scopeData).then(function(response) {
			
			$scope.campaigns = response.data.data.campaigns;
			console.log($scope.campaigns);
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
		});
		
		} // end confirm
		
		$(e.target).val();
	}
	
	
	$scope.editAd = function(advertiser_id){
        $scope.done=false;
        $scope.selinterests = [];
		scopeData.apiFactory.data = { 'id': advertiser_id };
				
		apiFactory.post('getAdByID', scopeData).then(function(response) {
			
			$scope.editcampaign = response.data.data.campaign;
			
			$scope.campaign_interest =  response.data.data.interest;
			
			apiFactory.post('getInterests', $scope).then(function(response) {
				
				if(response.data.data){
					
					$scope.editinterests = response.data.data;					
				}
				
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
			});
			
			//console.log(response.data.data.campaign);
	
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	}	

	$scope.updatedCampaign = function(){

		$scope.done = false;
		var chk = false;
		var sd = new Date($scope.editcampaign.startdate);
		var ed = new Date($scope.editcampaign.enddate);
		
		if($scope.editcampaign.advert_image == null) { $scope.status = "Please upload Advertising Image"; 
		
		} else if($scope.editcampaign.name.length > 40) { $scope.status = "Sorry , Tag name allow only 40 characters";
		
		} else if($scope.selinterests.length <= 0) { $scope.status = "Please select at least one interest";
		
		} else if(isNaN(sd)) { $scope.status = "Please Select Start Date"; 
		
		} else if(isNaN(ed)) { $scope.status = "Please Select End Date";
		
		} else if(sd>ed) { $scope.status = "Start Date must be less than End Date";
		
		} else if(!$scope.newAd.$valid) { $scope.status = "Invalid Tag , No space allow"; } else { chk= true; }
					
		if(chk) {
			
			$scope.editcampaign.interests = $scope.selinterests ;
			$scope.editcampaign.createddate = new Date();
			scopeData.apiFactory.data = JSON.stringify($scope.editcampaign);
			//console.log(scopeData);
			apiFactory.post('updatedCampaign', scopeData).then(function(response) {

					$scope.done = "btn-success";
					$scope.status = "New campaign was recorded successfully!";
					$scope.campaigns = response.data.data.campaigns;
				    $scope.interest = response.data.data.interest
			}, function(error) {
				console.log(error);
				$scope.done = 'btn-warning';
				$scope.status = error.data.message;
			}); 

		} else { $scope.done = 'btn-danger'; }
		
	}
	
	$scope.getTotal = function(){
	    var total = 0;
	    for(var i = 0; i < $scope.campaigns.length; i++){
	        var cprice = $scope.campaigns[i].price;
	        var clikes = $scope.campaigns[i].likes;
	    	//console.log($scope.campaigns[i].price);
	        total += parseInt(cprice) + Math.floor(parseInt(clikes)/$scope.bonus);
	    }
	    return total;
	}
	
	$scope.checkurl = function(url){
	    chk = url.indexOf('//');
	    if(chk<0) url= '//influencer.brandboost.asia' + url;
	    return url;
	}
	
	$scope.getAdSummary = function(){
	    var total = [];
	    var status0=0,status1=0,status2=0;
	    for(var i = 0; i < $scope.campaigns.length; i++){
	        var cstatus = $scope.campaigns[i].status;

	        if(cstatus==0) { status0++; } else if(cstatus==1) {status1++;} else if(cstatus==2) {status2++;}
	        
	    }
	    total.push(status0);
	    total.push(status1);
	    total.push(status2);
	    return total;
	}
	
	$scope.getPaySummary = function(){
	    var total = [];
	    var status0=0,status1=0,status2=0,status3=0,finpaid=0;
	    for(var i = 0; i < $scope.campaigns.length; i++){
	        var cstatus = $scope.campaigns[i].status;
            var price = $scope.campaigns[i].price;
            if(cstatus==0) { status0++; } else if(cstatus==1) { status1++; } else if(cstatus==2) {status2++;} else if(cstatus==3) {status3++; finpaid=finpaid+parseInt(price)}
	        
	    }
	    total.push(status0);
	    total.push(status1);
	    total.push(status2);
	    total.push(status3);
	    total.push(finpaid);
	    return total;
	}
	
	// --TEST ZONE---
	
	if($location.path()=="/create-campaign/influencers") {
		$rootScope.loading=true;
		$scope.done=false;
		$scope.selinterests = [];
		var interestbydefault = [];
		
		var advertiser_id = $cookies.advertiser_id;        
        
		scopeData.apiFactory.data = { 'id': advertiser_id };
				
		apiFactory.post('getAdByID', scopeData).then(function(response) {
			
			$scope.campaign = response.data.data.campaign; //console.log($scope.campaign);
			if($scope.campaign.influencers != null ) $scope.influnext = true;
			
			//interestbydefault = response.data.data.campaign.interests;
			
			$scope.campaign_interest =  response.data.data.interest;
			
			if(response.data.data.campaign.influencers) $scope.selInfluencers = response.data.data.campaign.influencers;
			
					apiFactory.post('getInterests', $scope).then(function(response) {
						
						if(response.data.data){
							
							$scope.editinterests = response.data.data;			
						}				
						
						//$location.path('/create-campaign/influencers');
						
					}, function(error) {
						console.log(error);
						alert('Error ' + error.data.message);
					});	
					
					//console.log($scope.campaign_interest);	
					angular.forEach($scope.campaign_interest, function(index){ interestbydefault.push(index.interest_id); });
					
					//load default at first
					//console.log(interestbydefault); 
					scopeData2.apiFactory.data = {"interests" : interestbydefault,"influencers" : $scope.campaign.influencers }; //console.log(scopeData2);
					apiFactory.post('getInfluencerInstadata', scopeData2).then(function(response) {
							$rootScope.loading=false;
							$scope.influencers = response.data.data.influencers;
							$scope.medias = response.data.data.medias;
							//console.log($scope.influencers);
							//console.log($scope.medias);
							
					}, function(error) {
						console.log(error);
						alert('Error ' + error.data.message);
						
					});
	
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});			
		
	}
	
	if($location.path()=="/create-campaign/payment" || $location.path()=="/create-campaign/summary") {
		$scope.done=false;

		var advertiser_id = $cookies.advertiser_id;        
        
		scopeData.apiFactory.data = { 'id': advertiser_id };
				
		apiFactory.post('getAdByID', scopeData).then(function(response) {
			
			$scope.campaign = response.data.data.campaign;
			if($scope.campaign.payment != null ) $scope.paynext = true;
		
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	}

	
}