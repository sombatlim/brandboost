function InstagramController($rootScope,$scope, $routeParams, $location, $window, apiFactory, $cookies,influencersModel) {

	$scope.influencer = {};
	$scope.apiFactory = {};
	
	$scope.profile = "/profile";
	$scope.interests = "/interests";
	$scope.login = "/login";
	$scope.signup = "/signup";
	$scope.loading = true;
	
	if($routeParams.success){	// This is from a signup, so authenticate the user and save data into DB.
		
		$scope.apiFactory.code = $location.search()['code'] || '';
		$scope.apiFactory.error = $location.search()['error'] || '';
		$scope.apiFactory.error_description = $location.search()['error_description'] || '';
		
		apiFactory.post('getInstagramUser', $scope).then(function(response) {
			
			if(response.data.data &&  response.data.data.influencer){
				
				influencersModel.data = response.data.data.influencer;
				
				if(influencersModel.data.influencer_id > 0){					
					//$location.url('/profile');
					$rootScope.signin = false;
					$rootScope.step11 = false;
					$rootScope.step20 = false;
					$rootScope.step30 = false;
					$rootScope.step10 = false;
					$rootScope.step21 = false;
					$rootScope.step31 = false;
					$rootScope.stepbar = false;
					$window.location.href = $scope.profile;
					
				}else{
					influencersModel.updateModel(influencersModel.notificationUpdated);
					//$location.url('/interests');
					$rootScope.step10 = true;
					$rootScope.step21 = true;
					$rootScope.step30 = true;
					$rootScope.stepbar = true;
					$rootScope.navbar = true;
					$rootScope.signin = false;
					$rootScope.step11 = false;
					$rootScope.step20 = false;
					$rootScope.step30 = false;
										
					$window.location.href = $scope.interests;
				}
			}
		}, function(error) {
			console.log(error);
			$scope.loading = false;
			found = 0;
			errormsg = error.data.message.toString();;
			found = errormsg.indexOf("not exist"); 
			if(found>0) { 
				//alert(error.data.message);
				$window.location.href = $scope.signup;
				
			} else {
				//alert(error.data.message + ', Please Login with Instagram' );
				//$scope.error = error.data.message;
				$window.location.href = $scope.login;
			}
			
		});
		
	}

}
