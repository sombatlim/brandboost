function AlertsController($rootScope, $scope, $routeParams, $location,  alertsModel, $cookies, $interval, campaignsModel ) {
	
	campaignsModel.checkAuth(campaignsModel.notificationAuth);
	
	$scope.$on(campaignsModel.notificationAuth, function(event, response) { //-- start checkAuth
		
		if(response.data.data.auth) {
    	
	$scope.login= true;
	
	$scope.refreshInterval = 60; // For every 30 sec
	
	$scope.alerts = {};
	
	$scope.follower = 5000;
	
	$interval(function() { 
		
			alertsModel.getAlerts(alertsModel.notificationUpdated);	// Calls to server.
			$scope.$on(alertsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
			$scope.alerts = response.data.data.alerts;
			$scope.activecampaigns = response.data.data.active;
			
		});	

	}, $scope.refreshInterval * 1000); // the refresh interval must be in millisec
	
	//initial
	alertsModel.getAlerts(alertsModel.notificationUpdated);	// Calls to server.
	
	if($cookies.influencer_id != null && $cookies.followed_by == null) alertsModel.getFollower();	// get followers.
	
	$scope.$on(alertsModel.notificationUpdated, function(event, response) {	// This is called on return fromserver based on influencersModel.notification
			$scope.alerts = response.data.data.alerts;
			$scope.activecampaigns = response.data.data.active;
			//console.log($scope.activecampaigns);
			if($scope.alerts != null && $scope.alerts.length>0) { $rootScope.alertcss="btn-danger"; } else { $rootScope.alertcss="btn-normal"; } 
			if($cookies.followed_by != null && $cookies.followed_by >= $scope.follower ) { $scope.alerttext= false } else { $scope.alerttext= true }
	});	
	
} 
	}); //--end-auth
	
}