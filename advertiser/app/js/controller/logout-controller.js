function LogoutController($scope, $routeParams, $location, $window, apiFactory,$cookies, $cookieStore, accountsModel, ezfb) {
	
	//console.log('in LogoutController');
	
	$scope.apiFactory = {};
	$scope.influencer = {};
	
	$scope.home = "/";
	
	$scope.error = null;
	
	updateLoginStatus(updateApiMe);	  	

	$scope.logout = function () {
	    /**
	     * Calling FB.logout
	     * https://developers.facebook.com/docs/reference/javascript/FB.logout
	     */
	    ezfb.logout(function () {
	      updateLoginStatus(updateApiMe);
	    },true);
	    
	    
	    apiFactory.post('logout', $scope).then(function(response) {
			
			angular.forEach($cookies, function (v, k) {
			    $cookieStore.remove(k);
			});
			
						
			$window.location.href = "http://www.brandboost.asia/"; //$scope.home;			
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
	    
	    
	  };
	  
	  /**
	   * For generating better looking JSON results 
	   */
	  var autoToJSON = ['loginStatus', 'apiMe']; 
	  angular.forEach(autoToJSON, function (varName) {
	    $scope.$watch(varName, function (val) {
	      $scope[varName + 'JSON'] = JSON.stringify(val, null, 2);
	    }, true);
	  });
	  
	  /**
	   * Update loginStatus result
	   */
	  function updateLoginStatus (more) {
	    ezfb.getLoginStatus(function (res) {
	      $scope.loginStatus = res;

	      (more || angular.noop)();
	    });
	  }

	  /**
	   * Update api('/me') result
	   */
	  function updateApiMe () {
	    ezfb.api('/me', function (res) {
	      $scope.apiMe = res;
	      
	    });
	  }
	  
}
