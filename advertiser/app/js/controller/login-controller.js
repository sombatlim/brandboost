function LoginController($rootScope,$scope, $routeParams, $location, $window, apiFactory, $cookies, accountsModel, ezfb, $templateCache) {
	

	console.log('in LoginController');
	
	$scope.apiFactory = {};
	
	
	$scope.error = null;
	
	$rootScope.signin = false;
	$rootScope.navbar = true;
	$rootScope.step11 = false;
	$rootScope.step20 = false;
	$rootScope.step30 = false;
	$rootScope.step10 = false;
	$rootScope.step21 = false;
	$rootScope.step31 = false;
	$rootScope.stepbar = false;	
	
	$templateCache.removeAll();
	
	/*
	 * 
	$scope.instagram = {};
	$scope.influencer = {};
	apiFactory.post('getInstagramLoginUrl', $scope).then(function(response) {
		if(response.data.data &&  response.data.data.loginUrl){
		
			$scope.instagram.loginUrl = response.data.data.loginUrl;
			
		}
	}, function(error) {
		console.log(error);
		alert('Error ' + error.data.message);
		
	});
	
	
	$scope.login = function(instagram){
		apiFactory.post('clearCookie', $scope).then(function(response) {
			
			$window.location.href = $scope.instagram.loginUrl
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	*/
//////// http://ngmodules.org/modules/angular-easyfb
	
	  updateLoginStatus0(updateApiMe);  // logout if status was connected

	  $scope.login = function () {
	    /**
	     * Calling FB.login with required permissions specified
	     * https://developers.facebook.com/docs/reference/javascript/FB.login/v2.0
	     */
	    ezfb.login(function (res) {
	      /**
	       * no manual $scope.$apply, I got that handled
	       */
	      if (res.authResponse) {
	        updateLoginStatus(updateApiMe);
	        
	        /////TEST
	        apiFactory.post('getFacebookUser', $scope).then(function(response) {
				
				if(response.data.data &&  response.data.data.account){
					
					accountsModel.data = response.data.data.account;

						$rootScope.signin = false;
						$rootScope.step11 = false;
						$rootScope.step20 = false;
						$rootScope.step30 = false;
						$rootScope.step10 = false;
						$rootScope.step21 = false;
						$rootScope.step31 = false;
						$rootScope.stepbar = false;
						if($cookies.fbid == null) $cookies.fbid = accountsModel.data.fbid;
						if($cookies.fullname == null) $cookies.fullname = accountsModel.data.fullname;
						if(response.data.data.signup) { $window.location.href = '/user'; $cookies.signup=1; } else { $window.location.href = '/'; }										

				}
			}, function(error) {
				console.log(error);
				$scope.loading = false;
				found = 0;
				errormsg = error.data.message.toString();;
				found = errormsg.indexOf("not exist"); 
				if(found>0) { 
					//alert(error.data.message);
					$window.location.href = '/signup';
					
				} else {
					//alert(error.data.message + ', Please Login with Instagram' );
					//$scope.error = error.data.message;
					$window.location.href = '/login';
				}
				
			});
	        
	        
	      }
	    }, {scope: 'email,user_likes'});
	  };

	  $scope.logout = function () {
	    /**
	     * Calling FB.logout
	     * https://developers.facebook.com/docs/reference/javascript/FB.logout
	     */
	    ezfb.logout(function () {
	      updateLoginStatus(updateApiMe);
	    });
	  };

	  /*
	  $scope.share = function () {
	    ezfb.ui(
	      {
	        method: 'feed',
	        name: 'angular-easyfb API demo',
	        picture: 'http://plnkr.co/img/plunker.png',
	        link: 'http://plnkr.co/edit/qclqht?p=preview',
	        description: 'angular-easyfb is an AngularJS module wrapping Facebook SDK.' + 
	                     ' Facebook integration in AngularJS made easy!' + 
	                     ' Please try it and feel free to give feedbacks.'
	      },
	      function (res) {
	        // res: FB.ui response
	      }
	    );
	  }; */

	  /**
	   * For generating better looking JSON results
	   */
	  var autoToJSON = ['loginStatus', 'apiMe']; 
	  angular.forEach(autoToJSON, function (varName) {
	    $scope.$watch(varName, function (val) {
	      $scope[varName + 'JSON'] = JSON.stringify(val, null, 2);
	    }, true);
	  });
	  
	  /**
	   * Update loginStatus result
	   */
	  function updateLoginStatus (more) {
	    ezfb.getLoginStatus(function (res) {
	      $scope.loginStatus = res;	      
	      (more || angular.noop)();
	    });
	  }
	  
	  function updateLoginStatus0 (more) {
		    ezfb.getLoginStatus(function (res) {
		      $scope.loginStatus = res;
		      if($scope.loginStatus.status=="connected") $scope.logout();
		      (more || angular.noop)();
		    });
		  }


	  /**
	   * Update api('/me') result
	   */
	  function updateApiMe () {
	    ezfb.api('/me', function (res) {
	      $scope.apiMe = res;
	      
	    });
	  }
	
}
