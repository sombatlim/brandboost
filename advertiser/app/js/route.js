﻿
app.config(function($routeProvider, $locationProvider, $httpProvider) {
  	
	$locationProvider.html5Mode(true).hashPrefix('!');
	
	
	$routeProvider.when('/', {
	    templateUrl: '/app/view/home.html',
	    controller: HomeController,
	    reloadOnSearch: false
	});		

	//// TESTING by sombat
	$routeProvider.when('/inbox', {
	    templateUrl: '/app/view/messages.html',
	    controller: MessagesController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/outbox', {
	    templateUrl: '/app/view/messages.html',
	    controller: MessagesController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/draft-campaigns', {
	    templateUrl: '/app/view/draft-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});	
		
	$routeProvider.when('/requested-campaigns', {
	    templateUrl: '/app/view/requested-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/posted-campaigns', {
	    templateUrl: '/app/view/posted-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/finished-campaigns', {
	    templateUrl: '/app/view/finished-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/archived-campaigns', {
	    templateUrl: '/app/view/archived-campaigns.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/payment-summary', {
	    templateUrl: '/app/view/payment-summary.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/campaigns-summary', {
	    templateUrl: '/app/view/campaigns-summary.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/create-campaign', {
	    templateUrl: '/app/view/create-campaign.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/create-campaign/influencers', {
	    templateUrl: '/app/view/create-campaign-influencers.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/create-campaign/payment', {
	    templateUrl: '/app/view/create-campaign-payment.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/create-campaign/summary', {
	    templateUrl: '/app/view/create-campaign-summary.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/edit-campaign', {
	    templateUrl: '/app/view/edit-campaign.html',
	    controller: CampaignsController,
	    reloadOnSearch: false
	});
	
	$routeProvider.when('/interests', {
	    templateUrl: '/app/view/signup/interests.html',
	    controller: InterestsController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/profile', {
	    templateUrl: '/app/view/signup/profile.html',
	    controller: ProfileController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/user', {
	    templateUrl: '/app/view/user.html',
	    controller: UserController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/login', {
	    templateUrl: '/app/view/signup/login.html',
	    controller: LoginController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/signup', {
	    templateUrl: '/app/view/signup/signup.html',
	    controller: SignupController,
	    reloadOnSearch: false
	});
	
	
	$routeProvider.when('/instagram/:success', {
		templateUrl: '/app/view/instagram.html',
		controller: InstagramController,
	    reloadOnSearch: false
	});	
	
	$routeProvider.when('/logout', {
		templateUrl: '/app/view/logout.html',
		controller: LogoutController,
	    reloadOnSearch: false
	});	
	
	
	/// Authorise the user.
	
	$routeProvider.otherwise('/signup');
	$routeProvider.otherwise('/signup/:success')
	
     var interceptor = ['$location', '$q', function ($location, $q) {

         function success(response) {
             return response;
         }

         function error(response) {

             if (response.status === 401) {
                 $location.path('/login');
                 return $q.reject(response);
             }
             else {
                 return $q.reject(response);
             }
         }

         return function (promise) {
             return promise.then(success, error);
         };
     }];

     $httpProvider.responseInterceptors.push(interceptor);
     
	
	
 
})



