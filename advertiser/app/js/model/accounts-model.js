app.service("accountsModel", ['$rootScope', 'apiFactory', 'baseModel','$cookies', function($rootScope,  apiFactory, baseModel, $cookies) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'accountsModel::updated';
	this.notificationDeleted = 'accountsModel::deleted';
	
	
	this.data = { 
			
			account_id: null,
			fbid: null,
			username: null,
			fullname: null,
			email: null,
			password:null
			
	};

	this.updateModel = function(notification){
		//console.log(this.data);
		baseModel.updateModel('accounts', this.data, notification);
	}
	
	this.getUserData = function(notification){
		
		//console.log(this.data.influencer_id);
		//baseModel.getModel('accounts', this.data, notification);
		
		
		apiFactory.post('getUser', scopeData).then(function(response) {

			if(notification != undefined)
			$rootScope.$broadcast(notification, response);

			if($cookies.fbid == null ) $cookies.fbid = response.data.data.accounts.fbid;
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		//baseModel.getModel('accounts', this.data, notification);
	}
	
	

	    
}]);