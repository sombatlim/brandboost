app.service("influencersModel", ['$rootScope', 'apiFactory', 'baseModel','$cookies', function($rootScope,  apiFactory, baseModel,$cookies) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'influencersModel::updated';
	this.notificationDeleted = 'influencersModel::deleted';
	
	
	this.data = { 
			
			influencer_id: null,
			instagram_id: null,
			username: null,
			full_name: null,
			bio: null,
			website: null,
			profile_picture: null,
			access_token: null,
			email: null,
			line_id:null,
			facebook_username:null
	};

	this.updateModel = function(notification){
		baseModel.updateModel('influencers', this.data, notification);
	}
	
	this.getPageData = function(notification){
		
		//console.log(this.data.influencer_id);
		//baseModel.getModel('influencers', this.data, notification);
		
		
		apiFactory.post('getInfluencer', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
			
			console.log(response.data.data.influencers.influencer_id);
			if($cookies.influencer_id == null ) $cookies.influencer_id = response.data.data.influencers.influencer_id;
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		//baseModel.getModel('influencers', this.data, notification);
	}
	
this.getInfluencerByID = function(notification){
		
		apiFactory.post('getInfluencerByID', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	    
}]);