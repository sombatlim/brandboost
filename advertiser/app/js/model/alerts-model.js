app.service("alertsModel", ['$rootScope', 'apiFactory', 'baseModel', '$cookies', function($rootScope, apiFactory, baseModel, $cookies) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'alertsModel::updated';
	this.notificationDeleted = 'alertsModel::deleted';
	
	
	this.data = { 
			alert_id: null,
			influencer_id: null,
			message: null,
			read: null,
	};

	this.updateModel = function(notification){
		baseModel.updateModel('alerts', this.data, notification);
	}
	
	this.getAlerts = function(notification){
		if($cookies.influencer_id) {
		var id = $cookies.influencer_id;
		console.log(id);
		this.data.influencer_id = id;
		this.data.status = 0;
		baseModel.getModel('alerts', this.data, notification);
		} else if($cookies.fbid) {
			
			apiFactory.post('getCampaignAlert', scopeData).then(function(response) {
				
				if(notification != undefined)
					$rootScope.$broadcast(notification, response);
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});
			
		}
	}
	
	this.getFollower = function(notification){
		
			apiFactory.post('getFollower', scopeData).then(function(response) {
	
				if(notification != undefined)
					$rootScope.$broadcast(notification, response);
				
				console.log(response.data.data.followed_by);
				if($cookies.followed_by == null ) $cookies.followed_by = response.data.data.followed_by;
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});
			
		}
	
	

	    
}]);