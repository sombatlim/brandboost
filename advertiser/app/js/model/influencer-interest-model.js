
app.service("influencerInterestModel", ['$rootScope', 'apiFactory', 'baseModel', function($rootScope, apiFactory, baseModel) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'influencerInterestModel::updated';
	this.notificationDeleted = 'influencerInterestModel::deleted';
	
	this.data = { 
			influencer_id: null,
			interest_id: null,
	};

	this.getPageData = function(notification){
		baseModel.getModel('influencer_interest', this.data, notification);
	}
	
	
	this.updateModel = function(notification){
		baseModel.updateModel('influencer_interest', this.data, notification);
		
	}
	this.deleteModel = function(notification){
		baseModel.deleteModel('influencer_interest', this.data, notification);
		
	}
    
}]);
	