app.service("messagesModel", ['$rootScope', 'apiFactory', 'baseModel', '$cookies', function($rootScope, apiFactory, baseModel, $cookies) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'messagesModel::updated';
	this.notificationDeleted = 'messagesModel::deleted';
	
	
	this.data = { 
			id: null,
			msgfrom: null,
			msgto: null,
			subject:null,
			message: null,
			createddate:null,
	};

	this.updateModel = function(notification){
		//console.log(this.data);
		baseModel.updateModel('messages', this.data, notification);
	}
	
	this.deleteModel = function(notification){
		baseModel.deleteModel('messages', this.data, notification);
		
	}
	
	this.getMessages = function(notification){
			
			apiFactory.post('getMessages', scopeData).then(function(response) {
				
				if(notification != undefined)
					$rootScope.$broadcast(notification, response); 
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});

	}
	
	    
}]);