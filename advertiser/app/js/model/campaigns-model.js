app.service("campaignsModel", ['$rootScope', 'apiFactory', 'baseModel', function($rootScope,  apiFactory, baseModel) {
	
	var scopeData = {};
	scopeData.apiFactory = {};
	
	this.notificationUpdated = 'campaignsModel::updated';
	this.notificationDeleted = 'campaignsModel::deleted';
	this.notificationDashboard = 'campaignsModel::dashboard';
	this.notificationAuth = 'campaignsModel::auth';
	
	
	this.data = { 
			
			advertiser_id:null,
			influencer_id: null,
			username: null,
			
	};

	this.updateModel = function(notification){
		baseModel.updateModel('campaigns', this.data, notification);
	}
	
	this.checkAuth = function(notification){

		apiFactory.post('checkAuth', scopeData).then(function(response) {			
				
			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				//console.log(response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.getDashboard = function(notification){

		apiFactory.post('getDashboard', scopeData).then(function(response) {

			if(notification != undefined)
				
				$rootScope.$broadcast(notification, response);
				//console.log(response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		//baseModel.getModel('campaigns', this.data, notification);
	}
	
	this.getDraftCampaigns = function(notification){
		
		apiFactory.post('getDraftCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
	
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	

	this.getNewCampaigns = function(notification){
		
		apiFactory.post('getNewCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
	
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.getNewCampaignByID = function(notification){
		
		console.log(this.data);
		
		/*
		apiFactory.post('getNewCampaignByID', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
	
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		}); */
		
	}
	
	this.getAllNewCampaigns = function(notification){

		apiFactory.post('getAllNewCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.getPostedCampaigns = function(notification){
			
			//console.log(this.data);
			//baseModel.getModel('campaigns', this.data, notification);

			apiFactory.post('getPostedCampaigns', scopeData).then(function(response) {
	
				if(notification != undefined)
					$rootScope.$broadcast(notification, response);
					
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});
			//baseModel.getModel('campaigns', this.data, notification);
	}
	
	this.getFinishedCampaigns = function(notification){

		apiFactory.post('getFinishedCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.getArchivedCampaigns = function(notification){

		apiFactory.post('getArchivedCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.getAllPostedCampaigns = function(notification){

		apiFactory.post('getAllPostedCampaigns', scopeData).then(function(response) {

			if(notification != undefined)
				$rootScope.$broadcast(notification, response);
				
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}
	
	this.postToInstagram = function(notification){
			
			//console.log(this.data);
			//baseModel.getModel('campaigns', this.data, notification);
			scopeData.apiFactory.data = JSON.stringify(this.data);
			console.log(scopeData);
			apiFactory.post('postToInstagram', scopeData).then(function(response) {
	
				if(notification != undefined)
					$rootScope.$broadcast(notification, response);
					
				
			}, function(error) {
				console.log(error);
				alert('Error ' + error.data.message);
				
			});
			//baseModel.getModel('campaigns', this.data, notification);
		}
	
	this.getPaymentSummary = function(notification){

		apiFactory.post('getPaymentSummary', scopeData).then(function(response) {

			if(notification != undefined)
				
				$rootScope.$broadcast(notification, response);
				//console.log(response);
			
		}, function(error) {
			console.log(error);
			alert('Error ' + error.data.message);
			
		});
		
	}

	    
}]);