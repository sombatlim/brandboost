var app = angular.module('myApp', ['ui.bootstrap','infinite-scroll','ngRoute', 'ui.utils', 'ngCookies','ezfb','hljs']);

function buildApiUrl(param){
	var url = '/api/api.php?act='+param;
	
	if(window.xdebug != 0)
		url += '&' + window.xdebug;
	return url;
}

app = angular.module('myApp').filter('moment', function () {
	  return function (input, momentFn /*, param1, param2, etc... */) {
	    var args = Array.prototype.slice.call(arguments, 2),
	        momentObj = moment(input);
	    return momentObj[momentFn].apply(momentObj, args);
	  };
	});


//http://ngmodules.org/modules/angular-easyfb

app.config(function (ezfbProvider) {
  /**
   * Basic setup
   *
   * https://github.com/pc035860/angular-easyfb#configuration
   */
    ezfbProvider.setInitParams({
    appId: '228932447276805'
  });  
});

