/* login toggle */
$(document).ready(function(){
  $(".login_box > a").click(function(){
    $(".login-dropdown").slideToggle("fast");
  });
});

$('body').on('click', function(e) {
    if(!$( e.target ).closest( "div" ).hasClass( "login_box")){
      $('.login_box > a')
      $('.login-dropdown').hide();
    }
  });

/* ------------ */

$(document).ready(function(){
  $(".goal").hover(function(){
  $(".advertising-process .right li.goal-reach").toggleClass("white-bg");
  });  
  $(".target").hover(function(){
  $(".advertising-process .right li.target").toggleClass("white-bg");
  }); 
  $(".manage").hover(function(){
  $(".advertising-process .right li.manage-campaign").toggleClass("white-bg");
  });
  $(".reporting").hover(function(){
  $(".advertising-process .right li.reporting-tracking").toggleClass("white-bg");
  }); 
});

$(document).ready(function(){
  $(".advertising-process .right li.goal-reach").hover(function(){
  $(".goal").toggleClass("active");
  });  
  $(".advertising-process .right li.target").hover(function(){
  $(".target").toggleClass("active");
  }); 
  $(".advertising-process .right li.manage-campaign").hover(function(){
  $(".manage").toggleClass("active");
  });
  $(".advertising-process .right li.reporting-tracking").hover(function(){
  $(".reporting").toggleClass("active");
  }); 
});


/* -- logo carousel -- */
$(document).ready(function(){
        if (Modernizr.touch) {
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
    
    
  /* accordion */
  $('#accordion').accordion({
      collapsible: true,
      heightStyle: "content"
  });
  
$('#accordion h5#ui-accordion-accordion-header-'+$(this).index()).trigger('click');

/* Mobile Menu */
$(document).ready(function(){
  $(".mobile-nav > a").click(function(){
    $(".main-nav").slideToggle("shlow");
  });
});

/* --------Peralaxx Background---------- */

$(document).ready(function(){
	// Cache the Window object
	$window = $(window);
                
   $('section, div, li[data-type="background"]').each(function(){
     var $bgobj = $(this); // assigning the object
                    
      $(window).scroll(function() {
                    
		// Scroll the background at var speed
		// the yPos is a negative value because we're scrolling it UP!								
		var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
		
		// Put together our final background position
		var coords = '50% '+ yPos + 'px';

		// Move the background
		$bgobj.css({ backgroundPosition: coords });
		
}); // window scroll Ends

 });	

}); 

/* Home slider */
$(function () {

// Slideshow 1
$("#slider1").responsiveSlides({
//maxwidth: 800,
speed: 800
});
});

/* Partners carousel */

$(function() {

				var $c = $('#carousel'),
					$w = $(window);

				$c.carouFredSel({
					align: false,
					items: 10,
					scroll: {
						items: 1,
						duration: 2000,
						timeoutDuration: 0,
						easing: 'linear',
						pauseOnHover: 'immediate'
					}
				});

				
				$w.bind('resize.example', function() {
					var nw = $w.width();
					if (nw < 990) {
						nw = 990;
					}

					$c.width(nw * 3);
					$c.parent().width(nw);

				}).trigger('resize.example');

			});
			

/* smooch scroling */
			
$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 2500, 'swing', function () {
	        window.location.hash = target;
	    });
	});
});



/* Tab Menu */
$(window).ready(function(){
  
  $( "#tabs" ).tabs({show: 'fade', hide: 'fade'});
  
  $('.more-data').click(function(){
    $('.accordian-data div').slideToggle();
  });
    
});


